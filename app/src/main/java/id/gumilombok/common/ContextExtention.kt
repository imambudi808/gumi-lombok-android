package id.gumilombok.common

import android.app.Activity
import android.app.TaskStackBuilder
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import id.gumilombok.R
import java.io.Serializable

/**
 * Created by wisnu on 11/28/18
 */

/**
 * Get layout inflater using context that is wrapped by app theme style. Always use this layout
 * inflater to avoid crash in some devices. Return null if context passed is null.
 */
val Context.inflater: LayoutInflater
    get() {
        val contextWrapped = ContextThemeWrapper(this, R.style.AppTheme)
        return LayoutInflater.from(contextWrapped)
    }

/**
 * Check if device is connected to the internet.
 */
val Context.isConnectedInternet: Boolean
    get() {
        val cm = this.systemService<ConnectivityManager>(Context.CONNECTIVITY_SERVICE)
        val ni: NetworkInfo? = cm?.activeNetworkInfo
        return (ni != null && ni.isConnected)
    }

/**
 * Show soft input keyboard.
 */
fun Context.showKeyboard() {
    val inputMethodManager = systemService<InputMethodManager>(Context.INPUT_METHOD_SERVICE)
    inputMethodManager?.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

inline fun <reified T> Context.systemService(systemService: String): T? {
    return getSystemService(systemService) as T
}

/**
 * Safely get color from resources
 */
fun Context?.getMyColor(@ColorRes id: Int): Int {
    this?.let {
        return ContextCompat.getColor(it, id)
    }
    return 0
}

/**
 * Safely get drawable from resources
 */
fun Context?.getMyDrawable(@DrawableRes id: Int): Drawable? {
    this?.let {
        return ContextCompat.getDrawable(it, id)
    }

    return null
}

fun Context.dpToPx(dp: Int): Int =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics).toInt()

fun Context.pxToDp(px: Int): Int =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, px.toFloat(), resources.displayMetrics).toInt()

fun Context.gotoActivityWithBackStack(from: Class<out Activity>, to: Class<out Activity>): TaskStackBuilder {
    return TaskStackBuilder
        .create(this)
        .addNextIntent(Intent(this, from))
        .addNextIntent(
            Intent(
                this,
                to
            ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        )
}

fun Context.hasPermission(permissions: Array<String>): Boolean {
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        !permissions.any { ActivityCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED }
    }
    return true
}

inline fun <reified T : Any> Context.intentFor(vararg params: Pair<String, Any?>): Intent {
    val intent = Intent(this, T::class.java)
    params.forEach {
        val value = it.second
        when (value) {
            null -> intent.putExtra(it.first, null as Serializable?)
            is Int -> intent.putExtra(it.first, value)
            is Long -> intent.putExtra(it.first, value)
            is CharSequence -> intent.putExtra(it.first, value)
            is String -> intent.putExtra(it.first, value)
            is Float -> intent.putExtra(it.first, value)
            is Double -> intent.putExtra(it.first, value)
            is Char -> intent.putExtra(it.first, value)
            is Short -> intent.putExtra(it.first, value)
            is Boolean -> intent.putExtra(it.first, value)
            is Serializable -> intent.putExtra(it.first, value)
            is Bundle -> intent.putExtra(it.first, value)
            is Parcelable -> intent.putExtra(it.first, value)
            is Array<*> -> when {
                value.isArrayOf<CharSequence>() -> intent.putExtra(it.first, value)
                value.isArrayOf<String>() -> intent.putExtra(it.first, value)
                value.isArrayOf<Parcelable>() -> intent.putExtra(it.first, value)
                else -> throw RuntimeException("Intent extra ${it.first} has wrong type ${value.javaClass.name}")
            }
            is IntArray -> intent.putExtra(it.first, value)
            is LongArray -> intent.putExtra(it.first, value)
            is FloatArray -> intent.putExtra(it.first, value)
            is DoubleArray -> intent.putExtra(it.first, value)
            is CharArray -> intent.putExtra(it.first, value)
            is ShortArray -> intent.putExtra(it.first, value)
            is BooleanArray -> intent.putExtra(it.first, value)
            else -> throw RuntimeException("Intent extra ${it.first} has wrong type ${value.javaClass.name}")
        }
    }
    return intent
}

fun Context.toast(message: String?): Toast {
    return Toast.makeText(this, message, Toast.LENGTH_SHORT).apply {
        show()
    }
}

fun Context.toast(message: String?, gravity: Int): Toast {
    return Toast.makeText(this, message, Toast.LENGTH_SHORT).apply {
        setGravity(gravity, 0, 0)
        show()
    }
}

fun Context.centerToast(message: String?) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}

fun Context.longToast(message: String?) = Toast.makeText(this, message, Toast.LENGTH_LONG).show()

fun Context.copyText(text: String, label: String = "label") {
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText(label, text)
    clipboard.setPrimaryClip(clip)}
