package id.gumilombok.common

fun String.toCamelCase(): String {
    this.toLowerCase()
    return if (this.isNotEmpty()) {
        var newChars = ""
        this.toCharArray().forEachIndexed { i, c ->
            var nC = c
            if (i == 0) nC = c.toUpperCase()
            if (i > 0) {
                if (this[i - 1].toString() == " ") {
                    nC = c.toUpperCase()
                }
            }
            newChars += nC
        }
        return newChars
    } else {
        this
    }
}

fun String?.toIntOrZero(): Int {
    this?.also {
        return if (it.isEmpty()) 0 else it.toInt()
    }
    return 0
}