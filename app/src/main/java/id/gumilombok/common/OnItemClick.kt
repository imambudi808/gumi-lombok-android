package id.gumilombok.common

interface OnItemClick<in T> {
    fun onItemClick(item: T?, position: Int?){}
}