package id.gumilombok.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Parcelable
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import id.gumilombok.presentation.base.view.BaseView
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import java.io.File
import java.io.IOException
import java.io.Serializable
import java.math.BigInteger
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


const val tagUtils = "Utilities"




/*use this method to hide soft key board*/
fun hideSoftKeyboard(activity: Activity?) {
    val imm: InputMethodManager? = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm?.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)
}

/*use this to check network is connected*/
fun isNetworkAvailable(context: Context): Boolean {
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val ni: NetworkInfo? = cm.activeNetworkInfo
    return (ni != null && ni.isConnected)
}


/*use this to handle error parsing json*/
fun doOnError(view: BaseView?, throwable: Throwable) {
    throwable.printStackTrace()
    view?.let {
        var message = ""
        when (throwable) {
            is HttpException -> {
                try {
                    val body = throwable.response()?.errorBody()
                    val jsonObject = JSONObject(body?.string())
                    message = "Maaf, ${jsonObject.getString("pesan")}"//getting value of pesan
                } catch (e: Exception) {
                    message = "Terjadi kesalahan pada server, coba lagi nanti!"
                    e.printStackTrace()
                }
            }
            is JsonSyntaxException -> {
                message = "Maaf, terjadi kesalahan saat membaca respon dari server!"
            }
            is SocketTimeoutException -> {
                message = "Koneksi ke server gagal, mohon periksa koneksi anda!"
            }
            is UnknownHostException -> {
                message = "Tidak ada koneksi internet!"
            }
            is IOException -> {
                message = "Tidak bisa mendapat respon apapun dari server!"
            }
            is JSONException -> {
                message = "Invalid json format"
            }
        }
        it.showProgress(false)
        if (message.isNotBlank()) {
            it.showMessage(message)
        }
    }
}

/*this method is for making printer output alignment justify*/
fun justifyPrintLine(leftText: String? = "", rightText: String? = "") : String{
    //dik : total char per line = 31 chars
    //dit : buat print line biar rata kiri dan kanan tanpa pindah baris
    /*solusi :
    1. a = hitung jumlah karakter key (left text)
    2. b = hitung jumlah karakter value (right text)
    3. total_chars = a+b
    4. total whitespace = 30 - total_chars
    5. lakukan loping untuk mencetak whitespace
    6. gabungkan : a+whitespace+b */

    val totalChars: Int = rightText?.length?.let { leftText?.length?.plus(it) } ?: 0
    val whiteSpaceChars = 30 - totalChars
    return if (whiteSpaceChars >= 0) {
        var whiteSpace = ""
        (0..whiteSpaceChars).forEach {
            whiteSpace += " "
        }
        "$leftText$whiteSpace$rightText"
    } else {
        "$leftText $rightText"
    }
}

/*use this to validate multiple string variables*/
fun isNullOrEmpty(strings: Array<String?>): Boolean {
    Log.i(tagUtils, "strings : ${Gson().toJsonTree(strings)}")
    strings.forEach {
        if (it.isNullOrEmpty()) {
            return true
        }
    }
    return false
}

/*use this to validate email*/
fun isValidEmail(email: String?): Boolean {
    if (email.isNullOrEmpty()) return false

    val emailPattern = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    val pattern = Pattern.compile(emailPattern)
    val matcher = pattern.matcher(email)
    return matcher.matches()
}

/*use this ti validate phone number*/
fun isValidPhoneNumber(phoneNumber: String?): Boolean {
    if (phoneNumber.isNullOrEmpty()) return false
    if (!TextUtils.isDigitsOnly(phoneNumber)) return false
    if (phoneNumber?.length?:0 < 10 || phoneNumber?.length?:0 > 14) return false
    return true
}



/*use this to get current day*/
fun getCurrentDay(): String {
    val days: Array<String> = arrayOf("Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu")
    return days[Calendar.DAY_OF_WEEK.minus(1)]
}


/*memotong karakter layanan*/
fun getNewLayanan(layanan: String?): String {

    var nama=layanan

    // Cek berapa banyak spasi di dalam nama
    val spaceCount = (nama?.length as Int) - (nama?.replace(" ","")?.length)
    if(spaceCount>2) {
        // Memotong kalimat dengan batas kemunculan spasi kedua
        nama = nama.substring(0, nama.indexOf(" ", nama.indexOf(" ")+2))

        // Memotong string hanya beberapa karakter
//         nama = nama.substring(0, 14)
    }

    return nama
}

/*convert yyyy-MM-dd HH:mm:ss -> dd-MM-yyyy*/
fun convertDateFormat(oldDate: String?): String {
    try {
        val oldSdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val newSdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        return oldDate?.let { if (it.isNotEmpty()) newSdf.format(oldSdf.parse(it)) else return ""}?: ""
    } catch (e: ParseException) {
        e.printStackTrace()
        return ""
    }
}

fun convertDateFormat2(oldDate: String?): String {
    try {
        val oldSdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val newSdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        return oldDate?.let { if (it.isNotEmpty()) newSdf.format(oldSdf.parse(it)) else return ""}?: ""
    } catch (e: ParseException) {
        e.printStackTrace()
        return ""
    }
}

fun convertDateTimeFormat(oldDate: String?): String {
    try {
        val oldSdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val newSdf = SimpleDateFormat("dd MMM yyyy, HH:mm", Locale.getDefault())
        return oldDate?.let { if (it.isNotEmpty()) newSdf.format(oldSdf.parse(it)) else return ""}?: ""
    } catch (e: ParseException) {
        e.printStackTrace()
        return oldDate ?: ""
    }
}

fun convertDateTimeFormat2(oldDate: String?): String {
    try {
        val oldSdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val newSdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault())
        return oldDate?.let { if (it.isNotEmpty()) newSdf.format(oldSdf.parse(it)) else return ""}?: ""
    } catch (e: ParseException) {
        e.printStackTrace()
        return oldDate ?: ""
    }
}

/*convert yyyy-MM-dd HH:mm:ss -> HH:mm*/
fun convertTimeFormat(oldDate: String?): String {
    try {
        val oldSdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val newSdf = SimpleDateFormat("HH:mm", Locale.getDefault())
        return oldDate?.let { if (it.isNotEmpty()) newSdf.format(oldSdf.parse(it)) else return ""}?: ""
    } catch (e: ParseException) {
        e.printStackTrace()
        return ""
    }
}

/*date format converter*/
fun convertDateFormat(date: String?, oldFormat: String, newFormat: String): String {
    try {
        val oldSdf = SimpleDateFormat(oldFormat, Locale.getDefault())
        val newSdf = SimpleDateFormat(newFormat, Locale.getDefault())
        return date?.let { if (it.isNotEmpty()) newSdf.format(oldSdf.parse(it)) else return ""}?: ""
    } catch (e: ParseException) {
        e.printStackTrace()
        return ""
    }
}

fun convertToDate(time: String?): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
    val output = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
    var date: Date? = null
    try {
        date = sdf.parse(time)
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return output.format(date)
}

fun getCurrentCalendar(): String = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    .format(Calendar.getInstance().time)

fun getCurrentDate(): String = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date())

fun getCurrentDateYMD(): String = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())

fun getCurrentHour(): String = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date())

fun getCurrentWeek(): String {
    // Get calendar set to current date and time
    var c = GregorianCalendar.getInstance()

    // Set the calendar to monday of the current week
    c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)

    // Print dates of the current week starting on Monday
    val df = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    val startDate = df.format(c.time)

    c = Calendar.getInstance(Locale.getDefault())
    val endDate = df.format(c.time)

    return startDate + " - " + endDate
}

fun getCurrentMonth(): String {
    val c = Calendar.getInstance(Locale.getDefault())
    return SimpleDateFormat("MMMM", Locale.getDefault()).format(c.time)
}

//val monthParse = SimpleDateFormat("MM",locale)
//val monthDisplay = SimpleDateFormat("MMMM",locale)
//return (1..12).map {
//    val month = monthDisplay.format(monthParse.parse(it.toString())!!)
//    Pair(it,month)
//}

fun getCurrentMonth3String(): String {

//    val locale=Locale.getDefault()
//    val monthParse = SimpleDateFormat("MM",locale)
//    val monthDisplay = SimpleDateFormat("MMMM",locale)
    val c = Calendar.getInstance(Locale.getDefault())

    return SimpleDateFormat("MMMM", Locale.getDefault()).format(c.time)
}

fun changeDatePosition(date: String?) : String {
    // ubah yyyy-MM-dd menjadi dd-MM-yyyy atau sebaliknya
    val mArray = date?.split("-")?.toTypedArray()
    return (mArray?.get(2) + "-" + mArray?.get(1) + "-" + mArray?.get(0))
}

fun convertMonthToBahasa(engMonth: String?): String? {
    val monthBulan = mutableMapOf<String,String>()

    monthBulan.put("January", "Januari")
    monthBulan.put("February", "Februari")
    monthBulan.put("March", "Maret")
    monthBulan.put("April", "April")
    monthBulan.put("May", "Mei")
    monthBulan.put("June", "Juni")
    monthBulan.put("July", "Juli")
    monthBulan.put("August", "Agustus")
    monthBulan.put("September", "September")
    monthBulan.put("October", "Oktober")
    monthBulan.put("November", "November")
    monthBulan.put("December", "December")
    //Test Dimas
    monthBulan.put("Januari", "Januari")
    monthBulan.put("Februari", "Februari")
    monthBulan.put("Maret", "Maret")
    monthBulan.put("April", "April")
    monthBulan.put("Mei", "Mei")
    monthBulan.put("Juni", "Juni")
    monthBulan.put("Juli", "Juli")
    monthBulan.put("Agustus", "Agustus")
    monthBulan.put("September", "September")
    monthBulan.put("Oktober", "Oktober")
    monthBulan.put("November", "November")
    monthBulan.put("Desember", "Desember")


    return monthBulan.get(engMonth)
}

/*convert distance. by default distance unit is meter*/
fun convertDistanceUnit(distance: String?) : String{
    if (distance == null) return "-"

    return if (distance.toDouble() > 500) {
        "%.1f km".format((distance.toDouble()/1000))
    } else {
        "$distance m"
    }
}

fun convertToMD5(password: String): String {
    var newPassword = password
    val mdEnc: MessageDigest
    try {
        mdEnc = MessageDigest.getInstance("MD5")
        mdEnc.update(password.toByteArray(), 0, password.length)
        newPassword = BigInteger(1, mdEnc.digest()).toString(16)
        while (password.length < 32) {
            newPassword = "0" + password
        }
    } catch (e1: NoSuchAlgorithmException) {
        e1.printStackTrace()
    }

    return newPassword
}

fun convertToDateTimeFormat(time: Long): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
    sdf.timeZone = TimeZone.getDefault()
    return sdf.format(Date(time))
}

fun convertToTimeDateFormat(time: Long): String {
    val sdf = SimpleDateFormat("HH:mm dd/MM/yyyy", Locale.getDefault())
    sdf.timeZone = TimeZone.getDefault()
    return sdf.format(Date(time))
}

fun convertToDateFormat(time: Long): String{
    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    sdf.timeZone = TimeZone.getDefault()
    return sdf.format(Date(time))
}

fun fromHtml(s: String): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(s, Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        Html.fromHtml(s)
    }
}

fun toCurrency(number: Int?): String {
    val num = NumberFormat.getInstance()
    return num.format(number?.toDouble()).replace(",", ".")
}

fun toCurrency(number: Double?): String {
    val num = NumberFormat.getInstance()
    return num.format(number).replace(",", ".")
}

fun formattedNumber(number: String?): String {

    try {
        val decimalVal = number?.toDouble()
        val subTotalInt = decimalVal?.toInt()
        return toCurrency(subTotalInt?:0)
    }catch (e:Exception){
        val decimalVal = number?.toDouble()
        val subTotalInt = decimalVal
        return subTotalInt.toString()
    }

}

fun fromCurrency(currency: String?): String? {
    val result = currency?.replace(".", "")
    return result?.replace(",", "")
}

fun getTahunList(startYear: Int): List<String> {
    val cal = Calendar.getInstance()
    val curYear = cal.get(Calendar.YEAR)
    return (curYear.downTo(startYear)).map { it.toString() }
}

fun getBulanList(): List<String> {
    val bulanList = ArrayList<String>()
    bulanList.add("Januari")
    bulanList.add("Februari")
    bulanList.add("Maret")
    bulanList.add("April")
    bulanList.add("Mei")
    bulanList.add("Juni")
    bulanList.add("Juli")
    bulanList.add("Agustus")
    bulanList.add("September")
    bulanList.add("Oktober")
    bulanList.add("November")
    bulanList.add("Desember")
    return bulanList
}


fun daysBetweenDates(start: String, end: String): Int {
    val myFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var daysBetween = .0f
    var difference: Long = 0

    try {
        val dateBefore = myFormat.parse(start)
        val dateAfter = myFormat.parse(end)
        difference = dateAfter.time - dateBefore.time
        daysBetween = (difference / (1000 * 60 * 60 * 24)).toFloat()
    } catch (e: Exception) {
        e.printStackTrace()
    }

    if(difference < 0) {
        daysBetween = -1.0f
    }

    return daysBetween.toInt()
}

fun checkSameDay(firstDate: String, secondDate: String): Boolean {
    var isSameDay = false
    val myFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val dateBefore = myFormat.parse(firstDate)
    val dateAfter = myFormat.parse(secondDate)


    val fmt = SimpleDateFormat("yyyy-MM-dd")

    if(fmt.format(dateBefore) == (fmt.format(dateAfter))) {
        isSameDay = true
    }

    return isSameDay
}

fun convertToDateObject(strDate: String) : Date {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    val date = dateFormat.parse(strDate)
    return date
}

fun checkFiturPesanAntarByState(isEnable : Boolean) : String {
    return if(isEnable) "Fitur Pesan Antar" else "Tidak Ada Fitur Pesan Antar"
}

fun NestedScrollView.setOnScrollListener(onScroll: (v: View, x: Int, y: Int, oldX: Int, oldY: Int) -> Unit) {
    setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
        onScroll(v, scrollX, scrollY, oldScrollX, oldScrollY)
    })
}

fun ImageView.loadImage(url: String?) {
    Glide.with(this).load(url).into(this)
}

fun ImageView.loadImage(id: Int) {
    Glide.with(this).load(id).into(this)
}

fun ImageView.loadImage(url: String?, id: Int) {
    Glide.with(this)
        .load(url)
        .placeholder(id)
        .error(id)
        .into(this)
}
//fragment util
inline fun <reified T : Activity> Fragment.startActivity(vararg params: Pair<String, Any?>) {
    this.context?.startActivity<T>(*params)
}

inline fun <reified T : Any> Fragment.intentFor(vararg params: Pair<String, Any?>): Intent? {
    val intent = Intent(activity, T::class.java)
    params.forEach {
        val value = it.second
        when (value) {
            null -> intent.putExtra(it.first, null as Serializable?)
            is Int -> intent.putExtra(it.first, value)
            is Long -> intent.putExtra(it.first, value)
            is CharSequence -> intent.putExtra(it.first, value)
            is String -> intent.putExtra(it.first, value)
            is Float -> intent.putExtra(it.first, value)
            is Double -> intent.putExtra(it.first, value)
            is Char -> intent.putExtra(it.first, value)
            is Short -> intent.putExtra(it.first, value)
            is Boolean -> intent.putExtra(it.first, value)
            is Serializable -> intent.putExtra(it.first, value)
            is Bundle -> intent.putExtra(it.first, value)
            is Parcelable -> intent.putExtra(it.first, value)
            is Array<*> -> when {
                value.isArrayOf<CharSequence>() -> intent.putExtra(it.first, value)
                value.isArrayOf<String>() -> intent.putExtra(it.first, value)
                value.isArrayOf<Parcelable>() -> intent.putExtra(it.first, value)
                else -> throw RuntimeException("Intent extra ${it.first} has wrong type ${value.javaClass.name}")
            }
            is IntArray -> intent.putExtra(it.first, value)
            is LongArray -> intent.putExtra(it.first, value)
            is FloatArray -> intent.putExtra(it.first, value)
            is DoubleArray -> intent.putExtra(it.first, value)
            is CharArray -> intent.putExtra(it.first, value)
            is ShortArray -> intent.putExtra(it.first, value)
            is BooleanArray -> intent.putExtra(it.first, value)
            else -> throw RuntimeException("Intent extra ${it.first} has wrong type ${value.javaClass.name}")
        }
    }
    return intent
}

fun Fragment.toast(message: String?) = context?.toast(message)

fun Fragment.finish() = this.activity?.finish()

fun Fragment.string(resId: Int): String = this.activity?.getString(resId).orEmpty()

