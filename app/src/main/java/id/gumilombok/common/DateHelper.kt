package id.gumilombok.common

import java.text.SimpleDateFormat
import java.util.*

object DateHelper {
    fun getDateList(locale: Locale):List<Pair<Int,String>>{
        val dateParse = SimpleDateFormat("d",locale)
        val dateDisplay = SimpleDateFormat("dd",locale)
        return (1..31).map {
            val date = dateDisplay.format(dateParse.parse(it.toString())!!)
            Pair(it,date)
        }
    }




}