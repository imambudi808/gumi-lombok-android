package id.gumilombok.common

import android.util.Patterns

class PhoneNumberValidator {
    companion object {
        fun isNumberValid(email: String): Boolean {
            return Patterns.PHONE.toRegex().matches(email);
        }
    }
}