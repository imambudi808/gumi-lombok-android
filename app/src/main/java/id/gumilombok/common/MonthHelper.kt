package id.gumilombok.common

import java.lang.RuntimeException
import java.text.SimpleDateFormat
import java.util.*

object MonthHelper {
    fun getMonthList(locale:Locale):List<Pair<Int,String>>{
        val monthParse = SimpleDateFormat("MM",locale)
        val monthDisplay = SimpleDateFormat("MMMM",locale)
        return (1..12).map {
            val month = monthDisplay.format(monthParse.parse(it.toString())!!)
            Pair(it,month)
        }
    }

    /**
     * Used for returning display name of given index of month
     * @link index : month index like 1 for January, 2 for February, etc
     * @link locale : get name by given locale
     * */

    fun getMonthByIndex(locale: Locale, index: Int): String {
        val monthParse = SimpleDateFormat("MM", locale)
        val monthDisplay = SimpleDateFormat("MMMM", locale)
        val date = monthParse.parse(index.toString())
        if (date != null)
            return monthDisplay.format(date)
        else
            throw RuntimeException("Invalid given index")
    }
}