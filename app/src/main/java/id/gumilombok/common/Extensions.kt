package id.gumilombok.common

import android.app.Activity
import android.content.Context

inline fun <reified T : Activity> Context.startActivity(vararg params: Pair<String, Any?>) = startActivity(intentFor<T>(*params))