package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.BlogService
import id.gumilombok.data.remote.model.BlogModel
import io.reactivex.Flowable
import javax.inject.Inject

class BlogRepository @Inject constructor(val service:BlogService,
                                         val sp:PreferencesManager) {
    fun blogList(offset:String?,limit:String?):Flowable<List<BlogModel>>{
        return service.loadBlog(offset,limit)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<BlogModel>::class.java)
                .toCollection(ArrayList<BlogModel>())) }
    }

    fun blogsLists(map:Map<String,String>):Flowable<List<BlogModel>>{
        return service.loadsBlogs(map)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<BlogModel>::class.java)
                .toCollection(ArrayList<BlogModel>())) }
    }

    fun loadHotBlog():Flowable<List<BlogModel>>{
        return service.loadsHotBlog()
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<BlogModel>::class.java)
                .toCollection(ArrayList<BlogModel>())) }
    }

    fun loadBlogByCategory(map: Map<String, String>):Flowable<List<BlogModel>>{
        return service.loadBlogByCategory(map)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<BlogModel>::class.java)
                .toCollection(ArrayList<BlogModel>())) }
    }

}