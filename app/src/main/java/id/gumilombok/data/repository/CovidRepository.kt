package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.CovidService
import id.gumilombok.data.remote.model.CovidModel
import io.reactivex.Flowable
import javax.inject.Inject

class CovidRepository @Inject constructor(val service:CovidService,
                                            val sp:PreferencesManager) {
    fun showCovidId():Flowable<List<CovidModel>>{
        return service.loadCovidData()
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<CovidModel>::class.java)
                .toCollection(ArrayList<CovidModel>())) }
    }
}