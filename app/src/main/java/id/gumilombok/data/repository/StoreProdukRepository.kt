package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.remote.StoreProdukService
import id.gumilombok.data.remote.model.StoreModel
import id.gumilombok.data.remote.model.StoreProdukModel
import io.reactivex.Flowable
import javax.inject.Inject

class StoreProdukRepository @Inject constructor(val service:StoreProdukService) {

    //showproduk by id store

    //show new produk
    fun loadNewProduk():Flowable<List<StoreProdukModel>>{
        return service.loadNewProduk()
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<StoreProdukModel>::class.java)
                .toCollection(ArrayList<StoreProdukModel>())) }
    }

    fun loadProdukByStoreId(map:Map<String,String>):Flowable<List<StoreProdukModel>>{
        return service.loadProdukByStoreId(map)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<StoreProdukModel>::class.java)
                .toCollection(ArrayList<StoreProdukModel>())) }
    }
}