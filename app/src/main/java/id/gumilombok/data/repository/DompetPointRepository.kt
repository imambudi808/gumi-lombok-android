package id.gumilombok.data.repository

import com.google.gson.JsonElement
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.DompetPointService
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton


class DompetPointRepository @Inject constructor(val service: DompetPointService,
                                                val sp :PreferencesManager) {

    fun savePointToDompetPoint(map: HashMap<String,String?>):Flowable<Map<String,String>>
        = service.savePointToDompet(map)
}