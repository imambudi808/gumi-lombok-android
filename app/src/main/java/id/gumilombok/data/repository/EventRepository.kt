package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.remote.EventService
import id.gumilombok.data.remote.model.EventModel
import io.reactivex.Flowable
import javax.inject.Inject

class EventRepository @Inject constructor(val service:EventService){
    fun eventList(map: Map<String,String>):Flowable<List<EventModel>>{
        return service.loadEventList(map)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<EventModel>::class.java)
                .toCollection(ArrayList<EventModel>())) }
    }
}