package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.DestinationService
import id.gumilombok.data.remote.model.DestinationModel
import id.gumilombok.data.remote.model.PhotoModel
import io.reactivex.Flowable
import javax.inject.Inject

class DestinationRepository @Inject constructor(val service: DestinationService,
                                                val sp:PreferencesManager) {
    fun destinationList(map: Map<String,String?>):Flowable<List<DestinationModel>>{
        return service.loadDestinationList(map)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<DestinationModel>::class.java)
                .toCollection(ArrayList<DestinationModel>())) }
    }

    fun loadDesPhotos(destId:String?): Flowable<List<PhotoModel>>{
        return service.loadDesPhoto(destId)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<PhotoModel>::class.java)
                .toCollection(ArrayList<PhotoModel>())) }}

    fun newDes():Flowable<ArrayList<DestinationModel>>{
        return service.newDestination()
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<DestinationModel>::class.java)
                .toCollection(ArrayList<DestinationModel>())) }
    }

    fun pdateDestinationRate(destId: String?,rataRating:String?,jumlahResponse:String?):Flowable<List<DestinationModel>>{
        return service.updateDestinationRate(destId,rataRating,jumlahResponse)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<DestinationModel>::class.java)
                .toCollection(ArrayList<DestinationModel>())) }
    }
}