package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.StoreReviewService
import id.gumilombok.data.remote.model.StoreReviewModel
import io.reactivex.Flowable
import javax.inject.Inject

class StoreReviewRepository @Inject constructor(val service:StoreReviewService,val sp:PreferencesManager) {

    //show all review
    fun storeReviewList(map: Map<String,String>):Flowable<List<StoreReviewModel>>{
        return service.loadStoreReviewByIdStore(map)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<StoreReviewModel>::class.java)
                .toCollection(ArrayList<StoreReviewModel>())) }
    }

}