package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.StoreService
import id.gumilombok.data.remote.model.StoreModel
import io.reactivex.Flowable
import javax.inject.Inject

class StoreRepository @Inject constructor(val service:StoreService,val sp:PreferencesManager){

    //allstore
    fun storeList(map:Map<String,String>):Flowable<List<StoreModel>>{
        return service.loadAllStore(map)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<StoreModel>::class.java)
                .toCollection(ArrayList<StoreModel>())) }
    }
    //newStore
    fun loadNewStore():Flowable<List<StoreModel>>{
        return service.loadNewStore()
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<StoreModel>::class.java)
                .toCollection(ArrayList<StoreModel>())) }
    }
}