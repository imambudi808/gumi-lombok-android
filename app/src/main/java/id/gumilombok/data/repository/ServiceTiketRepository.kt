package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.ServiceTiketService
import id.gumilombok.data.remote.model.ServiceModel

import id.gumilombok.data.remote.model.TiketModel
import io.reactivex.Flowable
import javax.inject.Inject

class ServiceTiketRepository @Inject constructor(val service: ServiceTiketService,
                                          val sp: PreferencesManager
) {
    fun tiketServiceList(map: Map<String,String?>): Flowable<List<ServiceModel>> {
        return service.loadservicetiketbyidtiket(map)
            .flatMap { Flowable.just(
                Gson().fromJson(Gson().toJsonTree(it.data),Array<ServiceModel>::class.java)
                .toCollection(ArrayList<ServiceModel>())) }
    }



}