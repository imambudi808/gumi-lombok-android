package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.TiketService
import id.gumilombok.data.remote.model.TiketModel
import io.reactivex.Flowable
import javax.inject.Inject

class TiketRepository @Inject constructor(val service:TiketService,
                                          val sp:PreferencesManager) {
    fun tiketList(map: Map<String,String?>):Flowable<List<TiketModel>>{
        return service.loadlisttiket(map)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<TiketModel>::class.java)
                .toCollection(ArrayList<TiketModel>())) }
    }



}