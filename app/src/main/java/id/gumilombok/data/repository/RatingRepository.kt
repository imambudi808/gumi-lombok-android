package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.RatingService
import id.gumilombok.data.remote.model.RatingModel
import id.gumilombok.data.remote.response.CekPointResponse
import id.gumilombok.data.remote.response.CekRatingResponse
import io.reactivex.Flowable
import javax.inject.Inject

class RatingRepository @Inject constructor(val service:RatingService,
                                           val sp:PreferencesManager){
    fun loadRatings(desId:String?):Flowable<List<RatingModel>>{
        return service.loadrating(desId)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<RatingModel>::class.java)
                .toCollection(ArrayList<RatingModel>())) }
    }

    fun insertRating(ratingValue:String?,ratingComent:String?,desId:String?,userId:String?):Flowable<List<RatingModel>>{
        return service.insertRating(ratingValue,ratingComent,desId,userId)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<RatingModel>::class.java)
                .toCollection(ArrayList<RatingModel>())) }
    }

    fun updateRating(desId:String?):Flowable<List<RatingModel>>{
        return service.update_rating(desId)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<RatingModel>::class.java)
                .toCollection(ArrayList<RatingModel>())) }
    }

    fun cekRating(userId: String,desId: String?):Flowable<CekRatingResponse>{
        return service.cek_rating(userId,desId)
    }

    fun updateRatingUser(userId: String,desId: String,comment:String?):Flowable<List<RatingModel>>{
        return service.updateRatingUser(userId,desId,comment)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<RatingModel>::class.java)
                .toCollection(ArrayList<RatingModel>())) }
    }

}