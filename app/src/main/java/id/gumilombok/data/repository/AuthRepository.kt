package id.gumilombok.data.repository

import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.AuthService
import id.gumilombok.data.remote.post.AuthPost
import id.gumilombok.data.remote.response.CekPointResponse
import id.gumilombok.data.remote.response.LoginBaseResponse
import id.gumilombok.data.remote.response.OrderBaseResponse
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRepository @Inject constructor(val service: AuthService,
                                         val sp:PreferencesManager) {
    fun login(data:AuthPost): Flowable<LoginBaseResponse> {
        return service.login(data)
    }

    fun register(nama:String,email: String,password: String,confirPassword:String):Flowable<LoginBaseResponse>{
        return  service.register(nama,email,password,confirPassword)
    }

    fun updateProfile(userId:String,nama:String,userPhone:String,email: String):Flowable<LoginBaseResponse>{
        return  service.updateProfile(userId,nama,userPhone,email)
    }

    fun updatePassword(userId:String,passwordBaru: String):Flowable<LoginBaseResponse>{
        return  service.updatePassword(userId,passwordBaru)
    }

    fun cekPoint(userId: String):Flowable<CekPointResponse>{
        return service.cekSisaPoint(userId)
    }


}