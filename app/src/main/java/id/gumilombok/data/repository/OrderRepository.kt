package id.gumilombok.data.repository

import com.google.gson.Gson

import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.OrderService
import id.gumilombok.data.remote.model.HistoriModel

import id.gumilombok.data.remote.model.OrderModel
import id.gumilombok.data.remote.response.OrderBaseResponse
import id.gumilombok.presentation.model.Order
import io.reactivex.Flowable
import javax.inject.Inject

class OrderRepository @Inject constructor(val service:OrderService,
                                          val sp:PreferencesManager){
    fun destinationInsertedList(map: Map<String,String?>): Flowable<OrderBaseResponse> = service.loadInsertedOrder(map)

    fun updateMetodeBayar(map: Map<String,String?>): Flowable<OrderBaseResponse> = service.updateMetodePembayaran(map)

    fun konfirmasiSudahBayar(map: Map<String,String?>): Flowable<OrderBaseResponse> = service.konfirmasiSudahBayar(map)

    fun orderList(map: Map<String,String?>):Flowable<List<HistoriModel>>{
        return service.loadOrderAll(map)
            .flatMap { Flowable.just(Gson().fromJson(Gson().toJsonTree(it.data),Array<HistoriModel>::class.java)
                .toCollection(ArrayList<HistoriModel>())) }
    }




}