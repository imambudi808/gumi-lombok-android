package id.gumilombok.data.repository

import com.google.gson.Gson
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.DestinationService
import id.gumilombok.data.remote.PointService
import id.gumilombok.data.remote.model.DestinationModel
import id.gumilombok.data.remote.model.PointModel
import id.gumilombok.data.remote.response.PointBaseResponse
import io.reactivex.Flowable
import javax.inject.Inject

class PointRepository @Inject constructor(val service: PointService,
                                val sp: PreferencesManager
) {
    fun pointListByIdDes(destId:String?): Flowable<PointBaseResponse>
    =service.loadPointByIdDes(destId)
}