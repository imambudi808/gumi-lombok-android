package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class IslandModel (

    @field:SerializedName("island_id")
    var IslandId:String?="",

    @field:SerializedName("island_name")
    var IslandName:String?="",

    @field:SerializedName("created_at")
    var createdAt:String?=""

):Parcelable