package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BlogModel(
    @field:SerializedName("blog_id")
    val blogId:String?="",

    @field:SerializedName("blog_judul")
    val blogJudul:String?="",

    @field:SerializedName("blog_creator")
    val blogCreator:String?="",

    @field:SerializedName("blog_creator_des")
    val blogCreatorDes:String?="",

    @field:SerializedName("blog_creator_image")
    val blogCreatorImage:String?="",

    @field:SerializedName("blog_image")
    val blogImage:String?="",

    @field:SerializedName("blog_isi")
    val blogIsi:String?="",

    @field:SerializedName("blog_category")
    val blogCategory:String?="",

    @field:SerializedName("total_view")
    val blogView:String?="",

    @field:SerializedName("total_comment")
    val totalComment:String?="",

    @field:SerializedName("created_at")
    val createdAt:String?="",

    @field:SerializedName("seo_link")
    val seoLink:String?=""
) : Parcelable