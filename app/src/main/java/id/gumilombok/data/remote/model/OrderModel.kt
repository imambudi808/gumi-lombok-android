package id.gumilombok.data.remote.model


import com.google.gson.annotations.SerializedName
import id.gumilombok.presentation.model.Order


data class OrderModel (
    @SerializedName("order_id")
    var orderId:String?="",

    @SerializedName("order_code")
    var orderCode:String?="",

    @SerializedName("tiket_id")
    var tiketId:String?="",

    @SerializedName("tiket_name")
    var tiketName:String?="",

    @SerializedName("tiket_route")
    var tiketRute:String?="",

    @SerializedName("tiket_agent")
    var tiketAgent:String?="",

    @SerializedName("tiket_price_diskon")
    var tiketPriceDiskon:String?="",

    @SerializedName("tiket_keberangkatan")
    var tiketKeberangkatan:String?="",

    @SerializedName("tiket_tiba")
    var tiketTiba:String?="",

    @SerializedName("tiket_detail")
    var tiketDetail:String?="",

    @SerializedName("durasi_tiket")
    var durasiTiket:String?="",

    @SerializedName("order_tiket_book")
    var orderTiketBook:String?="",

    @SerializedName("order_status")
    var orderStatus:String?="",

    @SerializedName("created_at")
    var createdAt:String?="",

    @SerializedName("updated_at")
    var updatedAt:String?="",

    @SerializedName("trash")
    var trash:String?="",

    @SerializedName("metode_bayar")
    var metodeBayar:String?="",

    @SerializedName("bank_tujuan")
    var bankTujuan:String?="",

    @SerializedName("norek")
    var norekening:String?="",

    @SerializedName("card_holder")
    var cardHolder:String?="",

    @SerializedName("nama_pemilik_rekening")
    var namaPemilikRekening:String?="",

    @SerializedName("tiket_price_adult")
    var tiketPriceAdult:String?="",

    @SerializedName("tiket_price_chil")
    var tiketPriceChil:String?="",

    @SerializedName("jumlah_adult")
    var jumlahAdult:String?="",

    @SerializedName("jumlah_anak")
    var jumlahAnak:String?="",

    @SerializedName("total_bayar")
    var totalBayar:String?="",

    @SerializedName("user_id")
    var userId:String?=""
) {
    fun build() = Order.build {
        this.orderId = this@OrderModel.orderId
        this.orderCode = this@OrderModel.orderCode
        this.tiketId = this@OrderModel.tiketId
        this.tiketName = this@OrderModel.tiketName
        this.tiketRute = this@OrderModel.tiketRute
        this.tiketAgent = this@OrderModel.tiketAgent
        this.tiketPriceDiskon = this@OrderModel.tiketPriceDiskon
        this.tiketKeberangkatan = this@OrderModel.tiketKeberangkatan
        this.tiketTiba=this@OrderModel.tiketTiba
        this.tiketDetail=this@OrderModel.tiketDetail
        this.durasiTiket=this@OrderModel.durasiTiket
        this.orderTiketBook=this@OrderModel.orderTiketBook
        this.orderStatus=this@OrderModel.orderStatus
        this.createdAt=this@OrderModel.createdAt
        this.updatedAt=this@OrderModel.updatedAt
        this.trash=this@OrderModel.trash
        this.metodeBayar=this@OrderModel.metodeBayar
        this.bankTujuan=this@OrderModel.bankTujuan
        this.norekening=this@OrderModel.norekening
        this.cardHolder=this@OrderModel.cardHolder
        this.namaPemilikRekening=this@OrderModel.namaPemilikRekening
        this.tiketPriceAdult=this@OrderModel.tiketPriceAdult
        this.tiketPriceChil=this@OrderModel.tiketPriceChil
        this.jumlahAdult=this@OrderModel.jumlahAdult
        this.jumlahAnak=this@OrderModel.jumlahAnak
        this.totalBayar=this@OrderModel.totalBayar
        this.userId=this@OrderModel.userId
    }

}