package id.gumilombok.data.remote.response

import com.google.gson.annotations.SerializedName

class CekRatingResponse(
    @field:SerializedName("status")
    val status:Boolean?=false,

    @field:SerializedName("pesan")
    val pesan:String?=null,

    @field:SerializedName("rating_value")
    val ratingValue: String?="0",

    @field:SerializedName("rating_coment")
    val ratingComent:String?=""
)