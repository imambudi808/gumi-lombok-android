package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RatingModel (
    @field:SerializedName("rating_id")
    var ratingId:String?="",

    @field:SerializedName("rating_value")
    var ratingValue:String?="",

    @field:SerializedName("rating_coment")
    var ratingComment:String?="",

    @field:SerializedName("destination_id")
    var destinationId:String?="",

    @field:SerializedName("user_id")
    var userId:String?="",

    @field:SerializedName("updated_at")
    var updatedAt:String?="",

    @field:SerializedName("created_at")
    var createdAt:String?="",

    @field:SerializedName("name")
    var name:String?=""

) : Parcelable