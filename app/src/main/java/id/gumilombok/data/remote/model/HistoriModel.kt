package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HistoriModel(
    @SerializedName("order_id")
    var orderId:String?="",

    @SerializedName("order_code")
    var orderCode:String?="",

    @SerializedName("tiket_id")
    var tiketId:String?="",

    @SerializedName("tiket_name")
    var tiketName:String?="",

    @SerializedName("tiket_route")
    var tiketRute:String?="",

    @SerializedName("tiket_agent")
    var tiketAgent:String?="",

    @SerializedName("tiket_price_diskon")
    var tiketPriceDiskon:String?="",

    @SerializedName("tiket_keberangkatan")
    var tiketKeberangkatan:String?="",

    @SerializedName("tiket_tiba")
    var tiketTiba:String?="",

    @SerializedName("tiket_detail")
    var tiketDetail:String?="",

    @SerializedName("durasi_tiket")
    var durasiTiket:String?="",

    @SerializedName("order_tiket_book")
    var orderTiketBook:String?="",

    @SerializedName("order_status")
    var orderStatus:String?="",

    @SerializedName("created_at")
    var createdAt:String?="",

    @SerializedName("updated_at")
    var updatedAt:String?="",

    @SerializedName("trash")
    var trash:String?="",

    @SerializedName("metode_bayar")
    var metodeBayar:String?="",

    @SerializedName("bank_tujuan")
    var bankTujuan:String?="",

    @SerializedName("norek")
    var norekening:String?="",

    @SerializedName("card_holder")
    var cardHolder:String?="",

    @SerializedName("nama_pemilik_rekening")
    var namaPemilikRekening:String?="",

    @SerializedName("tiket_price_adult")
    var tiketPriceAdult:String?="",

    @SerializedName("tiket_price_chil")
    var tiketPriceChil:String?="",

    @SerializedName("jumlah_adult")
    var jumlahAdult:String?="",

    @SerializedName("jumlah_anak")
    var jumlahAnak:String?="",

    @SerializedName("total_bayar")
    var totalBayar:String?="",

    @SerializedName("user_id")
    var userId:String?=""
) : Parcelable