package id.gumilombok.data.remote

import id.gumilombok.data.remote.response.BlogResponse
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface BlogService {

    @POST("show_blog")
    @FormUrlEncoded
    fun loadBlog(@Field("offset") offset:String?,@Field("limit") limit:String?):Flowable<BlogResponse>

    @POST("show_blog")
    fun loadsBlogs(@Body map: Map<String,String?>):Flowable<BlogResponse>

    @POST("show_hot_blog")
    fun loadsHotBlog():Flowable<BlogResponse>

    @POST("show_blog_by_category")
    fun loadBlogByCategory(@Body map: Map<String, String?>):Flowable<BlogResponse>
}