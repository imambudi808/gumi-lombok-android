package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//[
//{
//    "provinceState": null,
//    "countryRegion": "Indonesia",
//    "lastUpdate": 1584843193000,
//    "lat": -0.7893,
//    "long": 113.9213,
//    "confirmed": 450,
//    "recovered": 20,
//    "deaths": 38,
//    "active": 392,
//    "admin2": null,
//    "fips": null,
//    "combinedKey": null,
//    "iso2": "ID",
//    "iso3": "IDN"
//}
//]

@Parcelize
data class CovidModel (

    ///matheo
    @field:SerializedName("provinceState")
    val provinceSatate:String?="",

    @field:SerializedName("countryRegion")
    val countryRegion:String?="",

    @field:SerializedName("lastUpdate")
    val lastUpdate:Long=0,

    @field:SerializedName("lat")
    val lat:String?="",

    @field:SerializedName("long")
    val long:String?="",

    @field:SerializedName("confirmed")
    val totalKasus:Int=0,

    @field:SerializedName("recovered")
    val sembuh:Int=0,

    @field:SerializedName("deaths")
    val kematian:Int=0,

    @field:SerializedName("active")
    val positife:Int=0,

    @field:SerializedName("admin2")
    val admin2:String?="",

    @field:SerializedName("fips")
    val fips:String?="",

    @field:SerializedName("iso2")
    val iso2:String?="",

    //kawal covid ina
    @field:SerializedName("positif")
    val positifId:String?="",

    @field:SerializedName("sembuh")
    val sembuhId:String?="",

    @field:SerializedName("meninggal")
    val meninggalId:String?=""
) : Parcelable