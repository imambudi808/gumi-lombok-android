package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ServiceModel (
    @SerializedName("service_id")
    val serviceId:String="",

    @SerializedName("service_name")
    val serviceName:String="",

    @SerializedName("tiket_id")
    val tiketId:String="",

    @SerializedName("created_at")
    val cretedAt:String=""

) : Parcelable