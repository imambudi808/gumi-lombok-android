package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.gumilombok.presentation.model.Point
import kotlinx.android.parcel.Parcelize


data class PointModel (
    @field:SerializedName("point_id")
    val pointId:String?="",

    @field:SerializedName("point_code")
    val pointCode:String?="",

    @field:SerializedName("point_name")
    val pointName:String?="",

    @field:SerializedName("point_lat")
    val pointLat:String?="",

    @field:SerializedName("point_long")
    val pointlong:String?="",

    @field:SerializedName("point_value")
    val pointValue:String?="",

    @field:SerializedName("destination_id")
    val destinationId:String?="",

    @field:SerializedName("created_at")
    val createdAt:String?=""

){
    fun build() = Point.build {
        this.pointId = this@PointModel.pointId
        this.pointCode = this@PointModel.pointCode
        this.pointName = this@PointModel.pointName
        this.pointLat = this@PointModel.pointLat
        this.pointlong = this@PointModel.pointlong
        this.pointValue = this@PointModel.pointValue
        this.destinationId = this@PointModel.destinationId
        this.createdAt = this@PointModel.createdAt
    }
}