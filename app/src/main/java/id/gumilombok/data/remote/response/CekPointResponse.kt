package id.gumilombok.data.remote.response

import com.google.gson.annotations.SerializedName
import id.gumilombok.data.remote.model.CekLastPoint
import id.gumilombok.data.remote.model.LoginModel

class CekPointResponse (
    @field:SerializedName("status")
    val status:Boolean?=false,

    @field:SerializedName("pesan")
    val pesan:String?=null,

    @field:SerializedName("data")
    val data: String?=null
)
