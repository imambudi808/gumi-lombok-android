package id.gumilombok.data.remote.model

import com.google.gson.annotations.SerializedName

data class CekLastPoint (
    @field:SerializedName("total_point")
    var totalLastPoint:String?=""
)