package id.gumilombok.data.remote


import id.gumilombok.data.remote.response.CovidResponse
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.POST

interface CovidService {
    @GET("showCovidINA")
    fun loadCovidData(): Flowable<CovidResponse>
}