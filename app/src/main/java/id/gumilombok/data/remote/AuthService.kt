package id.gumilombok.data.remote

import id.gumilombok.data.remote.model.CekLastPoint
import id.gumilombok.data.remote.post.AuthPost
import id.gumilombok.data.remote.response.CekPointResponse
import id.gumilombok.data.remote.response.LoginBaseResponse
import id.gumilombok.data.remote.response.OrderBaseResponse
import id.gumilombok.data.remote.response.ServiceBaseResponse
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthService {


//    @FormUrlEncoded
//    @POST("login")
//    fun login(@Field("email") email:String,
//              @Field("password") password:String): Flowable<LoginBaseResponse>

    @POST("login")
    fun login(@Body data:AuthPost): Flowable<LoginBaseResponse>

    @POST("register")
    @FormUrlEncoded
    fun register(@Field("name") name:String,
                 @Field("email") email:String,
                 @Field("password") password: String,
                 @Field("c_password") confirPassword:String):Flowable<LoginBaseResponse>

    @POST("update_profile")
    @FormUrlEncoded
    fun updateProfile(@Field("id") userId:String,
                 @Field("name") name:String,
                 @Field("user_phone") userPhone:String,
                 @Field("email") email:String):Flowable<LoginBaseResponse>

    @POST("update_password")
    @FormUrlEncoded
    fun updatePassword(@Field("id") userId:String,
                      @Field("password_baru") passwordBaru:String):Flowable<LoginBaseResponse>

    @POST("cek_point")
    @FormUrlEncoded
    fun cekSisaPoint(@Field("user_id") userId:String):Flowable<CekPointResponse>


}