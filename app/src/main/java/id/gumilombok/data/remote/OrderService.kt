package id.gumilombok.data.remote


import id.gumilombok.data.remote.response.OrderBaseResponse
import io.reactivex.Flowable
import retrofit2.http.Body

import retrofit2.http.POST

interface OrderService {
    @POST("insert_orders")
    fun loadInsertedOrder(@Body map: Map<String, String?>): Flowable<OrderBaseResponse>

    @POST("update_orders")
    fun updateMetodePembayaran(@Body map: Map<String, String?>): Flowable<OrderBaseResponse>

    @POST("update_konfir")
    fun konfirmasiSudahBayar(@Body map: Map<String, String?>): Flowable<OrderBaseResponse>

    @POST("show_orders")
    fun loadOrderAll(@Body map: Map<String, String?>):Flowable<OrderBaseResponse>


}