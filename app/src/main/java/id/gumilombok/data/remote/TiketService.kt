package id.gumilombok.data.remote


import id.gumilombok.data.remote.response.TiketBaseResponse
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.POST

interface TiketService {

    @POST("tiket_toure")
    fun loadlisttiket(@Body map: Map<String, String?>): Flowable<TiketBaseResponse>


}