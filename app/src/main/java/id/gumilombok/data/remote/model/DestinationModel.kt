package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DestinationModel(

    @field:SerializedName("destination_id")
    var destinationId:String?="",

    @field:SerializedName("destination_name")
    var destinationName:String?="",

    @field:SerializedName("destination_image")
    var destinationImage:String?="",

    @field:SerializedName("destination_content")
    var destinationContent:String?="",

    @field:SerializedName("destination_author")
    var destinationAuthor:String?="",

    @field:SerializedName("created_at")
    var createdAt:String?="",

    @field:SerializedName("destination_category")
    var destinationCategory:String?="",

    @field:SerializedName("destination_lat")
    var destinationLat:String?="",

    @field:SerializedName("destination_long")
    var destinationLong:String?="",

    @field:SerializedName("island_id")
    var islandId:String?="",

    @field:SerializedName("destination_addres")
    var destinationAddress:String?="",

    @field:SerializedName("islands")
    var islands: IslandModel? = null,

    @field:SerializedName("rata_rating")
    var rataRating:String?="0",

    @field:SerializedName("jumlah_response")
    var jumlahResponse:String?="0",

    @field:SerializedName("seo_link")
    var seoLink:String?=""

): Parcelable

