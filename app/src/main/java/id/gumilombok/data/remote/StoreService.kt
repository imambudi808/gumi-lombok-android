package id.gumilombok.data.remote

import id.gumilombok.data.remote.response.BaseResponse
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface StoreService {

    @POST("showAllStore")
    fun loadAllStore(@Body map:Map<String,String?>):Flowable<BaseResponse>

    @POST("showNewStore")
    fun loadNewStore():Flowable<BaseResponse>


}