package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//"produk_id": 1,
//"produk_name": "tembakau lombok",
//"produk_des": "tembakau lombok adalah tembakau yg kerendGHHHH",
//"produk_banner": "https://instagram.fdps5-1.fna.fbcdn.net/v/t51.2885-15/e35/90833121_244496843262444_7832506543743450932_n.jpg?_nc_ht=instagram.fdps5-1.fna.fbcdn.net&_nc_cat=100&_nc_ohc=ldiROoLGVY4AX-Nnq1G&oh=0dd3a5073f1a27dc43777a5df3dbc238&oe=5EA368EA",
//"created_at": "2020-03-21 22:51:52",
//"updated_at": "2020-03-21 22:51:52",
//"store_id": 1,
//"seo_link": "TEMBAKAU-LOMBOK",
//"trash": null

@Parcelize
data class StoreProdukModel (
    @field:SerializedName("produk_id")
    val produkId:String?="",
    @field:SerializedName("produk_name")
    val produkName:String?="",
    @field:SerializedName("produk_des")
    val produkDes:String?="",
    @field:SerializedName("produk_banner")
    val produkBanner:String?="",
    @field:SerializedName("created_at")
    val createdAt:String?="",
    @field:SerializedName("updated_at")
    val updatedAt:String?="",
    @field:SerializedName("store_id")
    val storeId:String?="",
    @field:SerializedName("seo_link")
    val seoLink:String?="",
    @field:SerializedName("harga_awal")
    val hargaAwal:String?="",
    //store
    @field:SerializedName("store_number")
    val storePhoneNumber:String?=""
) : Parcelable