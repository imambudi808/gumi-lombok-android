package id.gumilombok.data.remote

import id.gumilombok.data.remote.response.DestinationBaseResponse
import id.gumilombok.data.remote.response.PointBaseResponse
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface DestinationService {

    @POST("destination")
    fun loadDestinationList(@Body map: Map<String, String?>):Flowable<DestinationBaseResponse>


    @POST("destination_photo")
    @FormUrlEncoded
    fun loadDesPhoto(@Field("destination_id") desId:String?): Flowable<DestinationBaseResponse>

    @POST("newdestination")
    fun newDestination():Flowable<DestinationBaseResponse>

    @POST("update_rating_des")
    @FormUrlEncoded
    fun updateDestinationRate(@Field("destination_id") desId: String?,@Field("rata_rating") rataRating:String?,@Field("jumlah_response") jumlahResponse:String?):Flowable<DestinationBaseResponse>
}