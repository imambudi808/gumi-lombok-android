package id.gumilombok.data.remote.post

import com.google.gson.annotations.SerializedName

data class AuthPost(
    @field:SerializedName("email")
    val email:String,

    @field:SerializedName("password")
    val password:String
)