package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class StoreModel (
    @field:SerializedName("store_id")
    val storeId:Int?=0,

    @field:SerializedName("store_name")
    val storeName:String?="",

    @field:SerializedName("store_image_profile")
    val storeImageProfile:String?="",

    @field:SerializedName("store_image_banner")
    val storeBanner:String?="",

    @field:SerializedName("store_des")
    val storeDes:String?="",

    @field:SerializedName("user_id")
    val userId:String?="",

    @field:SerializedName("created_at")
    val createdAt:String?="",

    @field:SerializedName("updated_at")
    val updatedAt:String?="",

    @field:SerializedName("seo_link")
    val seoLink:String?="",

    @field:SerializedName("store_number")
    val storePhoneNumber:String?=""
) : Parcelable