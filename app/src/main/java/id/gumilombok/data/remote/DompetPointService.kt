package id.gumilombok.data.remote


import io.reactivex.Flowable
import retrofit2.http.Body

import retrofit2.http.POST

interface DompetPointService {

    @POST("insert_point_to_dompet")
    fun savePointToDompet(@Body map: HashMap<String,String?>):Flowable<Map<String,String>>
}