package id.gumilombok.data.remote

import id.gumilombok.data.remote.response.StoreReviewResponse
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.POST

interface StoreReviewService {

    @POST("showReviewByStoreId")
    fun loadStoreReviewByIdStore(@Body map:Map<String,String?>):Flowable<StoreReviewResponse>
}