package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class TiketModel(
    @SerializedName("tiket_id")
    val tiketId:String?="",

    @SerializedName("tiket_name")
    val tiketName:String?="",

    @SerializedName("tiket_route")
    val tiketRoute:String?="",

    @SerializedName("tiket_agent")
    val tiketAgent:String?="",

    @SerializedName("tiket_price")
    val tiketPrice:String?="",

    @SerializedName("tiket_price_diskon")
    val tiketDiskon: String?="",

    @SerializedName("tiket_status")
    val tiketStatus:String?="",

    @SerializedName("tiket_keberangkatan")
    val tiketKeberangkatan:String?="",

    @SerializedName("tiket_tiba")
    val tiketTiba:String?="",

    @SerializedName("tiket_detail")
    val tiketDetail:String?="",

    @SerializedName("tiket_jumlah")
    val tiketJumlah:String?="",

    @SerializedName("created_at")
    val createdAt:String?="",

    @SerializedName("durasi")
    val tiketDurasi:String?="",

    @SerializedName("available_book")
    val tiketAvailableBook:String?="",

    @SerializedName("end_book")
    val tiketEndBook:String?="",

    @SerializedName("tiket_price_chil")
    val tiketPriceChil:String?=""


) : Parcelable