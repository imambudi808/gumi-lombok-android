package id.gumilombok.data.remote.response

import com.google.gson.annotations.SerializedName

data class OrderBaseResponse(
    @field:SerializedName("status")
    val status: Boolean = false,

    @field:SerializedName("pesan")
    val pesan: String? = "",

    @field:SerializedName("data")
    val data: List<Any>? = ArrayList(),

    @field:SerializedName("order_id")
    val orderIdBase: String? = ""
    )
