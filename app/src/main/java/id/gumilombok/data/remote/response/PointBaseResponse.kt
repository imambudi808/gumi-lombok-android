package id.gumilombok.data.remote.response

import com.google.gson.annotations.SerializedName
import id.gumilombok.data.remote.model.PointModel

data class PointBaseResponse (
    @field:SerializedName("status")
    val status: Boolean = false,

    @field:SerializedName("pesan")
    val pesan: String? = "",

    @field:SerializedName("data")
    val data: List<PointModel>? = null)