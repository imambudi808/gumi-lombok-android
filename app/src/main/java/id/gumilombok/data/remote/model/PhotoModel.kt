package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotoModel(

    @field:SerializedName("id")
    var photoId:String?="",

    @field:SerializedName("filename")
    var photoFile:String?="",

    @field:SerializedName("description")
    var photoDesc:String?="",

    @field:SerializedName("created_at")
    var createdAt :String?="",

    @field:SerializedName("destination_id")
    var destinationId:String?=""



): Parcelable
