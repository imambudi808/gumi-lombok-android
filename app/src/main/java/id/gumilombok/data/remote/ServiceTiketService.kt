package id.gumilombok.data.remote

import id.gumilombok.data.remote.response.ServiceBaseResponse
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.POST

interface ServiceTiketService {

    @POST("services")
    fun loadservicetiketbyidtiket(@Body map: Map<String, String?>): Flowable<ServiceBaseResponse>
}