package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//"review_id": 2,
//"review_rating": 5,
//"review_comment": "Barang Yang diJual sesuai deskripsi",
//"store_id": 1,
//"user_id": 93,
//"created_at": null,
//"updated_at": null,
//"name": "Imam Budi"

@Parcelize
data class StoreReviewModel(
    @field:SerializedName("review_id")
    val reviewId:String?="",

    @field:SerializedName("review_rating")
    val reviewRating:String?="",

    @field:SerializedName("review_comment")
    val reviewComment:String?="",

    @field:SerializedName("store_id")
    val storeId:String?="",

    @field:SerializedName("user_id")
    val userId:String?="",

    @field:SerializedName("created_at")
    val createdAt:String?="",

    @field:SerializedName("updated_at")
    val updatedAt:String?="",

    @field:SerializedName("name")
    val storeReviewerName:String?=""
) : Parcelable