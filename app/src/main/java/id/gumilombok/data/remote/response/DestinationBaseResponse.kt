package id.gumilombok.data.remote.response

import com.google.gson.annotations.SerializedName

data class DestinationBaseResponse(
    @field:SerializedName("status")
    val status: Boolean = false,

    @field:SerializedName("pesan")
    val pesan: String? = "",

    @field:SerializedName("data")
    val data: ArrayList<Any>? = ArrayList())