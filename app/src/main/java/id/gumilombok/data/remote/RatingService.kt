package id.gumilombok.data.remote

import id.gumilombok.data.remote.response.CekRatingResponse
import id.gumilombok.data.remote.response.RatingBaseResponse
import io.reactivex.Flowable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface RatingService {
    @POST("show_rating_by_des")
    @FormUrlEncoded
    fun loadrating(@Field("destination_id") desId:String?): Flowable<RatingBaseResponse>

    @POST("insert_rating")
    @FormUrlEncoded
    fun insertRating(@Field("rating_value") ratingValue:String?,@Field("rating_coment") ratingComent:String?,@Field("destination_id") desId: String?,@Field("user_id") userId:String?): Flowable<RatingBaseResponse>

    @POST("show_total_rate")
    @FormUrlEncoded
    fun update_rating(@Field("destination_id") desId:String?): Flowable<RatingBaseResponse>

    @POST("cek_rating")
    @FormUrlEncoded
    fun cek_rating(@Field("user_id") userId: String?,@Field("destination_id") desId:String?): Flowable<CekRatingResponse>

    @POST("update_rating_user")
    @FormUrlEncoded
    fun updateRatingUser(@Field("user_id") userId: String?,@Field("destination_id") desId:String?,@Field("rating_coment") ratingComment:String?): Flowable<RatingBaseResponse>



}