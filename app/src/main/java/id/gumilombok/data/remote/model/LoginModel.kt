package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginModel (
    @field:SerializedName("id")
    val id:String?=null,

    @field:SerializedName("name")
    val name:String?=null,

    @field:SerializedName("email")
    val email:String?=null,

    @field:SerializedName("email_verified_at")
    val email_verified:String?=null,

    @field:SerializedName("password")
    val password:String?=null,

    @field:SerializedName("user_role")
    val user_role:String?=null,

    @field:SerializedName("user_phone")
    val userPhone:String?=null,

    @field:SerializedName("user_address")
    val user_address:String?=null,

    @field:SerializedName("user_photo")
    val user_photo:String?=null,

    @field:SerializedName("total_point")
    val total_point:String?=null,

    @field:SerializedName("passwordnya")
    val passwordnya:String?=null,

    @field:SerializedName("token")
    val token:String?=null
) : Parcelable