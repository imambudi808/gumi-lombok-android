package id.gumilombok.data.remote.response

import com.google.gson.annotations.SerializedName

data class StoreReviewResponse (
    @field:SerializedName("status")
    val status:Boolean=false,

    @field:SerializedName("pesan")
    val pesan:String?="",

    @field:SerializedName("is_comment")
    val is_comment:String?="",

    @field:SerializedName("data")
    val data:List<Any>? = ArrayList()
)