package id.gumilombok.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//"event_id": 2,
////"event_name": "festival presean",
////"event_image": "https://instagram.fjog3-1.fna.fbcdn.net/v/t51.2885-15/e35/83241188_836975976817023_5973588088112662784_n.jpg?_nc_ht=instagram.fjog3-1.fna.fbcdn.net&_nc_cat=103&_nc_ohc=Z8JnZLkqLbIAX-TmC05&oh=4de1041cf4aab16e1edec047c3f3cc2c&oe=5ECB837D",
////"event_cost": "free entry",
////"event_location": "lombok utara",
////"event_date": "14 Januari 2020-16 Februari 2020",
////"created_at": "2020-01-29 19:20:40",
////"updated_at": "2020-01-29 19:20:40",
////"trash": null,
////"seo_link": "bau"
@Parcelize
data class EventModel(
    @field:SerializedName("event_id")
    val eventId:String?="",

    @field:SerializedName("event_name")
    val eventName:String?="",

    @field:SerializedName("event_image")
    val eventImage:String?="",

    @field:SerializedName("event_cost")
    val eventCost:String?="",

    @field:SerializedName("event_location")
    val eventLocation:String?="",

    @field:SerializedName("event_date")
    val eventDate:String?="",

    @field:SerializedName("created_at")
    val createdAt:String?="",

    @field:SerializedName("updated_at")
    val updatedAt:String?="",

    @field:SerializedName("seo_link")
    val seoLink:String?=""
) : Parcelable