package id.gumilombok.data.remote

import id.gumilombok.data.remote.response.DestinationBaseResponse
import id.gumilombok.data.remote.response.PointBaseResponse
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface PointService {

    @POST("show_all_point")
    fun loadAllPoint(@Body map: Map<String, String?>): Flowable<DestinationBaseResponse>

    @POST("show_point_by_des_id")
    @FormUrlEncoded
    fun loadPointByIdDes(@Field("destination_id") desId:String?): Flowable<PointBaseResponse>

}