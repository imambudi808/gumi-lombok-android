package id.gumilombok.data.remote.response

import com.google.gson.annotations.SerializedName
import id.gumilombok.data.remote.model.LoginModel

data class LoginBaseResponse (
    @field:SerializedName("status")
    val status:Boolean?=false,

    @field:SerializedName("pesan")
    val pesan:String?=null,

    @field:SerializedName("data")
    val data: LoginModel?=null
)