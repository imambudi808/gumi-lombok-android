package id.gumilombok.data.local

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.gumilombok.data.remote.model.CekLastPoint
import id.gumilombok.data.remote.model.LoginModel
import id.gumilombok.di.ApplicationContext
import java.util.HashMap
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class PreferencesManager @Inject constructor(@ApplicationContext context: Context){
    private val tag=this::class.java.simpleName

    private val sp: SharedPreferences = context.getSharedPreferences("gumilombok", Context.MODE_PRIVATE)
    private val spe: SharedPreferences.Editor=sp.edit()

    var token: String?
        get() = sp.getString("token", "")
        set(value) {
            spe.putString("token", value)
            spe.apply()
        }
    var isLoggedIn: Boolean
        get() = sp.getBoolean("is_logged_id", false)
        set(value) {
            spe.putBoolean("is_logged_id", value)
            spe.apply()
        }
    //google login tampung sementara jika tidak ada akun maka otomatis registrasi
    var emailTampung: String?
        get() = sp.getString("emailTampung", "")
        set(value) {
            spe.putString("emailTampung", value)
            spe.apply()
        }
    var idTampung: String?
        get() = sp.getString("idTampung", "")
        set(value) {
            spe.putString("idTampung", value)
            spe.apply()
        }
    var gmailNameTamp: String?
        get() = sp.getString("gmailNameTamp", "")
        set(value) {
            spe.putString("gmailNameTamp", value)
            spe.apply()
        }
    var gmailProfilImageTamp: String?
        get() = sp.getString("gmailProfilImageTamp", "")
        set(value) {
            spe.putString("gmailProfilImageTamp", value)
            spe.apply()
        }
    ////////////////////////////////

    val userSession: LoginModel
        get() {
            val keys = arrayOf(
                "id", "name", "email", "email_verified", "password", "user_role", "user_phone",
                "user_address", "user_photo", "total_point","passwordnya", "token"
            )

            val mapSession = HashMap<String, String?>()
            keys.forEach {
                mapSession[it] = sp.getString(it, "")
            }
            return Gson().fromJson(Gson().toJsonTree(mapSession), LoginModel::class.java)
        }

    fun setUserSession(loginData:LoginModel?){
        loginData?.token?.let { token=it }
        isLoggedIn=true

        val userSession: Map<String, Any> = Gson().fromJson(Gson().toJsonTree(loginData), object: TypeToken<Map<String, Any>>(){}.type)
        for ((key, value) in userSession) {
            if (value is String) {
                spe.putString(key as String?, value)
            }
        }
        spe.apply()
    }
    var cekLastPoint: String?
        get() = sp.getString("total_point", "")
        set(value) {
            spe.putString("total_point", value)
            spe.apply()
        }


//    var cekLastPoint:CekLastPoint?
//    get(){
//        val keys = arrayOf("total_point")
//        val cekPointMap = mutableMapOf<String,String?>()
//        (0.until(keys.size)).forEachIndexed { i, _ ->
//            cekPointMap[keys[i]] = sp.getString(keys[i],"")
//        }
//        val cekLastP:CekLastPoint=Gson().fromJson(Gson().toJsonTree(cekPointMap),CekLastPoint::class.java)
//        return cekLastP
//    }
//    set(value){
//        value.also {
//            val cekPointMap:Map<String,String> = Gson().fromJson(Gson().toJsonTree(it),object :TypeToken<Map<String,String>>(){}.type)
//            for ((k,v) in cekPointMap){
//                spe.putString(k,v)
//            }
//        }
//    }
//    var userAs: String?
//        get() = sp.getString("userAs", UserAs.Pengguna.name)
//        set(value) {
//            spe.putString("userAs", value)
//            spe.apply()
//        }

    var isFirstOpen: Boolean
        get() = sp.getBoolean("isFirstOpen", true)
        set(value) {
            spe.putBoolean("isFirstOpen", value)
            spe.apply()
        }

    fun setupFirstOpen() {
        spe.putBoolean("isFirstOpen", false)
        spe.apply()
    }

    fun logOut() {

        spe.clear().commit()
        isFirstOpen = false
        isLoggedIn = false
        spe.apply()
    }


}