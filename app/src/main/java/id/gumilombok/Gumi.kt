package id.gumilombok

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
//import com.ale.rainbowsdk.RainbowSdk
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import id.gumilombok.di.component.AppComponent
import id.gumilombok.di.component.DaggerAppComponent
import id.gumilombok.di.module.AppModule
import id.gumilombok.di.module.DataModule
import id.gumilombok.di.module.NetworkModule
//import id.gumilombok.di.module.SyncModule
import javax.inject.Inject

class Gumi : Application(), HasAndroidInjector {


    //    override fun serviceInjector(): AndroidInjector<Service> = serviceDispatchingInjector

    override fun androidInjector(): AndroidInjector<Any> = serviceDispatchingInjector

    @Inject
    lateinit var serviceDispatchingInjector: DispatchingAndroidInjector<Any>

    //TODO : Manage api key : https://console.developers.google.com/
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .dataModule(DataModule())
//            .syncModule(SyncModule())
            .networkModule(NetworkModule())
            .build()
    }


    companion object {
        fun get(context: Context): Gumi = context.applicationContext as Gumi
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)

//        RainbowSdk
//            .instance()
//            .initialize(
//                this,
//                "f6ca8730571111eab014b5b6600a3114",
//                "YdWDjqEkvHKJUwBG1EkuSFiMBleGcvp3U0OYaNZvE8GHM0EIBKlfAjiXHswREwFU"
//            )

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}