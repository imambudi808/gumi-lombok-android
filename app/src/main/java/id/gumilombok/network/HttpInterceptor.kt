package id.gumilombok.network

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import id.gumilombok.BuildConfig
import id.gumilombok.data.local.PreferencesManager
import okhttp3.*
import java.io.IOException

class HttpInterceptor(private val context: Context) : Interceptor {

    lateinit var sp: PreferencesManager

    companion object {
        const val TAG = "HttpInterceptor"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        sp = PreferencesManager(context)
        Log.i(TAG, "Authorization : ${sp.token}")

        val token = sp.token
        val request = chain?.request()!!

        //include token header to every request
        val modifyRequest: Request? = token?.let {
            request.newBuilder()
                ?.addHeader("Authorization","Bearer ${it}")
                ?.build()
        }

        val response: Response = chain.proceed(modifyRequest!!)

        return response
    }

}