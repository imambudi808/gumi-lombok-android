package id.gumilombok.di.module

import dagger.Module
import dagger.Provides
import id.gumilombok.data.remote.*
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideAuthApiService(retrofit: Retrofit): AuthService = retrofit.create(AuthService::class.java)

    @Provides
    @Singleton
    fun provideDestinationService(retrofit: Retrofit): DestinationService = retrofit.create(DestinationService::class.java)

    @Provides
    @Singleton
    fun providePointService(retrofit: Retrofit):PointService=retrofit.create(PointService::class.java)

    @Provides
    @Singleton
    fun provideDompetPointService(retrofit: Retrofit):DompetPointService = retrofit.create(DompetPointService::class.java)

    @Provides
    @Singleton
    fun provideTiketService(retrofit: Retrofit):TiketService = retrofit.create(TiketService::class.java)

    @Provides
    @Singleton
    fun provideServiceTiketService(retrofit: Retrofit):ServiceTiketService = retrofit.create(ServiceTiketService::class.java)

    @Provides
    @Singleton
    fun provideOrderService(retrofit: Retrofit):OrderService = retrofit.create(OrderService::class.java)

    @Provides
    @Singleton
    fun provideRatingService(retrofit: Retrofit):RatingService = retrofit.create(RatingService::class.java)

    @Provides
    @Singleton
    fun provideBlogService(retrofit: Retrofit):BlogService=retrofit.create(BlogService::class.java)

    @Provides
    @Singleton
    fun provideEventService(retrofit: Retrofit):EventService=retrofit.create(EventService::class.java)

    @Provides
    @Singleton
    fun provideCovidService(retrofit: Retrofit):CovidService=retrofit.create(CovidService::class.java)

    @Provides
    @Singleton
    fun provideStoreService(retrofit: Retrofit):StoreService=retrofit.create(StoreService::class.java)

    @Provides
    @Singleton
    fun provideProdukService(retrofit: Retrofit):StoreProdukService=retrofit.create(StoreProdukService::class.java)

    @Provides
    @Singleton
    fun provideStoreReviewService(retrofit: Retrofit):StoreReviewService=retrofit.create(StoreReviewService::class.java)

}
