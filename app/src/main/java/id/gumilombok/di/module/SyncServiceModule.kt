package id.gumilombok.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import id.gumilombok.di.FragmentContext
//import id.gumilombok.sync.GumiSyncAdapterService
import javax.inject.Singleton

@Module
class SyncServiceModule(val context: Context) {
    @Provides
    @FragmentContext
    fun provideContext(): Context = context
//    @Provides
//    @Singleton
//    fun syncAdapterService() : GumiSyncAdapterService = GumiSyncAdapterService()
}