package id.gumilombok.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.ApplicationContext
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideSharedPref(@ApplicationContext context: Context): PreferencesManager = PreferencesManager(context)


}