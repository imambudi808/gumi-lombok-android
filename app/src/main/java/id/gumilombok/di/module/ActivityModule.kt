package id.gumilombok.di.module

import android.app.Activity
import android.content.Context
import dagger.Module
import dagger.Provides
import id.gumilombok.di.ActivityContext
import id.gumilombok.di.PerActivity

@Module
class ActivityModule(val activity: Activity) {

    @Provides
    @PerActivity
    fun provideActivity(): Activity = activity

    @Provides
    @ActivityContext
    fun provideContext(): Context = activity
}