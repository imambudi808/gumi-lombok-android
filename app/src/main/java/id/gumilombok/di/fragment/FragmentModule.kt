package id.gumilombok.di.fragment


import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides

import dagger.android.ContributesAndroidInjector
import id.gumilombok.di.ActivityContext
import id.gumilombok.di.FragmentContext
import id.gumilombok.di.PerActivity

import id.gumilombok.presentation.ui.home.HomeFragment

@Module
class FragmentModule(val fragment: Fragment) {

    @Provides
    @PerActivity
    fun provideActivity(): Fragment = fragment

//    @Provides
//    @FragmentContext
//    fun provideContext(): Context = fragment
}