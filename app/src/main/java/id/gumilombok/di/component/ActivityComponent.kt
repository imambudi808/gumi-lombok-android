package id.gumilombok.di.component

import dagger.Subcomponent
import id.gumilombok.di.PerActivity
import id.gumilombok.di.module.ActivityModule
import id.gumilombok.presentation.ui.blog.BlogActivity
import id.gumilombok.presentation.ui.covid.CovidActivity
import id.gumilombok.presentation.ui.destination.DestinationActivity
import id.gumilombok.presentation.ui.destination.DetailDestinationActivity
import id.gumilombok.presentation.ui.destination.FormKementarActivity
import id.gumilombok.presentation.ui.event.EventActivity
import id.gumilombok.presentation.ui.histori_transaksi.CetakTiketActivity
import id.gumilombok.presentation.ui.histori_transaksi.DetailHistoriTransaksi
import id.gumilombok.presentation.ui.histori_transaksi.HistoriActivity
import id.gumilombok.presentation.ui.home.*
import id.gumilombok.presentation.ui.profile.EditPasswordActivity
import id.gumilombok.presentation.ui.login.LoginActivity
import id.gumilombok.presentation.ui.login.RegistrasiActivity
import id.gumilombok.presentation.ui.maps.AllMaps
import id.gumilombok.presentation.ui.maps.MapsView
import id.gumilombok.presentation.ui.point.DetailPointActivity
import id.gumilombok.presentation.ui.profile.EditProfileActivity
import id.gumilombok.presentation.ui.splash.SplashScreenActivity
import id.gumilombok.presentation.ui.splash.WelcomeActivity
import id.gumilombok.presentation.ui.store.StoreActivity
import id.gumilombok.presentation.ui.store.StoreDetailActivity
import id.gumilombok.presentation.ui.ticket.*


@PerActivity
@Subcomponent(modules = [(ActivityModule::class)])
interface ActivityComponent {

    @Subcomponent.Builder
    interface Builder{
        fun activityModule(activityModule: ActivityModule): Builder
        fun build(): ActivityComponent
    }
    fun inject(loginActivity: LoginActivity)
    fun inject(mainActivity: MainActivity)
    fun inject(destinationActivity: DestinationActivity)
    fun inject(splashScreenActivity: SplashScreenActivity)
    fun inject(welcomeActivity: WelcomeActivity)
    fun inject(detailDestinationActivity: DetailDestinationActivity)
    fun inject(detailPointActivity: DetailPointActivity)
    fun inject(TicketActivity: TicketActivity)
    fun inject(detailTiketActivity: DetailTiketActivity)
    fun inject(konfirmasiTransaksi: KonfirmasiTransaksi)
    fun inject(mapsView: MapsView)
    fun inject(metodePembayaran: MetodePembayaran)
    fun inject(konfirmasiBankTransfer: KonfirmasiBankTransfer)
    fun inject(historiActivity: HistoriActivity)
    fun inject(detailHistoriTransaksi: DetailHistoriTransaksi)
    fun inject(cetakTiketActivity: CetakTiketActivity)
    fun inject(registrasiActivity: RegistrasiActivity)
    fun inject(homeFragment: HomeFragment)
    fun inject(profileFragemnt: ProfileFragemnt)
    fun inject (historiFragment: HistoriFragment)
    fun inject (destinationSearchFragment: DestinationSearchFragment)
    fun inject(notifFragment: NotifFragment)
    fun inject(allMaps: AllMaps)
    fun inject (formKementarActivity: FormKementarActivity)
    fun inject(editProfileActivity: EditProfileActivity)
    fun inject(editPasswordActivity: EditPasswordActivity)
    fun inject(blogActivity: BlogActivity)
    fun inject(eventActivity: EventActivity)
    fun inject(storeActivity: StoreActivity)
    fun inject(covidActivity: CovidActivity)
    fun inject(storeDetailActivity: StoreDetailActivity)
}