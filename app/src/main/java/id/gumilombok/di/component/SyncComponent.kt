package id.gumilombok.di.component

import dagger.Subcomponent
import id.gumilombok.di.SyncScope
import id.gumilombok.di.module.SyncServiceModule
import id.gumilombok.presentation.ui.blog.BlogAllCategory
import id.gumilombok.presentation.ui.blog.BlogByCategoryFragment
import id.gumilombok.presentation.ui.blog.BlogHomeFragment
import id.gumilombok.presentation.ui.home.*
import id.gumilombok.presentation.ui.store.ProdukFragment
import id.gumilombok.presentation.ui.store.StoreDetailFragment
import id.gumilombok.presentation.ui.store.StoreReviewFragment
import id.gumilombok.services.FirebaseInstanceIDService

@SyncScope
@Subcomponent(modules = [(SyncServiceModule::class)])
interface SyncComponent {

    @Subcomponent.Builder
    interface Builder {
        fun syncModule(syncServiceModule: SyncServiceModule): Builder
        fun build(): SyncComponent
    }


    fun inject(firebaseInstanceIDService: FirebaseInstanceIDService)
    fun inject(homeFragment: HomeFragment)
    fun inject(fragment: HistoriFragment)
    fun inject(profileFragemnt: ProfileFragemnt)
    fun inject(destinationSearchFragment: DestinationSearchFragment)
    fun inject(notifFragment: NotifFragment)
    fun inject(produkFragment: ProdukFragment)
    fun inject(reviewFragment: StoreReviewFragment)
    fun inject(blogHomeFragment: BlogHomeFragment)
    fun inject(blogByCategoryFragment: BlogByCategoryFragment)
    fun inject(storeDetailFragment: StoreDetailFragment)
    fun inject(blogAllCategory: BlogAllCategory)

}