package id.gumilombok.di.component

import android.app.Application
import dagger.Component
import dagger.android.AndroidInjectionModule
import id.gumilombok.di.fragment.FragmentModule
import id.gumilombok.di.module.*
//import id.gumilombok.di.module.SyncModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        (AppModule::class),
        (DataModule::class),
        (NetworkModule::class),
        (SyncModule::class),
        (AndroidInjectionModule::class)]
)

interface AppComponent {
    fun activityComponent(): ActivityComponent.Builder
    fun syncComponent(): SyncComponent.Builder
    fun inject(app: Application)
}