package id.gumilombok.di

import javax.inject.Qualifier

@Qualifier
@Retention annotation class FragmentContext