package id.gumilombok.presentation.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.gumilombok.Gumi
import id.gumilombok.common.isNetworkAvailable
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.di.module.ActivityModule

abstract class BaseActivity: AppCompatActivity() {

    var isOnline: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isOnline = isNetworkAvailable(this)

        val activityComponent = Gumi.get(this)
            .appComponent
            .activityComponent()
            .activityModule(ActivityModule(this))
            .build()

        injectModule(activityComponent)
    }

    abstract fun injectModule(activityComponent: ActivityComponent)
}