package id.gumilombok.presentation.base.view

interface BaseView {
    fun showMessage(message: String?)
    fun showProgress(show: Boolean)
}