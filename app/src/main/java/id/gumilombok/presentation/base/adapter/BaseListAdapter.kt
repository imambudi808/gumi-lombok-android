package id.gumilombok.presentation.base.adapter

import android.content.Context
import android.os.Parcelable
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseListAdapter<T: Parcelable> : RecyclerView.Adapter<BaseListAdapter<T>.ViewHolder>(){

    var items = ArrayList<T>()

    abstract fun getItemView(context: Context): BaseViewHolder<T>

    fun addItem(item: T?){
        item?.let {
            items.add(it)
            notifyItemInserted(items.size)
        }
    }

    fun editItem(item: T?, position: Int?){
        item?.let {
            position?.let { p ->
                items[p] = it
                notifyItemChanged(p)
            }
        }
    }

    fun removeItemAt(position: Int?){
        position?.let {
            items.removeAt(it)
            notifyItemRemoved(it)
        }
    }

    fun addItems(itemsToAdd: List<T>?){
        itemsToAdd?.let {
            items.addAll(it)
            notifyItemRangeChanged(itemCount, items.size)
        }
    }

    fun clearItems(){
        items.clear()
        notifyItemRangeRemoved(0, items.size)
    }

    fun isEmpty(): Boolean = items.isEmpty()

    override fun onCreateViewHolder(group: ViewGroup, type: Int): ViewHolder
            = ViewHolder(getItemView(group.context!!))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.bind(items[position], position)
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val view: BaseViewHolder<T>) : RecyclerView.ViewHolder(view)
}