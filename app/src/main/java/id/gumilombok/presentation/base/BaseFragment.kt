package id.gumilombok.presentation.base


import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment

import androidx.appcompat.app.AppCompatActivity
import id.gumilombok.Gumi
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.presentation.base.view.BaseView
import id.gumilombok.di.module.ActivityModule
import id.gumilombok.di.module.SyncServiceModule


abstract class BaseFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val serviceInjector = Gumi.get(context!!.applicationContext)
            .appComponent
            .syncComponent()
            .syncModule(SyncServiceModule(context!!.applicationContext))
            .build()
        injectModule(serviceInjector)
    }
    abstract fun injectModule(activityComponent: SyncComponent)

}