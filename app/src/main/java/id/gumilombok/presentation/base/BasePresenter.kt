package id.gumilombok.presentation.base

import android.content.Context
import id.gumilombok.di.FragmentContext
import id.gumilombok.presentation.base.view.BaseView
import io.reactivex.disposables.CompositeDisposable

open class BasePresenter<T: BaseView> {

    protected val disposables = CompositeDisposable()
    protected var view: T? = null
    protected var isOnline = false

    fun bind(activityView: T, isOnline: Boolean? = false) {
        this.view = activityView
        isOnline?.let {
            this.isOnline = it
        }
    }



    private fun unbind() {
        this.view = null
    }

    fun clear() {
        disposables.clear()
        unbind()
    }

    fun dispose() {
        if (!disposables.isDisposed) disposables.clear()
        unbind()
    }
}