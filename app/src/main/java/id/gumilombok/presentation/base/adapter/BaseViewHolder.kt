package id.gumilombok.presentation.base.adapter

import android.content.Context
import android.os.Parcelable
import android.view.View
import android.widget.RelativeLayout

abstract class BaseViewHolder<in T: Parcelable>(context: Context?) : RelativeLayout(context) {

    init {
        init()
    }

    private fun init() {
        View.inflate(context, layoutResId(), this)
    }

    abstract fun layoutResId(): Int

    abstract fun bind(item: T, position: Int)
}