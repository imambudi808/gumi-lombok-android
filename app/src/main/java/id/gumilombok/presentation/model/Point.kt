package id.gumilombok.presentation.model

import android.annotation.SuppressLint
import android.os.Parcelable


import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Point (
    val pointId:String?="",
    val pointCode:String?="",
    val pointName:String?="",
    val pointLat:String?="",
    val pointlong:String?="",
    val pointValue:String?="",
    val destinationId:String?="",
    val createdAt:String?=""

): Parcelable{
    class Builder{
        var pointId:String?=""
        var pointCode:String?=""
        var pointName:String?=""
        var pointLat:String?=""
        var pointlong:String?=""
        var pointValue:String?=""
        var destinationId:String?=""
        var createdAt:String?=""

        fun build()=Point(this)
    }

    constructor(builder:Builder) : this(
        pointId = builder.pointId,
        pointCode = builder.pointCode,
        pointName = builder.pointName,
        pointLat = builder.pointLat,
        pointlong = builder.pointlong,
        pointValue = builder.pointValue,
        destinationId = builder.destinationId,
        createdAt = builder.createdAt
    )

    companion object{
        fun build(block:Builder. () -> Unit)=Builder().apply(block).build()
    }
}