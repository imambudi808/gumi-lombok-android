package id.gumilombok.presentation.model

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Order (
    var orderId:String?="",
    var orderCode:String?="",
    var tiketId:String?="",
    var tiketName:String?="",
    var tiketRute:String?="",
    var tiketAgent:String?="",
    var tiketPriceDiskon:String?="",
    var tiketKeberangkatan:String?="",
    var tiketTiba:String?="",
    var tiketDetail:String?="",
    var durasiTiket:String?="",
    var orderTiketBook:String?="",
    var orderStatus:String?="",
    var createdAt:String?="",
    var updatedAt:String?="",
    var trash:String?="",
    var metodeBayar:String?="",
    var bankTujuan:String?="",
    var norekening:String?="",
    var cardHolder:String?="",
    var namaPemilikRekening:String?="",
    var tiketPriceAdult:String?="",
    var tiketPriceChil:String?="",
    var jumlahAdult:String?="",
    var jumlahAnak:String?="",
    var totalBayar:String?="",
    var userId:String?=""
): Parcelable {
    class Builder{
        var orderId:String?=""
        var orderCode:String?=""
        var tiketId:String?=""
        var tiketName:String?=""
        var tiketRute:String?=""
        var tiketAgent:String?=""
        var tiketPriceDiskon:String?=""
        var tiketKeberangkatan:String?=""
        var tiketTiba:String?=""
        var tiketDetail:String?=""
        var durasiTiket:String?=""
        var orderTiketBook:String?=""
        var orderStatus:String?=""
        var createdAt:String?=""
        var updatedAt:String?=""
        var trash:String?=""
        var metodeBayar:String?=""
        var bankTujuan:String?=""
        var norekening:String?=""
        var cardHolder:String?=""
        var namaPemilikRekening:String?=""
        var tiketPriceAdult:String?=""
        var tiketPriceChil:String?=""
        var jumlahAdult:String?=""
        var jumlahAnak:String?=""
        var totalBayar:String?=""
        var userId:String?=""

        fun build() = Order(this)
    }
    constructor(builder:Builder):this(
        orderId = builder.orderId,
        orderCode = builder.orderCode,
        tiketId = builder.tiketId,
        tiketName = builder.tiketName,
        tiketRute = builder.tiketRute,
        tiketAgent = builder.tiketAgent,
        tiketPriceDiskon = builder.tiketPriceDiskon,
        tiketKeberangkatan = builder.tiketKeberangkatan,
        tiketTiba = builder.tiketTiba,
        tiketDetail = builder.tiketDetail,
        durasiTiket = builder.durasiTiket,
        orderTiketBook = builder.orderTiketBook,
        orderStatus = builder.orderStatus,
        createdAt = builder.createdAt,
        updatedAt = builder.updatedAt,
        trash = builder.trash,
        metodeBayar = builder.metodeBayar,
        bankTujuan = builder.bankTujuan,
        norekening=builder.norekening,
        cardHolder=builder.cardHolder,
        namaPemilikRekening = builder.namaPemilikRekening,
        tiketPriceAdult = builder.tiketPriceAdult,
        tiketPriceChil = builder.tiketPriceChil,
        jumlahAdult = builder.jumlahAdult,
        jumlahAnak = builder.jumlahAnak,
        totalBayar = builder.totalBayar,
        userId = builder.userId

    )
    companion object{
        fun build(block:Builder. () -> Unit)=Builder().apply(block).build()
    }
}