package id.gumilombok.presentation.ui.blog.view

import id.gumilombok.data.remote.model.BlogModel
import id.gumilombok.presentation.base.view.BaseView


interface BlogView:BaseView {
    fun showLoading(showLoading:Boolean){}
    fun onBlogLoad(blogs:List<BlogModel>?){}
    fun onBlogLoads(blogs: List<BlogModel>?,clearItem:Boolean)
    fun onBlogByCategoryload(blogs: List<BlogModel>?,clearItem:Boolean)

}