package id.gumilombok.presentation.ui.profile


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher

import android.util.Log.d

import android.view.View

import androidx.appcompat.app.AlertDialog
import com.google.gson.Gson
import id.gumilombok.R
import id.gumilombok.common.intentFor
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.CustomAlert
import id.gumilombok.presentation.base.BaseActivity

import id.gumilombok.presentation.ui.login.presenter.LoginPresenter
import id.gumilombok.presentation.ui.login.view.LoginView
import id.gumilombok.presentation.ui.splash.WelcomeActivity
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.edtEmail
import kotlinx.android.synthetic.main.activity_edit_profile.edtPhone

import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class EditProfileActivity : BaseActivity(),LoginView {


    val TAG = this::class.java.simpleName
    private lateinit var alert: CustomAlert
    private var edtNamaBaru:String=""
    private var edtEmailBaru:String=""
    private var edtPhoneBaru:String=""
    override fun onLoginSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRegistrasionSuccses() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onUpdateProfileSuccses() {
        this.toast("Profile berhasil diperbarui")
        finish()
    }

    override fun onUserNotRegistred() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showMessage(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showProgress(show: Boolean) {
        progress?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    @Inject lateinit var sp:PreferencesManager
    @Inject lateinit var presenter:LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.bind(this, isOnline)
        title = "Edit Profile"

        onSetupProfile()
    }
    private fun onSetupProfile(){
        edtEmail.setText(sp.userSession.email)
        edtPhone.setText(sp.userSession.userPhone)
        edtNama.setText(sp.userSession.name)


        edtNamaBaru=edtEmailBaru
        edtEmailBaru=edtEmail.text.toString()
        edtPhoneBaru=edtPhone.text.toString()
        edtNama.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                edtNamaBaru=s.toString()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                edtNamaBaru=s.toString()
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                edtNamaBaru=s.toString()
            }
        })
//        email
        edtEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                edtEmailBaru=s.toString()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                edtEmailBaru=s.toString()
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                edtEmailBaru=s.toString()
            }
        })
        //phone
        edtPhone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                edtPhoneBaru=s.toString()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                edtPhoneBaru=s.toString()
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                edtPhoneBaru=s.toString()
            }
        })




        d(TAG,"ini email :${Gson().toJsonTree(edtEmail.toString())}")
        d(TAG,"ini email baru :${Gson().toJsonTree(edtEmailBaru)}")
        btnSimpanProfile.setOnClickListener {
            AlertDialog.Builder(this)
                .setMessage("Simpan Perubahan")
                .setPositiveButton("Simpan") { dialogInterface, _ ->
                    presenter.updateProfile(sp.userSession.id.toString(),edtNamaBaru,edtPhoneBaru,edtEmailBaru)
                    sp.logOut()
                    startActivity(intentFor<WelcomeActivity>())
                    this.toast("Perubahan disimpan")
                    dialogInterface.dismiss()
                }
                .setNegativeButton("Discard") { dialogInterface, _ ->
                    this.toast("perubahan dibatalkan")
                    dialogInterface.dismiss()
                }
                .show()


        }
    }
}
