package id.gumilombok.presentation.ui.store.view

import id.gumilombok.data.remote.model.StoreModel
import id.gumilombok.data.remote.model.StoreProdukModel
import id.gumilombok.presentation.base.view.BaseView

interface StoreView : BaseView {
    fun showLoading(show: Boolean) {}
    fun onStoresLoad(stores: List<StoreModel>?, clearItem: Boolean) {}
    fun onLoadNewStore(stores:List<StoreModel>?){}
    fun onLoadNewProduk(produks:List<StoreProdukModel>?){}
    fun onLoadProdukByStoreId(produks:List<StoreProdukModel>?){}
}