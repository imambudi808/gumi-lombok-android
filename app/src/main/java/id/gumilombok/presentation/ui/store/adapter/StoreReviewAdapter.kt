package id.gumilombok.presentation.ui.store.adapter

import android.content.Context
import com.squareup.picasso.Picasso
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.data.remote.model.StoreReviewModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_rating.view.*

class StoreReviewAdapter(private val onItemClick: OnItemClick<StoreReviewModel>):
    BaseListAdapter<StoreReviewModel>() {
    override fun getItemView(context: Context): BaseViewHolder<StoreReviewModel> =Holder(context)

    inner class Holder(context: Context?):BaseViewHolder<StoreReviewModel>(context){
        override fun layoutResId(): Int = R.layout.item_rating

        override fun bind(item: StoreReviewModel, position: Int) {
            tvUserName.text=item.storeReviewerName
            tvComment.text=item.reviewComment
            rating_bar.rating=item.reviewRating!!.toFloat()
            tvCreatedAt.text=item.createdAt
        }

    }

}