package id.gumilombok.presentation.ui.ticket.presenter


import android.util.Log
import com.google.gson.Gson
import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager

import id.gumilombok.data.repository.TiketRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.ticket.view.TiketView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TiketPresenter @Inject constructor(val repo:TiketRepository,
                                         val sp:PreferencesManager): BasePresenter<TiketView>() {
    private val tag = this::class.java.simpleName

    fun showListTiket(offset:Int,limit:Int,clearItem:Boolean,text:String?=null){
        view?.showProgress((clearItem && text == null) || (!clearItem && text != null))
        view?.showLoadMoreProgress(!clearItem && text == null)

        val map = HashMap<String,String>()
        map["offset"] = offset.toString()
        map["limit"] = limit.toString()
        map["keyword"] = text.toString()
        disposables.add(
            repo.tiketList(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                    view?.showLoadMoreProgress(false)
                }
                .subscribe({
                    Log.i(tag,"load tiket: " + Gson().toJsonTree(it))
                    view?.onTiketLoad(it,clearItem)
                },{ doOnError(view,it)})
        )
    }



}