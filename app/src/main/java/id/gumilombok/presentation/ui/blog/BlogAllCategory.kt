package id.gumilombok.presentation.ui.blog

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.finish
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.BlogModel
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.presentation._costume.EndlessRecyclerViewScrollListener
import id.gumilombok.presentation.base.BaseFragment
import id.gumilombok.presentation.ui.blog.adapter.BlogAdapter
import id.gumilombok.presentation.ui.blog.presenter.BlogPresenter
import id.gumilombok.presentation.ui.blog.view.BlogView
import kotlinx.android.synthetic.main.fragment_blog_all_category.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class BlogAllCategory : BaseFragment(),BlogView {
    @Inject lateinit var presenter: BlogPresenter
    @Inject lateinit var sp: PreferencesManager
    private var query:String?=""
    lateinit var adapter: BlogAdapter
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item?.itemId == android.R.id.home) finish()

        return super.onOptionsItemSelected(item)
    }
    override fun injectModule(activityComponent: SyncComponent) {
        activityComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blog_all_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.bind(this,isOnline = true)
        setupView()
    }

    private fun setupView(){
        adapter=BlogAdapter(object :OnItemClick<BlogModel>{
            override fun onItemClick(item: BlogModel?, position: Int?) {
                val intent = Intent(activity, BlogDetailActivity::class.java)
                intent.putExtra("blogName",item?.blogJudul)
                intent.putExtra("blogImage",item?.blogImage)
                intent.putExtra("blogId",item?.blogId)
                intent.putExtra("blogIsi",item?.blogIsi)
                intent.putExtra("blogCreatorName",item?.blogCreator)
                intent.putExtra("blogCreatorDes",item?.blogCreatorDes)
                intent.putExtra("blogCreatorImage",item?.blogCreatorImage)
                intent.putExtra("blogView",item?.blogView)
                intent.putExtra("createdAt",item?.createdAt)
                intent.putExtra("seoLink",item?.seoLink)
                startActivity(intent)
            }

        },object :BlogAdapter.SearchResultCallback{
            override fun onDataNotFound(s: String) {
                TODO("Not yet implemented")
            }

        })

        val lm=GridLayoutManager(context,2)
        rv_blog_all_category.layoutManager=lm
        rv_blog_all_category.adapter=adapter
        rv_blog_all_category.invalidate()
        val scrollListener = object :EndlessRecyclerViewScrollListener(lm){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.showListsBlogs(totalItemsCount,totalItemsCount.plus(3),false,query)
            }

        }
        rv_blog_all_category.addOnScrollListener(scrollListener)
        presenter.showListsBlogs(0,3,true)
    }

    override fun onBlogLoads(blogs: List<BlogModel>?, clearItem: Boolean) {
        if (clearItem) adapter.clearItems()
        adapter.addItems(blogs)
    }

    override fun onBlogByCategoryload(blogs: List<BlogModel>?, clearItem: Boolean) {
        TODO("Not yet implemented")
    }

    override fun showMessage(message: String?) {

    }

    override fun showProgress(show: Boolean) {

    }

}
