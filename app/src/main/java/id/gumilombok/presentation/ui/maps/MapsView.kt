package id.gumilombok.presentation.ui.maps


import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.intentFor
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.model.Point
import id.gumilombok.presentation.ui.point.DetailPointActivity
import id.gumilombok.presentation.ui.point.adapter.PointAdapter
import id.gumilombok.presentation.ui.point.presenter.PointPresenter
import id.gumilombok.presentation.ui.point.view.PointView
import kotlinx.android.synthetic.main.activity_maps_view.*
import kotlinx.android.synthetic.main.toolbar.*
import java.io.IOException
import javax.inject.Inject

class MapsView : BaseActivity(), OnMapReadyCallback, PointView {
    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun displayPointLocationById(points: List<Point>?) {
       adapter.addItems(points)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }


    var destinationImage:String?=""
    var destinationLat:String?=""
    var destinationLong:String?=""
    var destinationName:String?=""
    var destinatinId:String?=""

    var destinationAuthor:String?=""
    var destinationContent:String?=""
    var destinationAddress:String?=""
    var IslandName:String?=""

    @Inject lateinit var presenter: PointPresenter
    private lateinit var adapter: PointAdapter

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    @Inject lateinit var sp:PreferencesManager
    private var googleMap: GoogleMap? = null

    var destinationLatLng:String?=""



    private lateinit var fusedLocationClient: FusedLocationProviderClient



    companion object{
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        //3
        private const val REQUEST_CHECK_SETTINGS = 2

        private const val PLACE_PICKER_REQUEST = 3

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps_view)
        setSupportActionBar(toolbar)
        presenter.bind(this,isOnline)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        destinationLatLng=intent.getStringExtra("destinationLatlong")
        destinationImage = intent.getStringExtra("destinationImage")
        destinatinId = intent.getStringExtra("destinationId")
        destinationLat= intent.getStringExtra("destinationLat")
        destinationLong= intent.getStringExtra("destinationLong")
        destinationName=intent.getStringExtra("destinationName")
        destinationAddress=intent.getStringExtra("destinationAddress")
        destinationAuthor=intent.getStringExtra("destinationAuthor")
        destinationContent=intent.getStringExtra("destinationContent")
        IslandName=intent.getStringExtra("destinationLokasi")
        title = "Lokasi Wisata"
//        destinationLatLng.toDouble()
        tv_alamat.text=destinationAddress
        tv_nama_wisata.text=destinationName



        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

/*
        adapter = PointAdapter(object : OnItemClick<Point> {
            override fun onItemClick(item:Point?, position:Int?){
                startActivity(intentFor<DetailPointActivity>().apply {
                    putExtra("latitude",item?.pointLat)
                    putExtra("longitude",item?.pointlong)
                    putExtra("pointName",item?.pointName)
                    putExtra("pointValue",item?.pointValue)
                    putExtra("pointCode",item?.pointCode)
                    putExtra("pointId",item?.pointId)

                })
            }
        })

        rvPoint.layoutManager = LinearLayoutManager(this)
        rvPoint.adapter = adapter
        presenter.loadPointByIdDestination(destinatinId)
 */

    }
    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
//        val latLngOrigin = LatLng(-7.763051, 110.419431) // Ayala
        val latLngDestination = LatLng(destinationLat!!.toDouble(), destinationLong!!.toDouble()) // SM City
//        this.googleMap!!.addMarker(MarkerOptions().position(latLngOrigin).title("Ayala"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination).title(destinationName))
        this.googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngDestination, 14.5f))

    }

}
