package id.gumilombok.presentation.ui.dompet.presenter

import android.content.SharedPreferences

import android.util.Log.i
import com.google.gson.Gson
import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager

import id.gumilombok.data.repository.DompetPointRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.dompet.view.DompetView
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class DompetPresenter @Inject constructor(val repo:DompetPointRepository,
                                          val sp:PreferencesManager):BasePresenter<DompetView>() {

    private val tag = this::class.java.simpleName

    fun savePointToDompet(pointValue:String?,userId:String?,pointName:String?,pointId:String?){
        view?.showProgress(true)
        val map = HashMap<String,String?>()
        map["point_value"] = pointValue
        map["user_id"] = userId
        map["point_name"] = pointName
        map["point_id"] = pointId

        i(tag,"map :"+Gson().toJsonTree(map))
        disposables.add(
            repo.savePointToDompetPoint(map)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{view?.showProgress(false)}
                .subscribe(
                    {view?.onSuccess(it.toMutableMap())},
                    { doOnError(view,it)}
                )
        )
    }

}