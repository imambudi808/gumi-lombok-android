package id.gumilombok.presentation.ui.destination.presenter

import android.util.Log
import com.google.gson.Gson
import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.repository.DestinationRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.destination.view.DestinationView
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class DestinationPresenter @Inject constructor(val repo: DestinationRepository,
                                               val sp:PreferencesManager):BasePresenter<DestinationView>() {

    private val tag = this::class.java.simpleName

    fun showListDestination(offset: Int, limit: Int, clearItem: Boolean, text: String? = null) {
        view?.showProgress((clearItem && text == null) || (!clearItem && text != null))
        view?.showLoadMoreProgress(!clearItem && text == null)

        val map = HashMap<String, String?>()
        map["offset"] = offset.toString()
        map["limit"] = limit.toString()
        map["keyword"] = text
        disposables.add(
            repo.destinationList(map)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach {
                    view?.showProgress(false)
                    view?.showLoadMoreProgress(false)
                }
                .subscribe({
                    Log.i(tag, "load destination: " + Gson().toJsonTree(it))
                    view?.onDestinationLoaded(it, clearItem)
                }, { doOnError(view, it) }))
    }

    fun loadDesPhotoByIdDes(destId:String?){
        view?.showProgress(true)
        disposables.add(
            repo.loadDesPhotos(destId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(tag, "load destination photo more: " + Gson().toJsonTree(it))
                    view?.onDesPhotoLoaded(it)
                },{ doOnError(view,it)})
        )
    }

    fun showNewDes() {
        disposables.add(
            repo.newDes()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach {
//                    view?.showProgress(false)
                    view?.showLoadMoreProgress(false)
                }
                .subscribe({
//                    Log.i(tag, "load new des: " + Gson().toJsonTree(it))
                    view?.newDesLoad(it)
                }, { doOnError(view, it) }))
    }

    fun updateDestination(destId: String?,rataRaing:String?,jumlahResponse:String?) {
        disposables.add(
            repo.pdateDestinationRate(destId,rataRaing,jumlahResponse)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach {
                    view?.showProgress(false)
                    view?.showLoadMoreProgress(false)
                }
                .subscribe({
                    Log.i(tag, "load new des: " + Gson().toJsonTree(it))
//                    view?.newDesLoad(it)
                }, { doOnError(view, it) }))
    }


}