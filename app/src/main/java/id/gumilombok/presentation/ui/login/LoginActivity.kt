package id.gumilombok.presentation.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.facebook.CallbackManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.gson.Gson
import id.gumilombok.R
import id.gumilombok.common.*
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.post.AuthPost
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.home.MainActivity
import id.gumilombok.presentation.ui.login.presenter.LoginPresenter
import id.gumilombok.presentation.ui.login.view.LoginView
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.toolbar.*
import java.net.URLDecoder
import javax.inject.Inject


class LoginActivity : BaseActivity(), LoginView {

    companion object {

        private const val RC_GOOGLE_SIGN_IN = 10

        private const val REQUEST_CODE__MAIN_ACTIVITY = 1000
        private const val REQUEST_CODE__SEARCH_FRAGMENT = 1001
        private const val REQUEST_CODE__NOTIFICATION_LIST_FRAGMENT = 1003
        private const val REQUEST_CODE__ITEM_LIST_FRAGMENT = 1004
        private const val REQUEST_CODE__COMMENT_LIST_FRAGMENT = 1005
        private const val REQUEST_CODE__ITEM_FRAGMENT = 1006
        private const val REQUEST_CODE__PROFILE_FRAGMENT = 1007
        private const val REQUEST_CODE__MAP_FILTERING = 1009
        private const val REQUEST_CODE__GOOGLE_SIGN = 1010
    }



    override fun onUpdateProfileSuccses() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



    override fun onRegistrasionSuccses() {
       toast("Registrasi berhasil")
        val dataAuthPost=AuthPost(sp.emailTampung!!,sp.idTampung!!)
        presenter.login(dataAuthPost)
    }


    @Inject
    lateinit var presenter: LoginPresenter

    @Inject
    lateinit var sp:PreferencesManager

    private lateinit var gso: GoogleSignInOptions
    lateinit var mGoogleSignInOptions: GoogleSignInOptions
    private val mSignInClient: GoogleSignInClient by lazy { GoogleSignIn.getClient(this, gso) }
    //Firebase test
    private val mAuth: FirebaseAuth? = null
    val TAG = LoginActivity::class.java.simpleName

    //google login

    //google login
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private val callbackManager: CallbackManager? = null
    private lateinit var firebaseAuth: FirebaseAuth




    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }
    override fun onLoginSuccess() {
        startActivity<MainActivity>()
        finish()
    }
    ///if login gmail dosnt exits
    override fun onUserNotRegistred() {
        presenter.registrasi(sp.gmailNameTamp!!,sp.emailTampung!!, sp.idTampung!!, sp.idTampung!!)
    }

    override fun showMessage(message: String?) {
        runOnUiThread { message?.let { toast(it) } }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setStatusBackgroundColor(getMyColor(R.color.white))
        setLightStatusBar(true)
        setSupportActionBar(toolbar)
        title = "Login"
        presenter.bind(this,isOnline)

        imgIllustration.loadImage(AssetUrl.LOGIN)



    /*

        tvSkip.setOnClickListener {
//            presenter.login("guest@gmail.com","nomorsatu1")
        }

        btnDaftar.setOnClickListener { startActivity<RegistrasiActivity>() }
        tvLupaPassword.setOnClickListener {

            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://gumilombok.id/password/reset")
            startActivity(intent)
        }
//        btnLogin.setOnClickListener { login() }

        checkPassword.setOnCheckedChangeListener{buttonView, isChecked ->
            val password = edtPassword.text.toString()
            if (isChecked) {
                edtPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            }else{
                edtPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            }
            edtPassword.setSelection(password.length)
        }

        val emailExtra=intent?.getStringExtra("email")

        if (emailExtra!=null){
            AlertDialog.Builder(this ?: return)
                .setMessage("Registrasi berhasil")
                .setPositiveButton("Ok") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
                .setNegativeButton("") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
                .show()
        }*/
        //google sign
        configureGoogleSignIn()
        firebaseAuth = FirebaseAuth.getInstance()
        btnLoginGoogle.setOnClickListener{
            signIn()
        }
        btnLoginFacebook.setOnClickListener{
            toast("Under Develope")
        }

    }

    override fun onBackPressed() {
        val mainActivity = Intent(Intent.ACTION_MAIN)
        mainActivity.addCategory(Intent.CATEGORY_HOME)
        mainActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(mainActivity)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            var task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                var account = task.getResult(ApiException::class.java)
                var acct: GoogleSignInAccount?

                firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
    }

    private fun signIn() {
        mGoogleSignInClient!!.signOut()
            .addOnSuccessListener {
                val signInIntent: Intent = mGoogleSignInClient!!.signInIntent
                startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN)
            }
            .addOnFailureListener {
                toast("Sorry, Signin process failed. Try again!")
            }

    }
//     gets an ID token from the GoogleSignInAccount object, exchange it for a Firebase credential, and authenticate with Firebase using the Firebase credential:
    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount?) {
        val credential = GoogleAuthProvider.getCredential(acct?.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {

                Log.d(TAG, "login firebase response : ${Gson().toJsonTree(it)}")
                Log.d(TAG,"Login it result res :${it}")
                Log.d(TAG,"account Response :${Gson().toJsonTree(acct)}")
                Log.d(TAG,"email Response :${Gson().toJsonTree(acct?.email)}")
                Log.d(TAG,"email2 Response :${acct?.email}")
                Log.d(TAG,"id Response :${acct?.id}")
                Log.d(TAG,"idToken Response :${acct?.idToken}")
                Log.d(TAG,"idToken Response :${acct?.displayName}")

                var email= URLDecoder.decode("imambudi808%40gmail.com", "UTF-8");

                Log.d(TAG,"email decode :${email}")
                val mail:String=acct?.email.toString()
                val pass:String=acct?.id.toString()
                val name:String=acct?.displayName.toString()
                val profileImage:String=acct?.photoUrl.toString()
                Log.d(TAG,"email decode :${mail}")

                sp.emailTampung=mail
                sp.idTampung=pass
                sp.gmailNameTamp=name
                sp.gmailProfilImageTamp=profileImage

                val dataAuthPost=AuthPost(mail,pass)
                presenter.login(dataAuthPost)


//                startActivity<MainActivity>()
            } else {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }

//    This can be achieved by checking if the current user is already signed in from within the onStart() method:
//    override fun onStart() {
//        super.onStart()
//        val user = FirebaseAuth.getInstance().currentUser
//        if (user != null) {
//            startActivity<MainActivity>()
//            finish()
//        }
//    }


}

