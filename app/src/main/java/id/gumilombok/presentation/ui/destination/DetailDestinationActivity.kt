package id.gumilombok.presentation.ui.destination


import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.UrlHelper
import id.gumilombok.common.intentFor
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.DestinationModel
import id.gumilombok.data.remote.model.PhotoModel
import id.gumilombok.data.remote.model.RatingModel
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.destination.adapter.PhotoDestinationAdapter
import id.gumilombok.presentation.ui.destination.presenter.DestinationPresenter
import id.gumilombok.presentation.ui.destination.view.DestinationView
import id.gumilombok.presentation.ui.home.adapter.NewDesAdapter
import id.gumilombok.presentation.ui.maps.MapsView
import id.gumilombok.presentation.ui.point.view.PointView
import id.gumilombok.presentation.ui.rating.adapter.RatingAdapter
import id.gumilombok.presentation.ui.rating.presenter.RatingPresenter
import id.gumilombok.presentation.ui.rating.view.RatingView
import kotlinx.android.synthetic.main.activity_detail_destination.*
import kotlinx.android.synthetic.main.detail_destinasi.*
import kotlinx.android.synthetic.main.item_detail_destinasi_baru.*
import kotlinx.android.synthetic.main.item_populer.*
import kotlinx.android.synthetic.main.lay_bintang.*
import kotlinx.android.synthetic.main.lay_bintang_edit.*
import javax.inject.Inject


class DetailDestinationActivity : BaseActivity(),PointView,DestinationView,RatingView {
    private val imageView: ImageView? = null
    override fun cekRatingV(status: String?, pointValue: String?, ratingKomen: String?) {
        if (pointValue?.toInt()!=0){
            inc_laBintang.visibility=View.GONE
            lay_bintangEdit.visibility=View.VISIBLE
            tvBeriUlasanText.text = "Komentar anda"
            textView50.text = "Ulasan yang anda berikan"
            tvNamaaaa.text = sp.userSession.name
            rating_bar_detail.rating=pointValue!!.toFloat()
//            if (pointValue=="1"){
//                bl1.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//            }
//            if (pointValue=="2"){
//                bl1.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//                bl.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//            }
//            if (pointValue=="3"){
//                bl1.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//                bl.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//                bl2.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//            }
//            if (pointValue=="4"){
//                bl1.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//                bl.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//                bl2.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//                bl3.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//            }
//            if (pointValue=="5"){
//                bl1.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//                bl.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//                bl2.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//                bl3.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//                bl4.background=ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
//            }
            tvKomentarSaya.text=ratingKomen
            tvImg.text = sp.userSession.name?.substring(0,1)?.toUpperCase()
            tvEdit.setOnClickListener {
                startActivity(intentFor<FormKementarActivity>().apply {
                    putExtra("destinationId",destinatinId);
                    putExtra("ratingJumlah",pointValue.toString())
                    //
                    putExtra("rataRating",ratarating)
                    putExtra("reviews",reviews)
                    putExtra("komentarExtra",ratingKomen)
                    bantuEdit="lama"
                    putExtra("bantuEdit",bantuEdit)

                })
            }
        }
    }


    private val url = UrlHelper()


    @Inject
    lateinit var sp: PreferencesManager

    @Inject lateinit var destinatinPresenter: DestinationPresenter
    @Inject lateinit var ratingPresenter: RatingPresenter


    private lateinit var photoAdapter: PhotoDestinationAdapter
    private lateinit var ratingAdapter: RatingAdapter
    private lateinit var neDesAdapter:NewDesAdapter


    private val context: Context = this

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }


    var destinatinId:String?=""
    var destinationImage:String?=""
    var destinationLat:String?=""
    var destinationLong:String?=""
    var destinationName:String?=""
    var destinationId:String?=""
    var ratarating:String?=""
    var jumlahReview:String?=""

    var destinationAuthor:String?=""
    var destinationContent:String?=""
    var destinationAddress:String?=""
    var IslandName:String?=""
    var format:Double?=0.0
    var reviews:String?=""
    var seoLink:String?=""
    private var bantuEdit:String?=""

    var statusCekRating = "false"
    var pointValue = 0
    override fun injectModule(activityComponent: ActivityComponent) {
       activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_destination)

        destinatinPresenter.bind(this,isOnline)
        ratingPresenter.bind(this,isOnline)

        destinationImage = intent.getStringExtra("destinationImage")
        destinatinId = intent.getStringExtra("destinationId")
        destinationLat= intent.getStringExtra("destinationLat")
        destinationLong= intent.getStringExtra("destinationLong")
        destinationName=intent.getStringExtra("destinationName")
        destinationAddress=intent.getStringExtra("destinationAddress")
        destinationAuthor=intent.getStringExtra("destinationAuthor")
        destinationContent=intent.getStringExtra("destinationContent")
        IslandName=intent.getStringExtra("destinationLokasi")
        ratarating=intent.getStringExtra("desRataRating")
        Log.d("Data Rata Rating : $ratarating",ratarating.toString())
        seoLink=intent.getStringExtra("seoLink")



        tv_nama_detail_destinasi.text = intent.getStringExtra("destinationName")
        Picasso.get().load(destinationImage).into(iv_detail_destinasi)
        var a=ratarating

        tv_deskripsi_destinasi.text = intent.getStringExtra("destinationAddress")
        ratingBarDesDetail.rating=a!!.toFloat()

//        btn_readmore.setOnClickListener{
//            val intent = Intent(Intent.ACTION_VIEW)
//            intent.data = Uri.parse("https://gumilombok.id/gumi-destination-detail/$seoLink")
//            startActivity(intent)
//        }



        btnDirection.setOnClickListener {
            startActivity(intentFor<MapsView>().apply {
                putExtra("destinationId", destinatinId)
                putExtra("destinationName",destinationName)
                putExtra("destinationAuthor",destinationAuthor)
                putExtra("destinationContent",destinationContent)
                putExtra("destinationImage",destinationImage)
                putExtra("destinationAddress",destinationAddress)
                putExtra("destinationLat",destinationLat)
                putExtra("destinationLong",destinationLong)
                putExtra("destinationLokasi",IslandName)
            })
        }

        ratingAdapter = RatingAdapter(object : OnItemClick<RatingModel> {

            override fun onItemClick(item:RatingModel?, position:Int?){

            }
        })


        rvUlasan.layoutManager = LinearLayoutManager(this)
        rvUlasan.adapter = ratingAdapter
        ratingPresenter.loadRatingByDes(destinatinId)

        ratingPresenter.cekRating(sp.userSession.id.toString(),destinatinId)

        buutonRatingAction()
        share_detail_destinasi.setOnClickListener{
            share()
        }
        back_detail_destinasi.setOnClickListener{
            finish()
        }
        layout_rating.visibility=View.GONE
        tv_views.setOnClickListener {
            if (layout_rating.visibility==View.GONE){
                layout_rating.visibility=View.VISIBLE
            }else{
                layout_rating.visibility=View.GONE
            }
        }
        neDesAdapter = NewDesAdapter(object : OnItemClick<DestinationModel> {
            override fun onItemClick(item: DestinationModel?, position: Int?) {
                startActivity(intentFor<DetailDestinationActivity>().apply {
                    putExtra("destinationId", item?.destinationId)
                    putExtra("destinationName",item?.destinationName)
                    putExtra("destinationAuthor",item?.destinationAuthor)
                    putExtra("destinationContent",item?.destinationContent)
                    putExtra("destinationImage",item?.destinationImage)
                    putExtra("destinationAddress",item?.destinationAddress)
                    putExtra("destinationLat",item?.destinationLat)
                    putExtra("destinationLong",item?.destinationLong)
                    putExtra("destinationLokasi",item?.islands?.IslandName)
                    putExtra("desRataRating",item?.rataRating)
                    putExtra("seoLink",item?.seoLink)
                    Log.d("dat rata rating :",item?.rataRating)

                })
            }
        })

        rv_same_destination.layoutManager=LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        rv_same_destination.adapter=neDesAdapter
        destinatinPresenter.showNewDes()
    }

    private fun sendExtraString(rating:String){
        startActivity(intentFor<FormKementarActivity>().apply {
            putExtra("destinationId",destinatinId);
            putExtra("ratingJumlah",rating)
            //
            putExtra("rataRating",ratarating)
            putExtra("reviews",reviews)
            bantuEdit="baru"
            putExtra("bantuEdit",bantuEdit)

        })
    }
    private fun buutonRatingAction(){
        b1.setOnClickListener {
            b1.background= ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
            sendExtraString("1")
        }
        b2.setOnClickListener {
            b1.background= ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
            b2.background= ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
            sendExtraString("2")
        }
        b3.setOnClickListener {
            b1.background= ContextCompat.getDrawable(context, R.drawable.ic_star_berisi)
            b2.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            b3.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            sendExtraString("3")
        }
        b4.setOnClickListener {
            b1.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            b2.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            b3.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            b4.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            sendExtraString("4")
        }
        b5.setOnClickListener {
            b1.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            b2.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            b3.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            b4.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            b5.background= ContextCompat.getDrawable(context, id.gumilombok.R.drawable.ic_star_berisi)
            sendExtraString("5")
        }
    }


    override fun onloadRating(rating:List<RatingModel>?){
        ratingAdapter.addItems(rating)
        rating_value.text = "${ratingAdapter.itemCount} ulasan"
        tv_views.text="${ratingAdapter.itemCount} comment"
        reviews=ratingAdapter.itemCount.toString()
        if (reviews==null){
            tvUlasaaaaatulisan.text="Belum Ada Ulasan"
        }
    }

    override fun onDesPhotoLoaded(photos: List<PhotoModel>?) {
        photoAdapter.addItems(photos)
    }

    override fun newDesLoad(dest: ArrayList<DestinationModel>?) {
        neDesAdapter.addItems(dest)
    }

    override fun showMessage(message: String?) {
        message?.let {
//            toast(it)
        }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun share(){
        val share = Intent.createChooser(Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "https://developer.android.com/training/sharing/")

            // (Optional) Here we're setting the title of the content
            putExtra(Intent.EXTRA_TITLE, "Introducing content previews")

            // (Optional) Here we're passing a content URI to an image to be displayed
//            setClipData(destinationImage);
            setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }, null)
        startActivity(share)
    }



}
