package id.gumilombok.presentation.ui.profile

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import id.gumilombok.R
import id.gumilombok.common.intentFor
import id.gumilombok.common.startActivity
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.login.presenter.LoginPresenter
import id.gumilombok.presentation.ui.login.view.LoginView
import id.gumilombok.presentation.ui.splash.WelcomeActivity
import kotlinx.android.synthetic.main.activity_edit_password.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class EditPasswordActivity : BaseActivity(),LoginView {


    override fun onLoginSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRegistrasionSuccses() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onUpdateProfileSuccses() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onUserNotRegistred() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showMessage(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showProgress(show: Boolean) {
        progress?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    @Inject lateinit var sp:PreferencesManager
    @Inject lateinit var presenter:LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_password)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.bind(this, isOnline)
        title = "Edit Profile"
        btnSimpanProfile.setOnClickListener {
            save()
        }
    }
    private fun save(){
        var passwordLama = edtPasswordLama.text.toString()
        var passwordBaru = edtPasswordB.text.toString()
        var confirPassword = edtConfirPasswordBaru.text.toString()
        if (passwordLama!=sp.userSession.passwordnya){
            this.toast("Password Lama Salah")
        }
        else if (passwordBaru!=confirPassword){
            this.toast("Password Baru Salah")
        }else if (passwordLama==null || passwordLama==""){
            this.toast("Masukkan Password Lama")
        }else if (passwordBaru==null || passwordBaru==""){
            this.toast("Masukkan Password Baru")
        }else if (confirPassword==null || confirPassword==""){
            this.toast("Masukkan Konfirmasi Password")
        }else{
            presenter.updatePassword(sp.userSession.id.toString(),passwordBaru)
            sp.logOut()
            this.toast("Ganti password berhasil")
            startActivity<WelcomeActivity>()
        }
    }


}
