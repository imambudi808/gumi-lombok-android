package id.gumilombok.presentation.ui.blog.adapter

import android.content.Context
import com.squareup.picasso.Picasso
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.UrlHelper
import id.gumilombok.data.remote.model.BlogModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_blog_horizontal.view.*

class BlogAdapterHorizontal(private val onItemClick: OnItemClick<BlogModel>) : BaseListAdapter<BlogModel>() {
    override fun getItemView(context: Context): BaseViewHolder<BlogModel> = Holder(context)
    private val url = UrlHelper()

    inner class Holder(context: Context?) : BaseViewHolder<BlogModel>(context){
        override fun layoutResId(): Int = R.layout.item_blog_horizontal

        override fun bind(item: BlogModel, position: Int) {
            tv_blog_name.text=item.blogJudul.toString()
//            tv_views.text="50 views"

            Picasso.get().load(item.blogImage).into(img_blog)
            setOnClickListener { onItemClick.onItemClick(item, position) }
//            btnTukarPoint.setOnClickListener { onItemClick.onItemClick(item,position) }
        }

    }

}