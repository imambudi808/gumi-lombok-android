package id.gumilombok.presentation.ui.store.view

import id.gumilombok.data.remote.model.StoreReviewModel
import id.gumilombok.presentation.base.view.BaseView

interface StoreReviewView:BaseView {
    fun onReviewLoads(reviews:List<StoreReviewModel>?){}
}