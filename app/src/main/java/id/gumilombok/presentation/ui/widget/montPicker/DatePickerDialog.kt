package id.gumilombok.presentation.ui.widget.montPicker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import id.gumilombok.R
import id.gumilombok.common.*
import kotlinx.android.synthetic.main.dialog_mont_picker.*
import java.util.*

class DatePickerDialog:AppCompatDialogFragment() {
    private var m = Calendar.getInstance().get(Calendar.MONTH)
    private lateinit var onSelect:(Int,Int,String,String)->Unit

    companion object{
        fun newInstance() = DatePickerDialog()
    }

    private val monthAdapter = GroupAdapter<ViewHolder>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState:Bundle?):View?{
        return inflater.inflate(R.layout.dialog_mont_picker,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        showMonth()


    }

    private fun setupView() = view?.run {
        rvMonth.layoutManager = GridLayoutManager(context,4)
        rvMonth.adapter = monthAdapter



        val cal = Calendar.getInstance()

        val year = cal.get(Calendar.YEAR)
        tvYear.text = year.toString()
        tvMonth.text = getCurrentMonth3String()
    }

    private fun showMonth(){
        monthAdapter.clear()
        DateHelper.getDateList(

            Locale.getDefault()).forEach{
            monthAdapter.add(DateAdapter(it,this::onMonthSelected))
        }
        btnPreviousMont.setOnClickListener {
            setPreviousMonth()
        }

        btnNextMonth.setOnClickListener {
            setNextMonth()
        }

    }

    private fun setPreviousMonth() {
        var tMont = m
        var month = tvMonth.text.toString().toInt()
//        if (month > 2019)
            tvMonth.text = (--month).toString()
    }

    private fun setNextMonth() {
        var tMont = m.toString().toInt()
        tvtampung.text = tMont.toString()

        var t2=0
        t2=++tMont

        toast(t2.toString())
        if(tMont==11){
            tvMonth.text = "November"
        }
        else if (tMont==12){
            tvMonth.text = "Desember"
        }
//        var month = tvMonth.text.toString().toInt()
//        tvMonth.text = (++month).toString()
    }
//    fungsi untuk pindah month
//    private fun setPreviousYear() {
//        var year = tvYear.text.toString().toInt()
//        if (year > 2019)
//            tvYear.text = (--year).toString()
//    }
//
//    private fun setNextYear() {
//        var year = tvYear.text.toString().toInt()
//        tvYear.text = (++year).toString()
//    }

    private fun onMonthSelected(pair: Pair<Int, String>) {
            val year = tvYear.text.toString().toInt()
            val month = tvMonth.text.toString()
            onSelect(year, pair.first, pair.second,month)
        }

        fun setOnSelectedListener(onSelect: (Int, Int, String,String) -> Unit) {
            this.onSelect = onSelect
        }
}