package id.gumilombok.presentation.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import id.gumilombok.Gumi
import id.gumilombok.R
import id.gumilombok.R.layout.fragment_home
import id.gumilombok.common.*
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.*
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.di.module.SyncServiceModule
import id.gumilombok.presentation.base.BaseFragment
import id.gumilombok.presentation.ui.blog.BlogActivity
import id.gumilombok.presentation.ui.blog.BlogDetailActivity
import id.gumilombok.presentation.ui.blog.adapter.BlogAdapter
import id.gumilombok.presentation.ui.blog.presenter.BlogPresenter
import id.gumilombok.presentation.ui.blog.view.BlogView
import id.gumilombok.presentation.ui.covid.CovidActivity
import id.gumilombok.presentation.ui.covid.adapter.CovidAdapter
import id.gumilombok.presentation.ui.covid.presenter.CovidPresenter
import id.gumilombok.presentation.ui.covid.view.CovidView
import id.gumilombok.presentation.ui.destination.DestinationActivity
import id.gumilombok.presentation.ui.destination.DetailDestinationActivity
import id.gumilombok.presentation.ui.destination.presenter.DestinationPresenter
import id.gumilombok.presentation.ui.destination.view.DestinationView
import id.gumilombok.presentation.ui.event.EventActivity
import id.gumilombok.presentation.ui.event.EventDetailActivity
import id.gumilombok.presentation.ui.event.adapter.EventAdapter
import id.gumilombok.presentation.ui.event.presenter.EventPresenter
import id.gumilombok.presentation.ui.event.view.EventView
import id.gumilombok.presentation.ui.general_search.GeneralSearchActivity
import id.gumilombok.presentation.ui.home.adapter.NewDesAdapter
import id.gumilombok.presentation.ui.maps.AllMaps
import id.gumilombok.presentation.ui.store.StoreActivity
import id.gumilombok.presentation.ui.ticket.DetailTiketActivity
import id.gumilombok.presentation.ui.ticket.TicketActivity
import id.gumilombok.presentation.ui.ticket.adapter.TiketHomeAdapter
import id.gumilombok.presentation.ui.ticket.presenter.TiketPresenter
import id.gumilombok.presentation.ui.ticket.view.TiketView
import kotlinx.android.synthetic.main.activity_store.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.home_icon_menu.*
import kotlinx.android.synthetic.main.item_covid.*
import javax.inject.Inject


class HomeFragment:BaseFragment(), TiketView, DestinationView,BlogView,EventView,CovidView {


    override fun showLoadMoreProgress(show: Boolean) {

    }

    override fun showMessage(message: String?) {
        message?.let { Toast.makeText(getActivity(),it, Toast.LENGTH_SHORT).show(); }
    }

    override fun showProgress(show: Boolean) {

    }

//    private var listDes :ArrayList<DestinationModel>?=null
//    private var listBaru:ArrayList<DestinationModel>? = null

    private val TAG = this::class.java.simpleName
    @Inject
    lateinit var sp: PreferencesManager
    @Inject
    lateinit var tiketPresenter: TiketPresenter
    @Inject lateinit var desPresenter: DestinationPresenter
    @Inject lateinit var blogPresenter:BlogPresenter
    @Inject lateinit var eventPresenter: EventPresenter
    @Inject lateinit var covidPresenter:CovidPresenter

    lateinit var tiketAdapter: TiketHomeAdapter
    lateinit var newDesAdapter: NewDesAdapter
    lateinit var blogAdapter:BlogAdapter
    lateinit var eventAdapter:EventAdapter
    lateinit var covidAdapter:CovidAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(fragment_home, container, false)

    }

    override fun injectModule(activityComponent: SyncComponent) {
        activityComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this).load(sp.gmailProfilImageTamp).into(imgAvatar)
        tvSisaPoint.text = toCurrency(sp.userSession.total_point?.toInt()) + " Pt"
        tvNama.text = sp.userSession.name
//        tvNamaHome.text = sp.userSession.name?.substring(0,1)?.toUpperCase()


        btnDestination.setOnClickListener {
            startActivity<DestinationActivity>()
        }
        btnTiketTour.setOnClickListener {
            startActivity<TicketActivity>()
        }
        btnMaps.setOnClickListener {
            startActivity<StoreActivity>()
        }
        btnBlog.setOnClickListener{
            startActivity<BlogActivity>()
        }
        btnEvent.setOnClickListener{
            startActivity<EventActivity>()
        }
        btSearchHomeF.setOnClickListener {
            view.findNavController().navigate(R.id.action_homeFragment_to_searchDesFragment)
        }
        setupView()
    }



    override fun onResume() {
        super.onResume()
        setupView()
    }

    private fun setupView() = view?.run {
        tiketPresenter.bind(this@HomeFragment,isNetworkAvailable(context))
        desPresenter.bind(this@HomeFragment,isNetworkAvailable(context))
        blogPresenter.bind(this@HomeFragment, isNetworkAvailable(context))
        eventPresenter.bind(this@HomeFragment, isNetworkAvailable(context))
        covidPresenter.bind(this@HomeFragment, isNetworkAvailable(context))
        showDestinationNew()
//        showTiketData()
        showBlogNew()
        showEventNew()
        showCovidHome()


    }

    private fun showCovidHome(){
        covidAdapter=CovidAdapter(object :OnItemClick<CovidModel>{

        })
        rvTiket.layoutManager = LinearLayoutManager(context)
        rvTiket.adapter = covidAdapter
        covidPresenter.loadDataCovidRi()
    }
    private fun showBlogNew(){
        blogAdapter = BlogAdapter(object :OnItemClick<BlogModel>{
            override fun onItemClick(item: BlogModel?, position: Int?) {
                val intent = Intent(activity, BlogDetailActivity::class.java)
                intent.putExtra("blogName",item?.blogJudul)
                intent.putExtra("blogImage",item?.blogImage)
                intent.putExtra("blogId",item?.blogId)
                intent.putExtra("blogIsi",item?.blogIsi)
                intent.putExtra("blogCreatorName",item?.blogCreator)
                intent.putExtra("blogCreatorDes",item?.blogCreatorDes)
                intent.putExtra("blogCreatorImage",item?.blogCreatorImage)
                intent.putExtra("blogView",item?.blogView)
                intent.putExtra("createdAt",item?.createdAt)
                intent.putExtra("seoLink",item?.seoLink)
                startActivity(intent)
            }

        },object:BlogAdapter.SearchResultCallback{
            override fun onDataNotFound(s: String) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })


        rvBlogHome.layoutManager = GridLayoutManager(context,2)
        rvBlogHome.adapter=blogAdapter
        blogPresenter.showListBlog(0.toString(),2.toString())

    }
    private fun showEventNew(){
        eventAdapter=EventAdapter(object :OnItemClick<EventModel>{
            override fun onItemClick(item: EventModel?, position: Int?) {
                val intent = Intent(activity, EventDetailActivity::class.java)
                intent.putExtra("eventId",item?.eventId)
                intent.putExtra("eventName",item?.eventName)
                intent.putExtra("eventImage",item?.eventImage)
                intent.putExtra("eventCost",item?.eventCost)
                intent.putExtra("eventLocation",item?.eventLocation)
                intent.putExtra("eventDate",item?.eventDate)
                intent.putExtra("createdAt",item?.createdAt)
                intent.putExtra("seoLink",item?.seoLink)
                startActivity(intent)
            }

        },object :EventAdapter.SearchResultCallback{
            override fun onDataNotFound(s: String) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })
        rvEventHome.layoutManager=LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        rvEventHome.adapter=eventAdapter
        eventPresenter.showEvents(0,2,true)
    }
    private fun showDestinationNew(){
        newDesAdapter = NewDesAdapter(object : OnItemClick<DestinationModel> {
            override fun onItemClick(item: DestinationModel?, position: Int?) {
                val intent = Intent(activity, DetailDestinationActivity::class.java)
                intent.putExtra("destinationId", item?.destinationId)
                intent.putExtra("destinationName",item?.destinationName)
                intent.putExtra("destinationAuthor",item?.destinationAuthor)
                intent.putExtra("destinationContent",item?.destinationContent)
                intent.putExtra("destinationImage",item?.destinationImage)
                intent.putExtra("destinationAddress",item?.destinationAddress)
                intent.putExtra("destinationLat",item?.destinationLat)
                intent.putExtra("destinationLong",item?.destinationLong)
                intent.putExtra("destinationLokasi",item?.islands?.IslandName)
                intent.putExtra("desRataRating",item?.rataRating)
                intent.putExtra("desJumlahReview",item?.jumlahResponse)
                intent.putExtra("seoLink",item?.seoLink)
                startActivity(intent)
            }
        })

        rvPopDes.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        rvPopDes.adapter = newDesAdapter
        desPresenter.showNewDes()
    }
    private fun showTiketData(){
        tiketAdapter = TiketHomeAdapter(object : OnItemClick<TiketModel> {
            override fun onItemClick(item: TiketModel?, position: Int?) {
                val intent = Intent(activity, DetailTiketActivity::class.java)
                intent.putExtra("tiketId",item?.tiketId)
                intent.putExtra("tiketAgent",item?.tiketAgent)
                intent.putExtra("tiketName",item?.tiketName)
                intent.putExtra("tiketPrice",item?.tiketPrice)
                intent.putExtra("tiketRoute",item?.tiketRoute)
                intent.putExtra("tiketKeberangkatan",item?.tiketKeberangkatan)
                intent.putExtra("tiketDetail",item?.tiketDetail)
                intent.putExtra("tiketAvailableBook",item?.tiketAvailableBook)
                intent.putExtra("durasi",item?.tiketDurasi)
                intent.putExtra("tiketPriceChil",item?.tiketPriceChil)
                startActivity(intent)

            }
        }, object : TiketHomeAdapter.SearchResultCallback {
            override fun onDataNotFound(s: String) {

            }
        })

        rvTiket.layoutManager = LinearLayoutManager(context)
        rvTiket.adapter = tiketAdapter
        tiketPresenter.showListTiket(0,1,true)


    }
    override fun onTiketLoad(tikets: List<TiketModel>?, clearItem: Boolean) {
        if (clearItem) tiketAdapter.clearItems()
        tiketAdapter.addItems(tikets)
    }

    override fun newDesLoad(dest:ArrayList<DestinationModel>?){
        if (newDesAdapter.isEmpty()){
            newDesAdapter.addItems(dest)
            sm_des.stopShimmer()
            sm_des.visibility = View.GONE
        }
//        sm_des.stopShimmer()
//        sm_des.visibility=View.GONE
//        listDes=dest
//        newDesAdapter.addItems(listBaru)
//        if (dest!=null) {
//            sm_des.stopShimmer()
//            sm_des.visibility = View.GONE
//            newDesAdapter.addItems(dest)
//        }


    }

    override fun onBlogLoad(blogs: List<BlogModel>?) {
        if (blogAdapter.isEmpty()){
            sm_blog.stopShimmer()
            sm_blog.visibility=View.GONE
            blogAdapter.addItems(blogs)
        }

//        if (blogs!=null){
//            sm_blog.stopShimmer()
//            sm_blog.visibility=View.GONE
//            blogAdapter.addItems(blogs)
//        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    override fun onCovidLoad(covid:List<CovidModel>?){
        if (covidAdapter.isEmpty()){
            covidAdapter.addItems(covid)
        }
    }

    override fun onEventLoads(events: List<EventModel>?, clearItem: Boolean) {
       if (eventAdapter.isEmpty()){
           sm_event.stopShimmer()
           sm_event.visibility=View.GONE
           eventAdapter.addItems(events)
       }
    }

    override fun onBlogLoads(blogs: List<BlogModel>?, clearItem: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBlogByCategoryload(blogs: List<BlogModel>?, clearItem: Boolean) {
        TODO("Not yet implemented")
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
//        outState.putParcelableArrayList("DATA_DES",listDes)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        val a = savedInstanceState?.getParcelableArrayList<DestinationModel>("DATA_DES")
//        listBaru=a


    }


}