package id.gumilombok.presentation.ui.blog

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.gumilombok.R
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.presentation.base.BaseFragment

/**
 * A simple [Fragment] subclass.
 */
class BlogByCategoryFragment : BaseFragment() {
    override fun injectModule(activityComponent: SyncComponent) {
        activityComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blog_by_category, container, false)
    }

}
