package id.gumilombok.presentation.ui.widget.montPicker

import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_mp_month.view.*
import id.gumilombok.R

class DateAdapter(private val item:Pair<Int,String>,
                  private val onSelect:(Pair<Int,String>)->Unit): Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.run {
            tvMonth.text = item.second
            tvMonth.setOnClickListener {
                onSelect(item)
            }
        }
    }

    override fun getLayout(): Int= R.layout.item_mp_month
}