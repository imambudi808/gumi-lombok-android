package id.gumilombok.presentation.ui.ticket


import android.app.DatePickerDialog
import android.os.Bundle

import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.intentFor

import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager

import id.gumilombok.data.remote.model.TiketModel
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.EndlessRecyclerViewScrollListener
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.ticket.adapter.TiketAdapter
import id.gumilombok.presentation.ui.ticket.presenter.TiketPresenter
import id.gumilombok.presentation.ui.ticket.view.TiketView

import kotlinx.android.synthetic.main.activity_destination.layToolbar

import kotlinx.android.synthetic.main.activity_ticket.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class TicketActivity : BaseActivity(),TiketView {
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }


    @Inject
    lateinit var presenter:TiketPresenter
    @Inject
    lateinit var sp:PreferencesManager
    private var query:String?=""

    lateinit var adapter:TiketAdapter
    var cal = Calendar.getInstance()
    private var tglBerangkat :String?=""




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket)
        setSupportActionBar(layToolbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Tiket Tour"
        presenter.bind(this, isOnline)
        setUpView()




        // create an OnDateSetListener
        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        }

        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        btnGantiTgl!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                DatePickerDialog(this@TicketActivity,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
            }

        })

    }


    private fun setUpView(){
        updateDateInView()

        adapter = TiketAdapter(object : OnItemClick<TiketModel> {
            override fun onItemClick(item: TiketModel?, position: Int?) {
                startActivity(intentFor<DetailTiketActivity>().apply {
                    putExtra("tiketId",item?.tiketId)
                    putExtra("tiketAgent",item?.tiketAgent)
                    putExtra("tiketName",item?.tiketName)
                    putExtra("tiketPrice",item?.tiketPrice)
                    putExtra("tiketRoute",item?.tiketRoute)
                    putExtra("tiketKeberangkatan",item?.tiketKeberangkatan)
                    putExtra("tiketDetail",item?.tiketDetail)
                    putExtra("tiketAvailableBook",item?.tiketAvailableBook)
                    putExtra("durasi",item?.tiketDurasi)
                    putExtra("tiketPriceChil",item?.tiketPriceChil)
                    putExtra("tanggalBerangkat",tglBerangkat)
                })
            }
        }, object : TiketAdapter.SearchResultCallback {
            override fun onDataNotFound(s: String) {
                presenter.showListTiket(0, 5, false, s)
            }
        })



        val layoutManager = LinearLayoutManager(this)
        recTransaksi.layoutManager = layoutManager
        recTransaksi.adapter = adapter
        recTransaksi.invalidate()
        val scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.showListTiket(totalItemsCount, totalItemsCount.plus(5), false, query)
            }
        }
        recTransaksi.addOnScrollListener(scrollListener)
        presenter.showListTiket(0, 5, true)
//        btnGantiTgl.setOnClickListener {
//            DatePickerDialog(this@TicketActivity,
//                dateSetListener,
//                // set DatePickerDialog to point to today's date when it loads up
//                cal.get(Calendar.YEAR),
//                cal.get(Calendar.MONTH),
//                cal.get(Calendar.DAY_OF_MONTH)).show()
//        }
    }

    private fun updateDateInView() {
        val myFormat = "MM-dd-yyyy" // mention the format you need
        val sdf = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault())
        tvTanggalReservasi!!.text = "Tanggal Berangkat : ${sdf.format(cal.getTime())}"
        tglBerangkat=sdf.format(cal.getTime()).toString()
    }

    override fun onRestart() {
        super.onRestart()
        setUpView()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchItem: MenuItem? = menu?.findItem(R.id.act_search)
        val searchView = searchItem?.actionView as SearchView?
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean = false

            override fun onQueryTextChange(newText: String?): Boolean {

                query = newText
                adapter.filter.filter(query)
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }



    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun showLoadMoreProgress(show: Boolean) {
        loadMoreProgress.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }
    override fun onTiketLoad(tikets: List<TiketModel>?, clearItem: Boolean) {
        if (clearItem) adapter.clearItems()
        adapter.addItems(tikets)
    }

}
