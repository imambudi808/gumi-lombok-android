package id.gumilombok.presentation.ui.store

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.squareup.picasso.Picasso
import id.gumilombok.R
import kotlinx.android.synthetic.main.activity_produk_detail.*
import kotlinx.android.synthetic.main.toolbar.view.*

class ProdukDetailActivity : AppCompatActivity() {

    private var storeId:String?=""
    private var produkId:String?=""
    private var produkName:String?=""
    private var produkBanner:String?=""
    private var produkDes:String?=""
    private var seoLink:String?=""
    private var storePhoneNumber:String?=""
    private var harga:String?=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_produk_detail)
        setSupportActionBar(layToolbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title="Produk Detail"
        setupView()
    }
    fun setupView(){
        storeId=intent?.getStringExtra("storeId")
        produkId=intent?.getStringExtra("produkId")
        produkName=intent?.getStringExtra("produkName")
        produkBanner=intent?.getStringExtra("produkBanner")
        produkDes=intent?.getStringExtra("produkDes")
        seoLink=intent?.getStringExtra("seoLink")
        storePhoneNumber=intent?.getStringExtra("storePhoneNumber")
        harga=intent?.getStringExtra("produkHarga")

        Picasso.get().load(produkBanner).into(produk_banner)
        tv_produk_name.text=produkName
        tv_harga.text="RP. ${harga}"
        tv_deskripsi.text=produkDes

    }
}
