package id.gumilombok.presentation.ui.ticket.presenter

import android.util.Log
import com.google.gson.Gson
import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.repository.ServiceTiketRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.ticket.view.TiketServiceView
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class TiketServicePresenter @Inject constructor(val repo: ServiceTiketRepository,
                                         val sp: PreferencesManager
): BasePresenter<TiketServiceView>() {
    private val tag = this::class.java.simpleName

    fun showTiketServiceByTiket(tiketId:String){


        val map = HashMap<String,String?>()
        map["tiket_id"] = tiketId

        disposables.add(
            repo.tiketServiceList(map)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(tag,"load tiket: " + Gson().toJsonTree(it))
                    view?.onServiceLoad(it)
                },{ doOnError(view,it)})
        )
    }



}