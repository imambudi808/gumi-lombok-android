package id.gumilombok.presentation.ui.store.adapter


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragments = mutableListOf<Fragment>()
    private val titles = mutableListOf<String>()

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getPageTitle(position: Int): CharSequence = titles[position]

    override fun getCount(): Int = fragments.size

    fun addFragment(fragment: Fragment, title: String = "") {
        fragments.add(fragment)
        titles.add(title)
    }

    fun clear() {
        fragments.clear()
        titles.clear()
    }

}