package id.gumilombok.presentation.ui.event.view

import id.gumilombok.data.remote.model.EventModel
import id.gumilombok.presentation.base.view.BaseView

interface EventView:BaseView {
    fun showLoadMore(showLoading:Boolean){}
    fun onEventLoads(events:List<EventModel>?,clearItem:Boolean){}
}