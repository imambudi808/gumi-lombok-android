package id.gumilombok.presentation.ui.login

import android.os.Bundle
import android.util.Patterns
import android.view.View
import id.gumilombok.R
import id.gumilombok.common.hideSoftKeyboard
import id.gumilombok.common.intentFor
import id.gumilombok.common.startActivity
import id.gumilombok.common.toast
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.login.presenter.LoginPresenter
import id.gumilombok.presentation.ui.login.view.LoginView
import kotlinx.android.synthetic.main.activity_registrasi.*
import java.util.regex.Pattern
import javax.inject.Inject


//////////////registrasi tidak terpakai lagi
class RegistrasiActivity : BaseActivity(),LoginView {


    override fun onUpdateProfileSuccses() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onUserNotRegistred() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @Inject lateinit var presenter:LoginPresenter
    override fun onLoginSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRegistrasionSuccses() {
        startActivity(intentFor<LoginActivity>().apply {
            putExtra("email",edtEmail.toString())
        })
    }

    override fun showMessage(message: String?) {
        runOnUiThread { message?.let { toast(it) } }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrasi)
        setSupportActionBar(toolbar)
        title = "Registrasi"
        presenter.bind(this,isOnline)

        btnLogin.setOnClickListener {
            startActivity<LoginActivity>()
        }

        btnDaftar.setOnClickListener {
            register()
        }
    }

    private fun isValidMobile(phone: String): Boolean {
        return if (!Pattern.matches("[a-zA-Z]+", phone)) {
            phone.length > 6 && phone.length <= 13
        } else false
    }



    fun register(){
        val name = edtName.text.toString()
        val phone=edtPhone.text.toString()
        val email=edtEmail.text.toString()
        val password = edtPassword.text.toString()
        val confirPassword=edtConfirPassword.text.toString()

        if (name.isNullOrEmpty()){
            showMessage("nama tidak boleh kosong")
            return
        }
        if (phone.isNullOrEmpty()){
            showMessage("nomor HP tidak boleh kosong")
            return
        }
        if (email.isNullOrEmpty()){
            showMessage("email tidak boleh kosong e")
        }
        if (Patterns.EMAIL_ADDRESS.matcher(email).matches() != true){
            showMessage("email tidak valid")
        }
        if (isValidMobile(phone) != true){
            showMessage("nomor hp tidak valid")
        }
        if (password.isNullOrEmpty()){
            showMessage("password tidak boleh kosong")
        }
        if (password.length <= 6){
            showMessage("password harus lebih dari 6 karakter")
        }
        if (confirPassword.isNullOrEmpty()){
            showMessage("konfirmasi password tidak boleh kosong")
        }
        if (confirPassword != password )
        {
            showMessage("konfirmasi password salah")
        }
        hideSoftKeyboard(this)
        if (confirPassword!=null && phone!=null && email!=null && password!=null && confirPassword!=null && confirPassword==password && Patterns.EMAIL_ADDRESS.matcher(email).matches() && isValidMobile(phone)== true &&  password.length > 6 ){
            presenter.registrasi(name,email, password, confirPassword)
        }

    }
}
