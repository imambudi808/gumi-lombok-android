package id.gumilombok.presentation.ui.login.presenter

import android.util.Log
import com.google.gson.Gson
import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.LoginModel
import id.gumilombok.data.remote.post.AuthPost
import id.gumilombok.data.repository.AuthRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.login.LoginActivity
import id.gumilombok.presentation.ui.login.view.LoginView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LoginPresenter @Inject constructor(val sp:PreferencesManager,
                                         val repo: AuthRepository
): BasePresenter<LoginView>() {
    val TAG = LoginActivity::class.java.simpleName

    fun login(data:AuthPost) {
        view?.showProgress(true)
        disposables.add(
            repo.login(data)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnEach { view?.showProgress(false) }
            .subscribe(
                {
                    Log.d(TAG, "login response : ${Gson().toJsonTree(it)}")
                    view?.onLoginSuccess()
                    if (it.status!!) {
                        val loginData = Gson().fromJson(Gson().toJsonTree(it.data), LoginModel::class.java)
                        sp.setUserSession(loginData)
                        view?.onLoginSuccess()
                    } else {
//                        view?.showMessage("Login gagal! " + it.pesan)
                        view?.onUserNotRegistred()
                    }
                },
                {
//                    doOnError(view, it)
                    view?.onUserNotRegistred()
                }
            )
        )
    }

    fun registrasi(nama:String,email: String,password: String,confirPassword:String){
        view?.showProgress(true)

        disposables.add(
            repo.register(nama,email,password,confirPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(TAG,"berhasil registasi"+Gson().toJsonTree(it))
//                    view?.onRegistrasionSuccses()
                    if(it.status!!){
                        view?.onRegistrasionSuccses()
                    }else{
                        view?.showMessage("registrasi gagal "+it.pesan)
                    }
                },{ doOnError(view,it)})
        )

    }

    fun updateProfile(userId:String,nama:String,phone:String,email: String){
        view?.showProgress(true)

        disposables.add(
            repo.updateProfile(userId,nama,phone,email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(TAG,"berhasil update profile"+Gson().toJsonTree(it))
//                    view?.onRegistrasionSuccses()
                    if(it.status!!){
                        view?.onUpdateProfileSuccses()
                    }else{
                        view?.showMessage("update prfile gagal "+it.pesan)
                    }
                },{ doOnError(view,it)})
        )

    }

    fun updatePassword(userId:String,passwordBaru: String){
        view?.showProgress(true)

        disposables.add(
            repo.updatePassword(userId,passwordBaru)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(TAG,"berhasil update password"+Gson().toJsonTree(it))
//                    view?.onRegistrasionSuccses()
                    if(it.status!!){
                        view?.onUpdateProfileSuccses()
                    }else{
                        view?.showMessage("update password gagal "+it.pesan)
                    }
                },{ doOnError(view,it)})
        )

    }

    fun cekPoint(userId: String){
        disposables.add(
            repo.cekPoint(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
//                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(TAG,"berhasil cek point 1"+Gson().toJsonTree(it))
//                    view?.onRegistrasionSuccses()
                    if(it.status!!){
                        Log.i(TAG,"berhasil cek point"+Gson().toJsonTree(it.data))
                        sp.cekLastPoint = it.data.toString()
                        Log.i(TAG,"cek last point:" + sp.cekLastPoint)

                    }else{
//                        view?.showMessage("update password gagal "+it.pesan)
                    }
                },{ doOnError(view,it)})
        )
    }
}


//                        sp.cekLastPoint(it.data)
//                        view?.dispalayLastPoint()