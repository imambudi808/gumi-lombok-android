package id.gumilombok.presentation.ui.store

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.StoreReviewModel
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.presentation.base.BaseFragment
import id.gumilombok.presentation.ui.store.adapter.StoreReviewAdapter
import id.gumilombok.presentation.ui.store.presenter.StoreReviewPresenter
import id.gumilombok.presentation.ui.store.view.StoreReviewView
import kotlinx.android.synthetic.main.fragment_store_review.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class StoreReviewFragment : BaseFragment(),StoreReviewView {

    companion object {
        private const val STOREID = "storeId"
        fun newInstance(storeId: Int) = StoreReviewFragment().apply {
            arguments = Bundle().apply {
                putInt(STOREID,storeId)
            }
        }
    }
    @Inject lateinit var pref:PreferencesManager
    @Inject lateinit var reviewPresenter: StoreReviewPresenter
    lateinit var reviewAdapter:StoreReviewAdapter
    override fun injectModule(activityComponent: SyncComponent) {
        activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        reviewPresenter.bind(this,isOnline = true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_store_review, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    fun setupView(){
        showReviewByStoreId()
    }

    fun showReviewByStoreId(){
        val storeId=arguments?.getInt(STOREID)
        val userId=pref.userSession.id?.toInt()
        reviewAdapter=StoreReviewAdapter(object :OnItemClick<StoreReviewModel>{

        })
        val layoutManager=LinearLayoutManager(context)
        rv_review_frag.layoutManager=layoutManager
        rv_review_frag.adapter=reviewAdapter
        reviewPresenter.showReviewStore(storeId!!,userId!!)
    }

    override fun showMessage(message: String?) {
        TODO("Not yet implemented")
    }

    override fun showProgress(show: Boolean) {
    }

    override fun onReviewLoads(reviews:List<StoreReviewModel>?){
        reviewAdapter.addItems(reviews)
    }

}
