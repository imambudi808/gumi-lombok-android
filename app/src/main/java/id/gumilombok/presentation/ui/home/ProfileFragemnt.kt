package id.gumilombok.presentation.ui.home



import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.browser.customtabs.CustomTabsIntent
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import id.gumilombok.R
import id.gumilombok.common.*
import id.gumilombok.di.component.SyncComponent
import kotlinx.android.synthetic.main.fragment_profile.*
import id.gumilombok.presentation.ui.profile.ProfileMenuAdapter
import id.gumilombok.Gumi

import id.gumilombok.common.intentFor
import id.gumilombok.common.toCurrency
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.module.SyncServiceModule
import id.gumilombok.presentation.base.BaseFragment
import id.gumilombok.presentation.ui.profile.EditPasswordActivity
import id.gumilombok.presentation.ui.profile.EditProfileActivity

import id.gumilombok.presentation.ui.splash.WelcomeActivity

import kotlinx.android.synthetic.main.fragment_profile.imgAvatar
import org.jetbrains.anko.clearTop

import javax.inject.Inject


class ProfileFragemnt : BaseFragment() {

    @Inject lateinit var sp:PreferencesManager
    private var tabsIntent: CustomTabsIntent? = null
    private lateinit var firebaseAuth: FirebaseAuth
    private var mGoogleSignInClient: GoogleSignInClient? = null
//    private val context=Context
//    // Build a GoogleSignInClient with the options specified by gso.
//    private val mSignInClient: GoogleSignInClient by lazy { GoogleSignIn.getClient(context, gso) }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun injectModule(activityComponent: SyncComponent) {
        activityComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(this).load(sp.gmailProfilImageTamp).into(imgAvatar)
        tvName.text=sp.userSession.name
        tvEmail.text=sp.userSession.email
        tvSisaPoint2.text = toCurrency(sp.userSession.total_point?.toInt()) + " Pt"


        setupView()

    }

    private fun setupView() = view?.run {
        firebaseAuth= FirebaseAuth.getInstance()

        setupProfileMenu()
//        imgAvatar.loadImage(R.drawable.image_1)
        imgAvatar.setOnClickListener{
//            startActivity<EditProfileActivity>()
        }
        tvDukungan.setOnClickListener {
//            tabsIntent?.launchUrl(context, Uri.parse("https://gumilombok.id/"))
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://gumilombok.id/")
            startActivity(intent)
        }


    }

    private fun setupProfileMenu() = view?.run {
        val menuAdapter = GroupAdapter<ViewHolder>()
        rvProfileMenu.run {
            layoutManager = LinearLayoutManager(context)
            adapter = menuAdapter
        }


        val setting = Pair((R.drawable.ic_settings_black_24dp), context.getString(R.string.ganti_bahasa))
        menuAdapter.add(ProfileMenuAdapter(setting) {
            toast("Under Develope")
//            startActivity<EditPasswordActivity>()

        })

        val logout = Pair(R.drawable.ic_exit_to_app_black_24dp, context.getString(R.string.keluar))
        menuAdapter.add(ProfileMenuAdapter(logout) {

            askForSignOut()
        })
    }

    private fun askForSignOut() {
        AlertDialog.Builder(context ?: return)
            .setMessage("Anda yakin akan keluar")
            .setPositiveButton("Batal") { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .setNegativeButton("Ya") { dialogInterface, _ ->
                dialogInterface.dismiss()
                mGoogleSignInClient?.signOut()
//                mGoogleSignInClient.signOut()
                firebaseAuth.signOut()
                sp.logOut()
                startActivity(intentFor<WelcomeActivity>()?.clearTop())
                finish()
            }
            .show()
    }






}