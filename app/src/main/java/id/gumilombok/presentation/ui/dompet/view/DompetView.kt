package id.gumilombok.presentation.ui.dompet.view

import id.gumilombok.presentation.base.view.BaseView

interface DompetView:BaseView {
    fun onSuccess(result: MutableMap<String, String>?){}
}