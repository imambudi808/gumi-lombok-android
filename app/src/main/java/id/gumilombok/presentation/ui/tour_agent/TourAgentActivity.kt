package id.gumilombok.presentation.ui.tour_agent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.gumilombok.R

class TourAgentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tour_agent)
    }
}
