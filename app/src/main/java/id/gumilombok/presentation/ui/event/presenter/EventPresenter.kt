package id.gumilombok.presentation.ui.event.presenter

import id.gumilombok.common.doOnError
import id.gumilombok.data.repository.EventRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.event.view.EventView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class EventPresenter @Inject constructor(val repo:EventRepository):BasePresenter<EventView>() {
    private val tag=this::class.java.simpleName

    fun showEvents(offset:Int,limit:Int,clearItem:Boolean,text:String?=null){
        view?.showProgress((clearItem && text==null)||(!clearItem && text != null))
        view?.showLoadMore(!clearItem && text==null)
        val map=HashMap<String,String>()
        map["offset"] = offset.toString()
        map["limit"] = limit.toString()
        map["keyword"] = text.toString()
        disposables.add(
            repo.eventList(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showLoadMore(false)
                    view?.showProgress(false)
                }
                .subscribe({
                    view?.onEventLoads(it,clearItem)
                },{ doOnError(view,it)})
        )
    }
}