package id.gumilombok.presentation.ui.covid.presenter

import android.util.Log
import com.google.gson.Gson
import id.gumilombok.common.doOnError
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.data.repository.CovidRepository
import id.gumilombok.presentation.ui.covid.view.CovidView
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class CovidPresenter @Inject constructor(val repo:CovidRepository):BasePresenter<CovidView>() {
    private val tag = this::class.java.simpleName

    fun loadDataCovidRi(){
        view?.showProgress(true)
        disposables.add(
            repo.showCovidId()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(tag,"load covid :"+Gson().toJsonTree(it))
                    view?.onCovidLoad(it)
                },{ doOnError(view,it)})
        )
    }
}