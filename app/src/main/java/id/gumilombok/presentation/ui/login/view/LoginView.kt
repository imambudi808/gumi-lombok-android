package id.gumilombok.presentation.ui.login.view

import id.gumilombok.presentation.base.view.BaseView

interface LoginView: BaseView {
    fun onLoginSuccess()
    fun onRegistrasionSuccses()
    fun onUpdateProfileSuccses()
    fun onUserNotRegistred()

}