package id.gumilombok.presentation.ui.store.adapter

import android.content.Context
import com.squareup.picasso.Picasso
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.toCurrency
import id.gumilombok.data.remote.model.StoreProdukModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_produk.view.*

class ProdukAdapter(private val onItemClick: OnItemClick<StoreProdukModel>):
    BaseListAdapter<StoreProdukModel>() {
    override fun getItemView(context: Context): BaseViewHolder<StoreProdukModel> =Holder(context)

    inner class Holder(context: Context?):BaseViewHolder<StoreProdukModel>(context){
        override fun layoutResId(): Int = R.layout.item_produk

        override fun bind(item: StoreProdukModel, position: Int) {
            Picasso.get().load(item.produkBanner).into(tv_produk_banner)
            tv_produk_name.text=item.produkName
            tv_produk_harga.text="Rp. ${toCurrency(item.hargaAwal?.toInt())}"

            setOnClickListener { onItemClick.onItemClick(item,position) }
        }

    }

}