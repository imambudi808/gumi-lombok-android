package id.gumilombok.presentation.ui.blog

import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import androidx.core.view.marginBottom
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder

import id.gumilombok.R
import id.gumilombok.common.*
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.BlogModel

import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.EndlessRecyclerViewScrollListener
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.blog.adapter.BlogAdapter
import id.gumilombok.presentation.ui.blog.adapter.BlogAdapterHorizontal
import id.gumilombok.presentation.ui.blog.adapter.CategoryAdapter
import id.gumilombok.presentation.ui.blog.presenter.BlogPresenter
import id.gumilombok.presentation.ui.blog.view.BlogView
import id.gumilombok.presentation.ui.home.MainActivity
import kotlinx.android.synthetic.main.activity_blog.*
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.toolbar.view.*
import javax.inject.Inject


class BlogActivity : BaseActivity(),BlogView {

    @Inject lateinit var sp:PreferencesManager
    @Inject lateinit var blogPresenter:BlogPresenter
    lateinit var blogAdapter: BlogAdapterHorizontal
    lateinit var blogByCategoryAdapter:BlogAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blog)
        setupView()
    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }


    private fun setupView(){
        blogPresenter.bind(this,isOnline)
        setStatusBackgroundColor(getColor(R.color.white))
        setLightStatusBar(true)
        blogHotShow()
        showBlogByCategory()
        btnClose.setOnClickListener {
            startActivity<MainActivity>()
        }

    }

    private fun showBlogByCategory(){
        blogByCategoryAdapter=BlogAdapter(object :OnItemClick<BlogModel>{
            override fun onItemClick(item: BlogModel?, position: Int?) {
                startActivity(intentFor<BlogDetailActivity>().apply {
                    putExtra("blogName",item?.blogJudul)
                    putExtra("blogImage",item?.blogImage)
                    putExtra("blogId",item?.blogId)
                    putExtra("blogIsi",item?.blogIsi)
                    putExtra("blogCreatorName",item?.blogCreator)
                    putExtra("blogCreatorDes",item?.blogCreatorDes)
                    putExtra("blogCreatorImage",item?.blogCreatorImage)
                    putExtra("blogView",item?.blogView)
                    putExtra("createdAt",item?.createdAt)
                    putExtra("seoLink",item?.seoLink)
                })
            }

        },object :BlogAdapter.SearchResultCallback{
            override fun onDataNotFound(s: String) {
                toast("belum ada blog baru dalam kategori ini")
            }

        })

        val lm= GridLayoutManager(this,2)
        rv_blog_by_category.layoutManager=lm
        rv_blog_by_category.adapter=blogByCategoryAdapter
        rv_blog_by_category.invalidate()
        btn_all.setOnClickListener {
            blogByCategoryAdapter.filter.filter("")
            btn_all.background=getDrawable(R.drawable.bg_button_red)
            btn_teknologi.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_hiburan.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_wisata.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_kesehatan.background=getDrawable(R.drawable.bg_blue_radius_4)
//            blogPresenter.loadListBlogByCategory(0,6,"",true)
        }
        btn_teknologi.setOnClickListener {
            blogByCategoryAdapter.filter.filter("teknologi")
            btn_all.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_teknologi.background=getDrawable(R.drawable.bg_button_red)
            btn_hiburan.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_wisata.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_kesehatan.background=getDrawable(R.drawable.bg_blue_radius_4)
        }
        btn_hiburan.setOnClickListener {
            blogByCategoryAdapter.filter.filter("hiburan")
            btn_all.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_teknologi.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_hiburan.background=getDrawable(R.drawable.bg_button_red)
            btn_wisata.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_kesehatan.background=getDrawable(R.drawable.bg_blue_radius_4)
        }
        btn_wisata.setOnClickListener {
            blogByCategoryAdapter.filter.filter("wisata")
            btn_all.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_teknologi.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_hiburan.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_wisata.background=getDrawable(R.drawable.bg_button_red)
            btn_kesehatan.background=getDrawable(R.drawable.bg_blue_radius_4)
        }
        btn_kesehatan.setOnClickListener {
            blogByCategoryAdapter.filter.filter("kesehatan")
            btn_all.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_teknologi.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_hiburan.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_wisata.background=getDrawable(R.drawable.bg_blue_radius_4)
            btn_kesehatan.background=getDrawable(R.drawable.bg_button_red)
        }
        blogPresenter.loadListBlogByCategory(0,10,"",false)
    }

    private fun blogHotShow(){
        blogAdapter= BlogAdapterHorizontal(object :OnItemClick<BlogModel>{
            override fun onItemClick(item: BlogModel?, position: Int?) {
                startActivity(intentFor<BlogDetailActivity>().apply {
                    putExtra("blogName",item?.blogJudul)
                    putExtra("blogImage",item?.blogImage)
                    putExtra("blogId",item?.blogId)
                    putExtra("blogIsi",item?.blogIsi)
                    putExtra("blogCreatorName",item?.blogCreator)
                    putExtra("blogCreatorDes",item?.blogCreatorDes)
                    putExtra("blogCreatorImage",item?.blogCreatorImage)
                    putExtra("blogView",item?.blogView)
                    putExtra("createdAt",item?.createdAt)
                    putExtra("seoLink",item?.seoLink)
                })
            }


        })
        val layoutManager=LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        rv_blog_populer.layoutManager=layoutManager
        rv_blog_populer.adapter=blogAdapter
//        rv_blog_populer.invalidate()
        blogPresenter.loadHotBlog()
    }

    override fun onBlogLoads(blogs: List<BlogModel>?, clearItem: Boolean) {

    }

    override fun onBlogByCategoryload(blogs: List<BlogModel>?, clearItem: Boolean) {
        if (blogs!=null){
            blogByCategoryAdapter.clearItems()
        }
        blogByCategoryAdapter.addItems(blogs)
    }

    override fun onBlogLoad(blogs:List<BlogModel>?){
        blogAdapter.addItems(blogs)
    }

    override fun showMessage(message: String?) {

    }

    override fun showProgress(show: Boolean) {

    }


}


