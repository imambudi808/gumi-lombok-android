package id.gumilombok.presentation.ui.ticket.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Filter
import android.widget.Filterable
import com.google.gson.Gson
import id.gumilombok.common.OnItemClick
import id.gumilombok.data.remote.model.TiketModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import id.gumilombok.R
import id.gumilombok.common.toCurrency
import kotlinx.android.synthetic.main.item_ticket.view.*



class TiketAdapter(private val itemClickListener:OnItemClick<TiketModel>,
                   private val searchResultCallbacktiket: SearchResultCallback
):BaseListAdapter<TiketModel>(),Filterable{

    private val tiketCopy = items

    override fun getItemView(context: Context): BaseViewHolder<TiketModel> = ViewHolder(context)

    override fun getFilter(): Filter {
        return object:Filter(){
            override fun performFiltering(text: CharSequence?): FilterResults {
                val search = text?.toString() ?: ""
                val itemList:List<TiketModel> = if (search.isEmpty()){
                    tiketCopy
                }else{
                    tiketCopy.filter {
                        it.tiketId?.contains(search)==true || it.tiketName?.contains(search) == true || it.tiketAgent?.toLowerCase()?.contains(search) == true || it.tiketPrice?.toLowerCase()?.contentEquals(search) == true
                    }

                }
                val results = Filter.FilterResults()
                results.values = itemList
                Log.d("cek filter tiket","${Gson().toJsonTree(itemList)}")
                return results
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(s: CharSequence?, results: FilterResults?) {
                items = results?.values as ArrayList<TiketModel>
                if (items.isEmpty()){
                    searchResultCallbacktiket.onDataNotFound(s?.toString()?:"")
                }
                notifyDataSetChanged()
                Log.d("cek filter publist","${Gson().toJsonTree(items)}")
            }

        }
    }



    inner class ViewHolder(context: Context?) : BaseViewHolder<TiketModel>(context){
        override fun layoutResId(): Int = R.layout.item_ticket

        @SuppressLint("SetTextI18n")
        override fun bind(item: TiketModel, position: Int) {
            var harga = item.tiketPrice!!.toInt()


            tv_tiket_agent.text = item.tiketAgent.toString()
            tv_tiket_nama.text = item.tiketName.toString()
            tv_tiket_rute.text = item.tiketRoute.toString()
            tv_tiket_harga.text = "Rp ${toCurrency(harga)}"
           tv_hari_tour.text = "${item.tiketDurasi} Jam"


            setOnClickListener { itemClickListener.onItemClick(item, position) }
        }

    }

    interface SearchResultCallback{
        fun onDataNotFound(s:String)
    }
}