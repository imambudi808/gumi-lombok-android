package id.gumilombok.presentation.ui.maps

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import id.gumilombok.R
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

import com.google.android.gms.maps.model.BitmapDescriptorFactory

import android.graphics.drawable.Drawable
import android.graphics.PorterDuff
import android.graphics.Bitmap

import android.view.LayoutInflater
import androidx.annotation.DrawableRes

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.view.View
import android.widget.ImageView


class AllMaps : BaseActivity(), OnMapReadyCallback {



    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    @Inject
    lateinit var sp: PreferencesManager
    private var googleMap: GoogleMap? = null

    var destinationLatLng:String?=""



    private lateinit var fusedLocationClient: FusedLocationProviderClient



    companion object{
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        //3
        private const val REQUEST_CHECK_SETTINGS = 2

        private const val PLACE_PICKER_REQUEST = 3

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(id.gumilombok.R.layout.activity_all_maps)
        setSupportActionBar(toolbar)

        val mapFragment = supportFragmentManager.findFragmentById(id.gumilombok.R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

//        destinationLatLng.toDouble()



        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)



    }

    private fun getMarkerBitmapFromView(@DrawableRes resId: Int): Bitmap {

        val customMarkerView =
            (getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                id.gumilombok.R.layout.view_custom_marker,
                null
            )
        val markerImageView = customMarkerView.findViewById(id.gumilombok.R.id.profile_image) as ImageView
        markerImageView.setImageResource(resId)
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        customMarkerView.layout(
            0,
            0,
            customMarkerView.getMeasuredWidth(),
            customMarkerView.getMeasuredHeight()
        )
        customMarkerView.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(
            customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = customMarkerView.getBackground()
        if (drawable != null)
            drawable!!.draw(canvas)
        customMarkerView.draw(canvas)
        return returnedBitmap
    }
    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
        val latLngDestination = LatLng(-8.493639,116.295656) // Air Terjun Segenter
        val latLngDestination2 = LatLng(-8.647846,116.115494) // Banyu Mulek
        val latLngDestination4 = LatLng(-8.708508,116.106879) // Batu Idung
        val latLngDestination5 = LatLng(-8.592097,116.205907)// Botanic Garden
        val latLngDestination6 = LatLng(-8.517389,116.134527)//	Bukit Bintang Tiga Rasa
        val latLngDestination7 = LatLng(-8.665841,116.131294)//	Bundaran Gerung
        val latLngDestination8 = LatLng(-8.890350 , 116.009393)//	Buwun Mas
        val latLngDestination9 = LatLng(-8.730492 , 116.025613)//	Gili Gede
        val latLngDestination10 = LatLng(-8.730322 , 116.025591)//Gili Kedis
        val latLngDestination11 = LatLng(-8.727863 , 115.906830)//Gili Layar
        val latLngDestination12 = LatLng(-8.717438 , 116.008333)//Gili Nanggu
        val latLngDestination13 = LatLng(-8.718002 , 115.915024)//Gili Rengit
        val latLngDestination14 = LatLng(-8.724510 , 116.024124)//	Gili Sudak
        val latLngDestination15 = LatLng(-8.723634 , 116.013889)//Gili Tangkong
        val latLngDestination16 = LatLng(-8.642480 , 116.087500)////Gunung Pengson
        val latLngDestination17 = LatLng(-8.698997 , 116.161987)//Gunung Sasak
        val latLngDestination18 = LatLng(-8.761945 , 116.049687)//Hutan Mangrove
        val latLngDestination19 = LatLng(-8.729685 , 116.072802)//Pelabuhan Lembar
        val latLngDestination20 = LatLng(-8.603628 , 116.075846)//Loang Balok
        val latLngDestination21 = LatLng(-8.516262 , 116.060540)//Makam Batu Layar
        val latLngDestination22 = LatLng(-8.719101 , 116.057947)//Pantai Cemara
        val latLngDestination23 = LatLng(-8.673666 , 116.071076)//Pantai Indok
        val latLngDestination24 = LatLng(-8.679397 , 116.137634)//Penas
        val latLngDestination25 = LatLng(-8.495886 , 116.044659)//Sengigi
        val latLngDestination26 = LatLng(-8.487900 , 116.274143)//Sesaot
        val latLngDestination27 = LatLng(-8.576843 , 116.235194)//Surenadi
        val latLngDestination28 = LatLng(-8.595209 , 116.203588)//Taman Narmada
        val latLngDestination29 = LatLng(-8.625326 , 116.098178)//Tembolak
        val latLngDestination30 = LatLng(-8.520212 , 116.201896)//Tiu Timponan

//        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination).icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.ic_star_half_black_24dp))).title("tes"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination).title("Air Terjun Segenter"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination2).title("Banyu Mulek"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination4).title("Batu Idung"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination5).title("Botanic Garden"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination6).title("Bukit Bintang Tiga Rasa"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination7).title("Bundaran Gerung"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination8).title("Buwun Mas"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination9).title("Gili Gede"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination10).title("Gili Kedis"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination11).title("Gili Layar"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination12).title("Gili Nanggu"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination13).title("Gili Rengit"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination14).title("Gili Sudak"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination15).title("Gili Tangkong"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination16).title("Gunung Pengson"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination17).title("Gunung Sasak"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination18).title("Hutan Mangrove"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination19).title("Pelabuhan Lembar"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination20).title("Loang Balok"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination21).title("Makam Batu Layar"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination22).title("Pantai Cemara"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination23).title("Pantai Indok"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination24).title("Penas"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination25).title("Sengigi"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination26).title("Sesaot"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination27).title("Surenadi"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination28).title("Taman Narmada"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination29).title("Tembolak"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination30).title("Tiu Timponan"))

        this.googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngDestination20, 10.5f))

    }

}

