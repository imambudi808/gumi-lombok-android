package id.gumilombok.presentation.ui.store


import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.AttributeSet
import android.view.View

import com.squareup.picasso.Picasso
import id.gumilombok.R
import id.gumilombok.common.setOnScrollListener
import id.gumilombok.common.toast
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity

import id.gumilombok.presentation.ui.store.adapter.ViewPagerAdapter

import kotlinx.android.synthetic.main.activity_store_detail.*
import kotlinx.android.synthetic.main.activity_store_detail.layToolbar
import kotlinx.android.synthetic.main.toolbar.view.*

class StoreDetailActivity : BaseActivity()  {
    var storeId:Int?=0
    var storeName:String?=""
    var storeDes:String?=""
    var storeBanner:String?=""
    var storeProfile:String?=""
    var userId:String?=""
    var seoLink:String?=""
    var storePhoneNumber:String?=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_detail)
        setSupportActionBar(layToolbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title="Detail Toko"

        storeId=intent?.getIntExtra("storeId",0)
        storeName=intent?.getStringExtra("storeName")
        storeDes=intent?.getStringExtra("storeDes")
        storeBanner=intent?.getStringExtra("storeBanner")
        storeProfile=intent?.getStringExtra("storeProfile")
        userId=intent?.getStringExtra("userId")
        seoLink=intent?.getStringExtra("seoLink")
        storePhoneNumber=intent?.getStringExtra("storePhoneNumber")
        Picasso.get().load(storeProfile).into(img_profile_store)
        tv_store_name.text=storeName
        viewPagerStup()
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(name, context, attrs)

    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    private fun viewPagerStup(){
        val viewPagerAdapter=ViewPagerAdapter(supportFragmentManager)
        viewPagerAdapter.addFragment(
            ProdukFragment.newInstance(
                storeId!!
            ),getString(R.string.produk)
        )
        viewPagerAdapter.addFragment(
            StoreDetailFragment.newInstance(
                storeId!!
            ),getString(R.string.detail)
        )

        vpStore.adapter=viewPagerAdapter
        tabLayout.setupWithViewPager(vpStore)

    }


}
