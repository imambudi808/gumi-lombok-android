package id.gumilombok.presentation.ui.ticket

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import id.gumilombok.R
import id.gumilombok.common.intentFor

import id.gumilombok.common.toCurrency
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager

import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity

import id.gumilombok.presentation.ui.ticket.presenter.OrderPresenter
import id.gumilombok.presentation.ui.ticket.view.OrderView
import kotlinx.android.synthetic.main.activity_konfirmasi_transaksi.*
import kotlinx.android.synthetic.main.activity_konfirmasi_transaksi.tv_agent_name
import kotlinx.android.synthetic.main.tiket_konfir.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class KonfirmasiTransaksi : BaseActivity(),OrderView {

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }


    @Inject lateinit var sp:PreferencesManager
    @Inject lateinit var orderPresenter:OrderPresenter



    private var tiketId:String=""
    private var tiketAgent:String=""
    private var tiketName:String=""
    private var tiketPrice:String=""
    private var tiketRoute:String=""
    private var tiketKberangkatan:String=""
    private var tiketDetail:String=""
    private var tiketAvailableBook:String=""
    private var durasiTour:String=""
    private var tiketPriceChil:String=""
    private var jumlahBookAnak:String=""
    private var jumlahBookDewasa:String=""
    private var totalBayar:Int=0
    private var tglBerangkat:String?=""

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_konfirmasi_transaksi)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Isi Pesanan Tiket"
        orderPresenter.bind(this,isOnline)

        tiketId=intent.getStringExtra("tiketId")
        tiketAgent=intent.getStringExtra("tiketAgent")
        tiketName=intent.getStringExtra("tiketName")
        tiketPrice=intent.getStringExtra("tiketPrice")
        tiketRoute=intent.getStringExtra("tiketRoute")
        tiketKberangkatan=intent.getStringExtra("tiketKeberangkatan")
        tiketDetail=intent.getStringExtra("tiketDetail")
        tiketAvailableBook=intent.getStringExtra("tiketAvailableBook")
        durasiTour=intent.getStringExtra("durasi")
        tiketPriceChil=intent.getStringExtra("tiketPriceChil")
        tglBerangkat=intent.getStringExtra("tanggalBerangkat")

        layDataPemesan.visibility = View.GONE
        edtNama.setText(sp.userSession.name.toString())
        edtEmail.setText(sp.userSession.email.toString())

        var edtJmlahDewsa = edtJumlahTiketDewasa
        var edtJmlahAnak = edtJumlahTiketAnak
        jumlahBookDewasa = edtJmlahDewsa.text.toString()
        jumlahBookAnak = edtJmlahAnak.text.toString()
        if (jumlahBookDewasa == "")
        {
            jumlahBookDewasa="0"
        }
        if (jumlahBookAnak == "")
        {
            jumlahBookAnak="0"
        }

        tv_agent_name.text = tiketAgent
        tvTourName.text = tiketName
//        hadle kalo misalkan ada yg tidak unya harga tiket anak
            tvNameTiket1.visibility = View.GONE
            tvNameTiket2.visibility = View.GONE
            tvPriceDewasa.visibility = View.GONE
            tvPriceAnak.visibility = View.GONE
            edtJumlahTiketDewasa.visibility = View.GONE
            edtJumlahTiketAnak.visibility = View.GONE
            tvNameTiket1a.visibility = View.GONE
            tvNameTiket2a.visibility = View.GONE

            if (tiketPrice.isNotEmpty()){
                tvNameTiket1.visibility = View.VISIBLE
                tvPriceDewasa.visibility = View.VISIBLE
                tvPriceDewasa.text = "Rp ${toCurrency(tiketPrice.toInt())}"
                edtJumlahTiketDewasa.visibility = View.VISIBLE
                tvNameTiket1a.visibility = View.VISIBLE


            }
            if (tiketPriceChil.isNotEmpty()){
                tvNameTiket2.visibility = View.VISIBLE
                tvPriceAnak.visibility = View.VISIBLE
                tvPriceAnak.text = "Rp ${toCurrency(tiketPriceChil.toInt())}"
                edtJumlahTiketAnak.visibility = View.VISIBLE
                tvNameTiket2a.visibility = View.VISIBLE

            }


        if (tiketPrice.isEmpty()){
            totalBayar = jumlahBookAnak.toInt()*tiketPriceChil.toInt()
            tvTextTotal.text = "Harga total untuk ${jumlahBookAnak} tiket anak"
            tvDetailBayar.text = "( ${jumlahBookAnak}x${toCurrency(tiketPriceChil.toInt())} )"

        }else if (tiketPriceChil.isEmpty()){
            totalBayar = jumlahBookDewasa.toInt()*tiketPrice.toInt()
            tvTextTotal.text = "Harga total untuk ${jumlahBookDewasa} tiket dewasa"
            tvDetailBayar.text = "( ${jumlahBookDewasa}x${toCurrency(tiketPrice.toInt())})"
        }else{
            totalBayar = jumlahBookDewasa.toInt()*tiketPrice.toInt() + jumlahBookAnak.toInt()*tiketPriceChil.toInt()
            tvTextTotal.text = "Harga total untuk ${jumlahBookDewasa} tiket dewasa dan ${jumlahBookAnak} tiket anak"
            tvDetailBayar.text = "( ${jumlahBookDewasa}x${toCurrency(tiketPrice.toInt())} + ${jumlahBookAnak}x${toCurrency(tiketPriceChil.toInt())} )"
        }
        tvTotalBayar.text = "Rp ${toCurrency(totalBayar)}"




        edtJumlahTiketDewasa.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                jumlahBookDewasa = s.toString()
                if (jumlahBookDewasa == "")
                {
                    jumlahBookDewasa="0"
                }
                if (jumlahBookAnak == "")
                {
                    jumlahBookAnak="0"
                }
                if (tiketPrice.isEmpty()){
                    totalBayar = jumlahBookAnak.toInt()*tiketPriceChil.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookAnak} tiket anak"
                    tvDetailBayar.text = "( ${jumlahBookAnak}x${toCurrency(tiketPriceChil.toInt())} )"

                }else if (tiketPriceChil.isEmpty()){
                    totalBayar = jumlahBookDewasa.toInt()*tiketPrice.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookDewasa} tiket dewasa"
                    tvDetailBayar.text = "( ${jumlahBookDewasa}x${toCurrency(tiketPrice.toInt())})"
                }else{
                    totalBayar = jumlahBookDewasa.toInt()*tiketPrice.toInt() + jumlahBookAnak.toInt()*tiketPriceChil.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookDewasa} tiket dewasa dan ${jumlahBookAnak} tiket anak"
                    tvDetailBayar.text = "( ${jumlahBookDewasa}x${toCurrency(tiketPrice.toInt())} + ${jumlahBookAnak}x${toCurrency(tiketPriceChil.toInt())} )"
                }
                tvTotalBayar.text = "Rp ${toCurrency(totalBayar)}"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                jumlahBookDewasa = s.toString()
                if (jumlahBookDewasa == "")
                {
                    jumlahBookDewasa="0"
                }
                if (jumlahBookAnak == "")
                {
                    jumlahBookAnak="0"
                }
                if (tiketPrice.isEmpty()){
                    totalBayar = jumlahBookAnak.toInt()*tiketPriceChil.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookAnak} tiket anak"
                    tvDetailBayar.text = "( ${jumlahBookAnak}x${toCurrency(tiketPriceChil.toInt())} )"

                }else if (tiketPriceChil.isEmpty()){
                    totalBayar = jumlahBookDewasa.toInt()*tiketPrice.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookDewasa} tiket dewasa"
                    tvDetailBayar.text = "( ${jumlahBookDewasa}x${toCurrency(tiketPrice.toInt())})"
                }else{
                    totalBayar = jumlahBookDewasa.toInt()*tiketPrice.toInt() + jumlahBookAnak.toInt()*tiketPriceChil.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookDewasa} tiket dewasa dan ${jumlahBookAnak} tiket anak"
                    tvDetailBayar.text = "( ${jumlahBookDewasa}x${toCurrency(tiketPrice.toInt())} + ${jumlahBookAnak}x${toCurrency(tiketPriceChil.toInt())} )"
                }
                tvTotalBayar.text = "Rp ${toCurrency(totalBayar)}"
            }
        })
        //anak

        edtJumlahTiketAnak.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                jumlahBookAnak = s.toString()
                if (jumlahBookDewasa == "")
                {
                    jumlahBookDewasa="0"
                }
                if (jumlahBookAnak == "")
                {
                    jumlahBookAnak="0"
                }
                if (tiketPrice.isEmpty()){
                    totalBayar = jumlahBookAnak.toInt()*tiketPriceChil.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookAnak} tiket anak"
                    tvDetailBayar.text = "( ${jumlahBookAnak}x${toCurrency(tiketPriceChil.toInt())} )"

                }else if (tiketPriceChil.isEmpty()){
                    totalBayar = jumlahBookDewasa.toInt()*tiketPrice.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookDewasa} tiket dewasa"
                    tvDetailBayar.text = "( ${jumlahBookDewasa}x${toCurrency(tiketPrice.toInt())})"
                }else{
                    totalBayar = jumlahBookDewasa.toInt()*tiketPrice.toInt() + jumlahBookAnak.toInt()*tiketPriceChil.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookDewasa} tiket dewasa dan ${jumlahBookAnak} tiket anak"
                    tvDetailBayar.text = "( ${jumlahBookDewasa}x${toCurrency(tiketPrice.toInt())} + ${jumlahBookAnak}x${toCurrency(tiketPriceChil.toInt())} )"
                }
                tvTotalBayar.text = "Rp ${toCurrency(totalBayar)}"

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                jumlahBookAnak = s.toString()
                if (jumlahBookDewasa == "")
                {
                    jumlahBookDewasa="0"
                }
                if (jumlahBookAnak == "")
                {
                    jumlahBookAnak="0"
                }
                if (tiketPrice.isEmpty()){
                    totalBayar = jumlahBookAnak.toInt()*tiketPriceChil.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookAnak} tiket anak"
                    tvDetailBayar.text = "( ${jumlahBookAnak}x${toCurrency(tiketPriceChil.toInt())} )"

                }else if (tiketPriceChil.isEmpty()){
                    totalBayar = jumlahBookDewasa.toInt()*tiketPrice.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookDewasa} tiket dewasa"
                    tvDetailBayar.text = "( ${jumlahBookDewasa}x${toCurrency(tiketPrice.toInt())})"
                }else{
                    totalBayar = jumlahBookDewasa.toInt()*tiketPrice.toInt() + jumlahBookAnak.toInt()*tiketPriceChil.toInt()
                    tvTextTotal.text = "Harga total untuk ${jumlahBookDewasa} tiket dewasa dan ${jumlahBookAnak} tiket anak"
                    tvDetailBayar.text = "( ${jumlahBookDewasa}x${toCurrency(tiketPrice.toInt())} + ${jumlahBookAnak}x${toCurrency(tiketPriceChil.toInt())} )"
                }
                tvTotalBayar.text = "Rp ${toCurrency(totalBayar)}"
            }
        })

        btnPesan.setOnClickListener {
            if (jumlahBookDewasa==null || jumlahBookDewasa=="0"){
                AlertDialog.Builder(this ?: return@setOnClickListener)
                    .setMessage("Jumlah pesanan kosong")
                    .setPositiveButton("Ok") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                    }
                    .setNegativeButton("") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                    }
                    .show()
            }else{
                orderPresenter.insertTiketToOrder(tiketId,tiketName.toString(),tiketRoute,tiketAgent,"",tglBerangkat,"",tiketDetail,durasiTour,"",
                    "Menunggu Pembayaran","","","","","","",tiketPrice,tiketPriceChil,jumlahBookDewasa,jumlahBookAnak,totalBayar.toString(),sp.userSession.id)
            }



        }




    }
    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
//        progress.visibility = View.GONE
    }

    override fun onTransaksiSuccess(orderIdBase:String?) {
        startActivity(intentFor<MetodePembayaran>().apply {
            putExtra("orderId",orderIdBase)
            putExtra("tiketId",tiketId)
            putExtra("tiketAgent",tiketAgent)
            putExtra("tiketName",tiketName)
            putExtra("tiketPrice",tiketPrice)
            putExtra("tiketRoute",tiketRoute)
            putExtra("tiketKeberangkatan",tiketKberangkatan)
            putExtra("tiketDetail",tiketDetail)
            putExtra("tiketAvailableBook",tiketAvailableBook)
            putExtra("durasi",durasiTour)
            putExtra("tiketPriceChil",tiketPriceChil)
            putExtra("totalBayar",totalBayar.toString())
            putExtra("jumlahBookDewasa",jumlahBookDewasa)
            putExtra("jumlahBookAnak",jumlahBookAnak)
            putExtra("tanggalBerangkat",tglBerangkat)
        })

    }



}
