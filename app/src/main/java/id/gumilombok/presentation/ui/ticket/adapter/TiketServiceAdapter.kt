package id.gumilombok.presentation.ui.ticket.adapter

import android.content.Context
import id.gumilombok.common.OnItemClick
import id.gumilombok.data.remote.model.ServiceModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_point_location.view.*
import id.gumilombok.R
import kotlinx.android.synthetic.main.item_service.view.*

class TiketServiceAdapter(private val onItemClick: OnItemClick<ServiceModel>) : BaseListAdapter<ServiceModel>() {
    override fun getItemView(context: Context): BaseViewHolder<ServiceModel> = Holder(context)

    inner class Holder(context: Context?) : BaseViewHolder<ServiceModel>(context){
        override fun layoutResId(): Int = R.layout.item_service

        override fun bind(item: ServiceModel, position: Int) {
            tvService.text = item.serviceName
            setOnClickListener { onItemClick.onItemClick(item, position) }
//            btnTukarPoint.setOnClickListener { onItemClick.onItemClick(item,position) }
        }

    }

}