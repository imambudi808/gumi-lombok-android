package id.gumilombok.presentation.ui.event.adapter

import android.content.Context
import android.util.Log
import android.widget.Filter
import android.widget.Filterable
import com.bumptech.glide.Glide
import com.google.gson.Gson
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.data.remote.model.EventModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_event_activity.view.*


class EventAdapterActivity(private val onItemClick: OnItemClick<EventModel>,
                   private val searchResultCallback: SearchResultCallback):
    BaseListAdapter<EventModel>(), Filterable {
    private val events=items
    interface SearchResultCallback {
        fun onDataNotFound(s: String)
    }

    inner class ViewHolder(context: Context?): BaseViewHolder<EventModel>(context){
        override fun layoutResId(): Int = R.layout.item_event_activity

        override fun bind(item: EventModel, position: Int) {
//            tv_event_name.text=item.eventName?.toUpperCase()
//            tv_event_date.text="Date : ${item.eventDate}"
//            tv_biaya.text=item.eventCost?.toUpperCase()
//            tv_lokasi.text="Lokasi :${item.eventLocation}"
//            tvCreatedAt.text=item.createdAt

            Glide.with(this).load(item.eventImage).into(img_event)
            setOnClickListener{onItemClick.onItemClick(item,position)}
        }

    }

    override fun getItemView(context: Context): BaseViewHolder<EventModel> = ViewHolder(context)

    override fun getFilter(): Filter {
        return object: Filter(){
            override fun performFiltering(text: CharSequence?): FilterResults {
                val search = text?.toString() ?: " "
                val itemList:List<EventModel> = if (search.isEmpty()){
                    events
                }else{
                    events.filter {
                        it.eventName?.toLowerCase()?.contains(search) == true
                    }

                }
                val results = FilterResults()
                results.values = itemList
                Log.d("cek filter tiket","${Gson().toJsonTree(itemList)}")
                return results
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(s: CharSequence?, results: FilterResults?) {
                items = results?.values as ArrayList<EventModel>
                if (items.isEmpty()){
                    searchResultCallback.onDataNotFound(s?.toString()?:" ")
                }
                notifyDataSetChanged()
                Log.d("cek filter publist","${Gson().toJsonTree(items)}")
            }

        }
    }
}