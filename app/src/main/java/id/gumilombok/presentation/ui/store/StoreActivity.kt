package id.gumilombok.presentation.ui.store

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.intentFor
import id.gumilombok.data.remote.model.StoreModel
import id.gumilombok.data.remote.model.StoreProdukModel
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.store.adapter.ProdukAdapter
import id.gumilombok.presentation.ui.store.adapter.StoreAdapter
import id.gumilombok.presentation.ui.store.presenter.StorePresenter
import id.gumilombok.presentation.ui.store.presenter.StoreProdukPresenter
import id.gumilombok.presentation.ui.store.view.StoreView
import kotlinx.android.synthetic.main.activity_store.*
import kotlinx.android.synthetic.main.toolbar.view.*
import javax.inject.Inject

class StoreActivity : BaseActivity(),StoreView {

    @Inject lateinit var storePresenter:StorePresenter
    lateinit var storeAdapter:StoreAdapter

    @Inject lateinit var produkPresenter:StoreProdukPresenter
    lateinit var produkAdapter:ProdukAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store)
        setSupportActionBar(layToolbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        storePresenter.bind(this,isOnline)
        produkPresenter.bind(this,isOnline)
        title="Gumi Market"
        setupView()
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(name, context, attrs)
        sm_store.startShimmer()
    }

    fun setupView(){
        showNewStore()
        showNewProduk()
    }
    fun showNewStore(){
        storeAdapter= StoreAdapter(object :OnItemClick<StoreModel>{
            override fun onItemClick(item:StoreModel?, position:Int?){
                startActivity(intentFor<StoreDetailActivity>().apply {
                    putExtra("storeId",item?.storeId)
                    putExtra("storeName",item?.storeName)
                    putExtra("storeDes",item?.storeDes)
                    putExtra("storeBanner",item?.storeBanner)
                    putExtra("storeProfile",item?.storeImageProfile)
                    putExtra("userId",item?.userId)
                    putExtra("seoLink",item?.seoLink)
                    putExtra("storePhoneNumber",item?.storePhoneNumber)
                })
            }
        })

        val layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        rv_toko_berbintang.layoutManager=layoutManager
        rv_toko_berbintang.adapter=storeAdapter
        storePresenter.showNewStore()
    }
    fun showNewProduk(){
        produkAdapter= ProdukAdapter(object :OnItemClick<StoreProdukModel>{
            override fun onItemClick(item:StoreProdukModel?, position:Int?){
                startActivity(intentFor<ProdukDetailActivity>().apply {
                    putExtra("storeId",item?.storeId)
                    putExtra("produkId",item?.produkId)
                    putExtra("produkName",item?.produkName)
                    putExtra("produkBanner",item?.produkBanner)
                    putExtra("produkDes",item?.produkDes)
                    putExtra("seoLink",item?.seoLink)
                    putExtra("storePhoneNumber",item?.storePhoneNumber)
                    putExtra("produkHarga",item?.hargaAwal)
                })
            }
        })
        val layoutManager=GridLayoutManager(this,2)
        rv_produk_baru.layoutManager=layoutManager
        rv_produk_baru.adapter=produkAdapter
        produkPresenter.showNewProduk()
    }

    override fun onLoadNewStore(stores:List<StoreModel>?){
        if (storeAdapter.isEmpty()){
            storeAdapter.addItems(stores)
        }
    }
    override fun onLoadNewProduk(produks:List<StoreProdukModel>?){
//        if (produkAdapter.isEmpty()){
//            produkAdapter.addItems(produks)
//        }
        if (produks!=null){
            sm_store.startShimmer()
            sm_store.visibility=View.GONE
            produkAdapter.addItems(produks)
        }
    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun showMessage(message: String?) {
        TODO("Not yet implemented")
    }

    override fun showProgress(show: Boolean) {


    }
}
