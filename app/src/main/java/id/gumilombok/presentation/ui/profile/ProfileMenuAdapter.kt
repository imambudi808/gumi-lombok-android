package id.gumilombok.presentation.ui.profile

import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import id.gumilombok.R
import id.gumilombok.common.loadImage
import kotlinx.android.synthetic.main.item_profile_menu.view.*

class ProfileMenuAdapter(private val item: Pair<Int, String>,
                         private val onClick: () -> Unit) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.run {
            imgIcon.loadImage(item.first)
            tvMenuName.text = item.second

            setOnClickListener {
                onClick()
            }
        }
    }

    override fun getLayout(): Int = R.layout.item_profile_menu
}