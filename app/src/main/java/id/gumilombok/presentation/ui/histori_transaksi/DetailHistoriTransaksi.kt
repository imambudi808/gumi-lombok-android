package id.gumilombok.presentation.ui.histori_transaksi

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import id.gumilombok.R
import id.gumilombok.common.intentFor
import id.gumilombok.common.startActivity
import id.gumilombok.common.toCurrency
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.CustomAlert
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.home.MainActivity
import id.gumilombok.presentation.ui.ticket.MetodePembayaran
import id.gumilombok.presentation.ui.ticket.PilihBankActivity
import id.gumilombok.presentation.ui.ticket.presenter.OrderPresenter
import id.gumilombok.presentation.ui.ticket.view.OrderView
import kotlinx.android.synthetic.main.activity_detail_histori_transaksi.*
import kotlinx.android.synthetic.main.bank_bayar.*
import kotlinx.android.synthetic.main.detail_penanan_tiket.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class DetailHistoriTransaksi : BaseActivity(),OrderView {
    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
//        progress.visibility = View.GONE
    }

    @Inject
    lateinit var sp: PreferencesManager
    @Inject
    lateinit var orderPresenter: OrderPresenter

    private var tiketId:String=""
    private var orderCode:String=""
    private var tiketAgent:String=""
    private var tiketName:String=""
    private var tiketPrice:String=""
    private var tiketRoute:String=""
    private var tiketKberangkatan:String=""
    private var tiketDetail:String=""

    private var durasiTour:String=""
    private var tiketPriceChil:String=""
    private var jumlahBookAnak:String=""
    private var jumlahBookDewasa:String=""
    private var totalBayar:String=""
    private var orderId:String=""
    private var bankName:String=""
    private var norek:String=""
    private var cardHolder:String=""
    private var metodeBayar:String=""
    private var orderStatus:String=""
    private lateinit var customAlert: CustomAlert

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_histori_transaksi)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Detail Pesanan"
        orderPresenter.bind(this,isOnline)

        orderId=intent.getStringExtra("orderId")
        orderCode=intent.getStringExtra("orderCode")
        tiketId=intent.getStringExtra("tiketId")
        tiketId=intent.getStringExtra("tiketId")
        tiketAgent=intent.getStringExtra("tiketAgent")
        tiketName=intent.getStringExtra("tiketName")
        tiketPrice=intent.getStringExtra("tiketPrice")
        tiketRoute=intent.getStringExtra("tiketRoute")
        tiketKberangkatan=intent.getStringExtra("tiketKeberangkatan")
        tiketDetail=intent.getStringExtra("tiketDetail")

        durasiTour=intent.getStringExtra("durasi")
        tiketPriceChil=intent.getStringExtra("tiketPriceChil")
        totalBayar=intent.getStringExtra("totalBayar")
        jumlahBookAnak=intent.getStringExtra("jumlahBookAnak")
        jumlahBookDewasa = intent.getStringExtra("jumlahBookDewasa")
        bankName=intent.getStringExtra("bankName")
        metodeBayar=intent.getStringExtra("metodeBayar")

        norek=intent.getStringExtra("norek")
        cardHolder=intent.getStringExtra("cardHolder")
        orderStatus=intent.getStringExtra("orderStatus")

        if (jumlahBookAnak.isNotEmpty() && jumlahBookAnak != "0"){
            tvTotalTiketAnak.text = "${jumlahBookAnak} x ${toCurrency(tiketPriceChil.toInt())}"
        }else{

            textView18.visibility = View.GONE
            tvTotalTiketAnak.visibility = View.GONE
        }
        if (jumlahBookDewasa.isNotEmpty() && jumlahBookDewasa != "0"){
            tvTotalTiketDewasa.text = "${jumlahBookDewasa} x ${toCurrency(tiketPrice.toInt())}"
        }else{
            textView22.visibility = View.GONE
            tvTotalTiketDewasa.visibility = View.GONE
        }
        tvTotal.text = "Rp. ${toCurrency(totalBayar.toInt())}"
        tvTiketAgent.text = tiketAgent
        tvNamaTiket.text = tiketName
        tvInvoice.text = "INVOICE : ${orderId}"
        tvTotalBayar.text = "Total : ${toCurrency(totalBayar.toInt())}"
        tvNamaBank.text = bankName
        tvNorek.text = norek
        tvMetodeBayar.text = metodeBayar
        tvStatus.text = orderStatus


        if (orderStatus == "Menunggu Pembayaran"){
            btnKonfirmasiBayar.setText("Sudah Bayar")
            btnKonfirmasiBayar.setOnClickListener {
                onKonfirmasiSudahBayar()
            }
        }else if (orderStatus=="Menunggu Konfirmasi"){
            btnKonfirmasiBayar.setText("Close")
            btnUbahBayar.visibility = View.GONE
            btnKonfirmasiBayar.setOnClickListener{

            }

        }else if (orderStatus=="Berhasil"){
            btnKonfirmasiBayar.setText("Cetak Tiket")
            btnUbahBayar.visibility = View.GONE
            btnKonfirmasiBayar.setOnClickListener{
                startActivity(intentFor<CetakTiketActivity>().apply {
                    putExtra("orderId",orderId)
                    putExtra("orderCode",orderCode)
                    putExtra("tiketId",tiketId)
                    putExtra("tiketAgent",tiketAgent)
                    putExtra("tiketName",tiketName)
                    putExtra("tiketPrice",tiketPrice)
                    putExtra("tiketRoute",tiketRoute)
                    putExtra("tiketKeberangkatan",tiketKberangkatan)
                    putExtra("tiketDetail",tiketDetail)

                    putExtra("durasi",durasiTour)
                    putExtra("tiketPriceChil",tiketPriceChil)
                    putExtra("totalBayar",totalBayar.toString())
                    putExtra("jumlahBookDewasa",jumlahBookDewasa)
                    putExtra("jumlahBookAnak",jumlahBookAnak)
                })

            }
        }else if (orderStatus=="Gagal"){
            btnKonfirmasiBayar.setText("Close")
            btnKonfirmasiBayar.setOnClickListener{

            }
        }

        btnUbahBayar.setOnClickListener {
            startActivity(intentFor<MetodePembayaran>().apply {
                putExtra("orderId",orderId)
                putExtra("orderCode",orderCode)
                putExtra("tiketId",tiketId)
                putExtra("tiketAgent",tiketAgent)
                putExtra("tiketName",tiketName)
                putExtra("tiketPrice",tiketPrice)
                putExtra("tiketRoute",tiketRoute)
                putExtra("tiketKeberangkatan",tiketKberangkatan)
                putExtra("tiketDetail",tiketDetail)

                putExtra("durasi",durasiTour)
                putExtra("tiketPriceChil",tiketPriceChil)
                putExtra("totalBayar",totalBayar.toString())
                putExtra("jumlahBookDewasa",jumlahBookDewasa)
                putExtra("jumlahBookAnak",jumlahBookAnak)
            })
        }
    }

    private fun onKonfirmasiSudahBayar(){
        val v = LayoutInflater.from(this).inflate(R.layout.alert_konfirmasi,
            ConstraintLayout(this)
        )
        val code = v.findViewById<TextView>(R.id.edt_Kode)
        customAlert = CustomAlert(this)
        customAlert.setTitle("Konfirmasi Pembayaran")
        customAlert.setContent(v)
        customAlert.setPositiveButton("Simpan",object :CustomAlert.AlertClickListener{
            override fun onClick() {
                val insertCode = code.text.toString()
                if (insertCode.isEmpty()){
                    showMessage("Anda belum memasukkan kode point")
                }else{
                    orderPresenter.konfirmasiSudahBayar(orderId.toString(),"Menunggu Konfirmasi",insertCode)
                    customAlert.dismiss()
                    showMessage("Konfirmasi Berhasil")
                    startActivity<HistoriActivity>()

                }
            }
        })

        customAlert.setNegativeButton("Batal",object :CustomAlert.AlertClickListener{
            override fun onClick() {
                customAlert.dismiss()
            }
        })
        customAlert.show()
    }
}
