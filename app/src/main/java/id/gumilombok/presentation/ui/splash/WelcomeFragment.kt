package id.gumilombok.presentation.ui.splash

import android.os.Bundle
import android.provider.Contacts.SettingsColumns.KEY
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.gumilombok.R

class WelcomeFragment:Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val argKey=arguments?.getInt(KEY)
        var resLayout = 0
        when(argKey){
            0->{resLayout = R.layout.fragment_welcome_1}
            1->{resLayout = R.layout.fragment_wlcome2}

//            bisa ditambahkan layout selanjutnya
        }
        return inflater.inflate(resLayout,container,false)
    }

    companion object{
        val KEY = "position"
        fun newInstance(position:Int):WelcomeFragment{
            val args = Bundle()
            args.putInt(KEY,position)
            val fragment=WelcomeFragment()
            fragment.arguments = args
            return fragment
        }
    }
}