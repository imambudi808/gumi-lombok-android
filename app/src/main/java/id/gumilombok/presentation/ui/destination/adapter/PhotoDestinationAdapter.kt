package id.gumilombok.presentation.ui.destination.adapter

import android.content.Context
import com.squareup.picasso.Picasso
import id.gumilombok.common.OnItemClick
import id.gumilombok.data.remote.model.PhotoModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import id.gumilombok.R
import kotlinx.android.synthetic.main.item_photo_des.view.*

class PhotoDestinationAdapter(private val onItemClick:OnItemClick<PhotoModel>) : BaseListAdapter<PhotoModel>() {

    override fun getItemView(context: Context): BaseViewHolder<PhotoModel> = Holder(context)

    inner class Holder(context: Context?) : BaseViewHolder<PhotoModel>(context){
        override fun layoutResId(): Int = R.layout.item_photo_des

        override fun bind(item: PhotoModel, position: Int) {

            Picasso.get().load(item.photoFile).into(ivDesPhoto)

            setOnClickListener { onItemClick.onItemClick(item, position) }

        }

    }

}