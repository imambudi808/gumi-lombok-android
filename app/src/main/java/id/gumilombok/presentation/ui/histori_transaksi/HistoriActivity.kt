package id.gumilombok.presentation.ui.histori_transaksi


import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.intentFor
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.HistoriModel
import id.gumilombok.data.remote.model.OrderModel

import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.EndlessRecyclerViewScrollListener
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.model.Order
import id.gumilombok.presentation.ui.histori_transaksi.Adapter.OrderAdapter

//import id.gumilombok.presentation.ui.histori_transaksi.Adapter.OrderAdapter
import id.gumilombok.presentation.ui.home.MainActivity
import id.gumilombok.presentation.ui.ticket.presenter.OrderPresenter
import id.gumilombok.presentation.ui.ticket.view.OrderView

import kotlinx.android.synthetic.main.activity_histori.*
import kotlinx.android.synthetic.main.activity_histori.progress
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class HistoriActivity : BaseActivity(),OrderView {

    @Inject lateinit var presenter:OrderPresenter
    @Inject lateinit var sp:PreferencesManager
    private var query:String?=""
    lateinit var adapter: OrderAdapter

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress?.visibility = if (show) View.VISIBLE else View.GONE
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_histori)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter.bind(this, isOnline)
        title = "Histori Transaksi"

        adapter = OrderAdapter(object : OnItemClick<HistoriModel> {
            override fun onItemClick(item: HistoriModel?, position: Int?) {
                startActivity(intentFor<DetailHistoriTransaksi>().apply {
                    putExtra("orderId",item?.orderId)
                    putExtra("orderCode",item?.orderCode)
                    putExtra("tiketId",item?.tiketId)
                    putExtra("tiketId",item?.orderCode)
                    putExtra("tiketAgent",item?.tiketAgent)
                    putExtra("tiketName",item?.tiketName)
                    putExtra("tiketPrice",item?.tiketPriceAdult)
                    putExtra("tiketRoute",item?.tiketRute)
                    putExtra("tiketKeberangkatan",item?.tiketKeberangkatan)
                    putExtra("tiketDetail",item?.tiketDetail)

                    putExtra("durasi",item?.durasiTiket)
                    putExtra("tiketPriceChil",item?.tiketPriceChil)
                    putExtra("totalBayar",item?.totalBayar)
                    putExtra("jumlahBookDewasa",item?.jumlahAdult)
                    putExtra("jumlahBookAnak",item?.jumlahAnak)
                    putExtra("metodeBayar",item?.metodeBayar)
                    putExtra("bankName",item?.bankTujuan)
                    putExtra("norek",item?.norekening)
                    putExtra("cardHolder",item?.cardHolder)
                    putExtra("orderStatus",item?.orderStatus)
                })
            }
        }, object : OrderAdapter.SearchResultCallback {
            override fun onDataNotFound(s: String) {
                presenter.showListOrder(sp.userSession.id,0, 15, false, s)
            }
        })
        val layoutManager = LinearLayoutManager(this)
        rvHistori.layoutManager = layoutManager
        rvHistori.adapter = adapter
        rvHistori.invalidate()
        val scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.showListOrder(sp.userSession.id,totalItemsCount, totalItemsCount.plus(15), false, query)
            }
        }
        rvHistori.addOnScrollListener(scrollListener)
        presenter.showListOrder(sp.userSession.id,0,15,false)
    }



    override fun onOrderLoaded(orders: List<HistoriModel>?, clearItem: Boolean) {

        if (clearItem) adapter.clearItems()
        adapter.addItems(orders)
    }
}
