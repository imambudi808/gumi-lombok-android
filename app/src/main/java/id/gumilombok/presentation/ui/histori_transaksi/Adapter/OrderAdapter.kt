package id.gumilombok.presentation.ui.histori_transaksi.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import id.gumilombok.common.OnItemClick

import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import id.gumilombok.R
import id.gumilombok.data.remote.model.HistoriModel
import kotlinx.android.synthetic.main.item_history_order.view.*


class OrderAdapter(private val itemClickListener: OnItemClick<HistoriModel>,
                   private val searchResultCallback:SearchResultCallback):
    BaseListAdapter<HistoriModel>(), Filterable {

    private val orders = items
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(text: CharSequence?): FilterResults {
                val search = text?.toString() ?: ""
                val itemList: List<HistoriModel> = if (search.isEmpty()) {
                    orders
                } else {
                    orders.filter {
                        it.orderId?.contains(search) == true || it.orderStatus?.toLowerCase()?.contains(search) == true || it.tiketAgent?.toLowerCase()?.contains(search) == true
                    }
                }
                val result = Filter.FilterResults()
                result.values = itemList
                Log.d("cek filter", "${Gson().toJsonTree(itemList)}")
                return result
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(s: CharSequence?, results: FilterResults?) {
                items = results?.values as ArrayList<HistoriModel>
                if (items.isEmpty()) {
                    searchResultCallback.onDataNotFound(s?.toString() ?: "")
                }
                notifyDataSetChanged()
                Log.d("cek filter publish", "${Gson().toJsonTree(items)}")
            }
        }
    }

    override fun getItemView(context: Context): BaseViewHolder<HistoriModel> = ViewHolder(context)


    inner class ViewHolder(context: Context?) : BaseViewHolder<HistoriModel>(context) {
        override fun layoutResId(): Int = R.layout.item_history_order

        @SuppressLint("SetTextI18n")
        override fun bind(item: HistoriModel, position: Int) {

            if (item.orderStatus=="Menunggu Pembayaran"){
                tvStatus.background = ContextCompat.getDrawable(context,R.drawable.bg_button_red)
            }else if (item.orderStatus=="Menunggu Konfirmasi"){
                tvStatus.background=ContextCompat.getDrawable(context,R.drawable.bg_kunig_radius4)
            }else if (item.orderStatus=="Berhasil"){
                tvStatus.background=ContextCompat.getDrawable(context,R.drawable.bg_blue_radius_4)
            }

            tvOrderId.text="INVOICE : ${item.orderId}"
            tvNamaTour.text=item.tiketAgent
            tvCreatedAt.text=item.createdAt
            tvStatus.text=item.orderStatus?.toUpperCase()


            setOnClickListener { itemClickListener.onItemClick(item, position) }

        }

    }

    interface SearchResultCallback {
        fun onDataNotFound(s: String)
    }
}

