package id.gumilombok.presentation.ui.home.adapter

import android.content.Context
import com.squareup.picasso.Picasso
import id.gumilombok.common.OnItemClick
import id.gumilombok.data.remote.model.DestinationModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder

import id.gumilombok.R
import id.gumilombok.common.UrlHelper
import kotlinx.android.synthetic.main.item_home_destination.view.*

class NewDesAdapter(private val onItemClick: OnItemClick<DestinationModel>) : BaseListAdapter<DestinationModel>() {
    override fun getItemView(context: Context): BaseViewHolder<DestinationModel> = Holder(context)
    private val url = UrlHelper()

    inner class Holder(context: Context?) : BaseViewHolder<DestinationModel>(context){
        override fun layoutResId(): Int = R.layout.item_home_destination

        override fun bind(item: DestinationModel, position: Int) {
            tvDesName.text=item.destinationName.toString()
            ratingBar.rating=item.rataRating!!.toFloat()

            Picasso.get().load(item.destinationImage).into(imgDestination)
            setOnClickListener { onItemClick.onItemClick(item, position) }
//            btnTukarPoint.setOnClickListener { onItemClick.onItemClick(item,position) }
        }

    }

}