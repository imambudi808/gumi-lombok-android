package id.gumilombok.presentation.ui.rating.adapter

import android.content.Context
import androidx.core.content.ContextCompat

import id.gumilombok.common.OnItemClick

import id.gumilombok.data.remote.model.RatingModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder

import id.gumilombok.R


import kotlinx.android.synthetic.main.item_rating.view.*



class RatingAdapter(private val onItemClick: OnItemClick<RatingModel>) : BaseListAdapter<RatingModel>() {

    override fun getItemView(context: Context): BaseViewHolder<RatingModel> = Holder(context)

    inner class Holder(context: Context?) : BaseViewHolder<RatingModel>(context){
        override fun layoutResId(): Int = R.layout.item_rating

        override fun bind(item: RatingModel, position: Int) {

//            Picasso.get().load(item.photoFile).into(ivDesPhoto)
            rating_bar.rating=item.ratingValue!!.toFloat()
            /*if(item.ratingValue?.toDouble()==0.5){
                bintang1.background= ContextCompat.getDrawable(context,R.drawable.ic_star_half_black_24dp)
            }
            if(item.ratingValue?.toDouble()==1.0){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
            }
            if(item.ratingValue?.toDouble()==1.5){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_half_black_24dp)
            }
            if(item.ratingValue?.toDouble()==2.0){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
            }
            if(item.ratingValue?.toDouble()==2.5){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_half_black_24dp)
            }
            if(item.ratingValue?.toDouble()==3.0){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
            }
            if(item.ratingValue?.toDouble()==3.5){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_half_black_24dp)
            }
            if(item.ratingValue?.toDouble()==4.0){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
            }
            if(item.ratingValue?.toDouble()==4.5){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_half_black_24dp)
            }
            if(item.ratingValue?.toDouble()==5.0){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
            }*/

            tvUserName.text=item.name.toString()
            tvComment.text=item.ratingComment.toString()
            tvCreatedAt.text=item.createdAt




            setOnClickListener { onItemClick.onItemClick(item, position) }

        }

    }

}