package id.gumilombok.presentation.ui.ticket


import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import id.gumilombok.R
import id.gumilombok.common.intentFor
import id.gumilombok.common.startActivity
import id.gumilombok.common.toCurrency
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.CustomAlert
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.home.MainActivity
import id.gumilombok.presentation.ui.ticket.presenter.OrderPresenter
import id.gumilombok.presentation.ui.ticket.view.OrderView
import kotlinx.android.synthetic.main.activity_konfirmasi_bank_transfer.*
import kotlinx.android.synthetic.main.detail_bank_transfer.*
import kotlinx.android.synthetic.main.detail_penanan_tiket.*
import kotlinx.android.synthetic.main.detail_penanan_tiket.tvTotal
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.singleTop
import javax.inject.Inject

class KonfirmasiBankTransfer : BaseActivity(),OrderView {
    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    @Inject
    lateinit var sp: PreferencesManager
    @Inject
    lateinit var orderPresenter: OrderPresenter

    private lateinit var customAlert: CustomAlert

    var orderId:String?=""
    private var tiketId:String=""
    private var tiketAgent:String=""
    private var tiketName:String=""
    private var tiketPrice:String=""
    private var tiketRoute:String=""
    private var tiketKberangkatan:String=""
    private var tiketDetail:String=""
    private var tiketAvailableBook:String=""
    private var durasiTour:String=""
    private var tiketPriceChil:String=""
    private var jumlahBookAnak:String=""
    private var jumlahBookDewasa:String=""
    private var totalBayar:String=""
    private var orderIdBase:String=""
    private var norek:String=""
    private var norekName:String=""
    private var bankName:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_konfirmasi_bank_transfer)
        setSupportActionBar(toolbar)
        orderPresenter.bind(this,isOnline)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Konfirmasi Pembayaran"

        orderId=intent.getStringExtra("orderId")
        tiketId=intent.getStringExtra("tiketId")
        tiketAgent=intent.getStringExtra("tiketAgent")
        tiketName=intent.getStringExtra("tiketName")
        tiketPrice=intent.getStringExtra("tiketPrice")
        tiketRoute=intent.getStringExtra("tiketRoute")
        tiketKberangkatan=intent.getStringExtra("tiketKeberangkatan")
        tiketDetail=intent.getStringExtra("tiketDetail")
//        tiketAvailableBook=intent.getStringExtra("tiketAvailableBook")
        durasiTour=intent.getStringExtra("durasi")
        tiketPriceChil=intent.getStringExtra("tiketPriceChil")
        totalBayar=intent.getStringExtra("totalBayar")
        jumlahBookAnak=intent.getStringExtra("jumlahBookAnak")
        jumlahBookDewasa = intent.getStringExtra("jumlahBookDewasa")
        orderIdBase=intent.getStringExtra("orderId")
        norek=intent.getStringExtra("norek")
        norekName=intent.getStringExtra("norekName")
        bankName=intent.getStringExtra("bankName")

        tvInvoice.text = "Invoice : ${orderId.toString()}"
        tvTotalBayar.text = "Total : Rp ${toCurrency(totalBayar.toInt())}"
        showDetail.visibility = View.GONE




        if (jumlahBookAnak.isNotEmpty() && jumlahBookAnak != "0"){
            tvTotalTiketAnak.text = "${jumlahBookAnak} x ${toCurrency(tiketPriceChil.toInt())}"
        }else{

            textView18.visibility = View.GONE
            tvTotalTiketAnak.visibility = View.GONE
        }
        if (jumlahBookDewasa.isNotEmpty() && jumlahBookDewasa != "0"){
            tvTotalTiketDewasa.text = "${jumlahBookDewasa} x ${toCurrency(tiketPrice.toInt())}"
        }else{
            textView22.visibility = View.GONE
            tvTotalTiketDewasa.visibility = View.GONE
        }
        tvTotal.text = "Rp ${toCurrency(totalBayar.toInt())}"
        tvTiketAgent.text = tiketAgent
        tvNamaTiket.text = tiketName

        tvNorek.text = norek
        tvPemilikRekening.text = norekName
        tvNamBank.text = bankName
        tvTotalTransfer.text = "Rp ${toCurrency(totalBayar.toInt())}"

        orderPresenter.updateMetodeBayar(orderId.toString(),"transfer bank",bankName,norek,norekName)
        btnSudahBayar.setOnClickListener {
            onKonfirmasiSudahBayar()
        }
    }

    private fun onKonfirmasiSudahBayar(){
        val v = LayoutInflater.from(this).inflate(R.layout.alert_konfirmasi,
            ConstraintLayout(this)
        )
        val code = v.findViewById<TextView>(R.id.edt_Kode)
        customAlert = CustomAlert(this)
        customAlert.setTitle("Konfirmasi Pembayaran")
        customAlert.setContent(v)
        customAlert.setPositiveButton("Simpan",object :CustomAlert.AlertClickListener{
            override fun onClick() {
                val insertCode = code.text.toString()
                if (insertCode.isEmpty()){
                    showMessage("Anda belum memasukkan kode point")
                }else{
                    orderPresenter.konfirmasiSudahBayar(orderId.toString(),"Menunggu Konfirmasi",insertCode)
                    customAlert.dismiss()
                    showMessage("Konfirmasi Berhasil")
                    startActivity(intentFor<MainActivity>().singleTop().clearTop().clearTask())


                }
            }
        })

        customAlert.setNegativeButton("Batal",object :CustomAlert.AlertClickListener{
            override fun onClick() {
                customAlert.dismiss()
            }
        })
        customAlert.show()
    }

}
