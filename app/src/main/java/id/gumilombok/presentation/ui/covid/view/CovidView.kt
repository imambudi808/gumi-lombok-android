package id.gumilombok.presentation.ui.covid.view

import id.gumilombok.data.remote.model.CovidModel
import id.gumilombok.presentation.base.view.BaseView

interface CovidView:BaseView {
    fun onCovidLoad(covid:List<CovidModel>?){}
}