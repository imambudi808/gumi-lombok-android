package id.gumilombok.presentation.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.gumilombok.R.layout.fragment_notif
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.di.module.SyncServiceModule
import id.gumilombok.Gumi
import id.gumilombok.presentation.base.BaseFragment

class NotifFragment: BaseFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(fragment_notif, container, false)
    }

    override fun injectModule(activityComponent: SyncComponent) {
        activityComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}