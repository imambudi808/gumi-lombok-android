package id.gumilombok.presentation.ui.rating.presenter

import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.repository.RatingRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.rating.view.RatingView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RatingPresenter @Inject constructor(val repo:RatingRepository,
                                          val sp:PreferencesManager): BasePresenter<RatingView>() {
    private val tag = this::class.java.simpleName
    var statusCekRating = false
    var pointValue = 0

    fun loadRatingByDes(destId:String?){
        view?.showProgress(true)
        disposables.add(
            repo.loadRatings(destId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(tag,"load rating :"+Gson().toJsonTree(it))
                    view?.onloadRating(it)
                },{ doOnError(view,it)})
        )
    }

    fun insertDataToRating(ratingValue:String?,ratingComent:String?,destId:String?,userId:String?){
        view?.showProgress(true)
        disposables.add(
            repo.insertRating(ratingValue,ratingComent,destId,userId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(tag,"load rating :"+Gson().toJsonTree(it))
                    view?.onloadRating(it)
                },{ doOnError(view,it)})
        )
    }

    fun updateRatingRata(destId:String?){
        view?.showProgress(true)
        disposables.add(
            repo.updateRating(destId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(tag,"load rating :"+Gson().toJsonTree(it))
                    view?.onloadRating(it)
                },{ doOnError(view,it)})
        )

    }

    fun cekRating(userId: String,destId: String?){
//        statusCekRating= it.
        disposables.add(
            repo.cekRating(userId,destId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    //                    view?.showProgress(false)
                }
                .subscribe({
                    view?.cekRatingV(it.status.toString(),it.ratingValue,it.ratingComent)

                    Log.i(tag,"berhasil cek point 1"+Gson().toJsonTree(it))
//                    view?.onRegistrasionSuccses()
                    if(it.status!!){
                        Log.i(tag,"berhasil cek point"+Gson().toJsonTree(it.status))
                        var tam = it.status

                        var t2 = it.ratingValue
                        statusCekRating=it.status
                        pointValue=it.ratingValue!!.toInt()
//                        sp.cekLastPoint = it.ratingValue.toString()
//                        Log.i(tag,"cek last point:" + sp.cekLastPoint)

                    }else{
//                        view?.showMessage("update password gagal "+it.pesan)
                    }
                },{ doOnError(view,it)})
        )
//        statusCekRating=tam
    }

    fun updateRatingUser(userId: String,destId: String,comment:String?){
        view?.showProgress(true)
        disposables.add(
            repo.updateRatingUser(userId,destId,comment)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    Log.i(tag,"update rating :"+Gson().toJsonTree(it))

                },{
//                    doOnError(view,it)
                })
        )

    }
}