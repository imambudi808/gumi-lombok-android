package id.gumilombok.presentation.ui.store.presenter


import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.repository.StoreProdukRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.store.view.StoreView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StoreProdukPresenter @Inject constructor(val repo:StoreProdukRepository,val sp:PreferencesManager):
    BasePresenter<StoreView>() {
    private val tag=this::class.java.simpleName

    //new produk
    fun showNewProduk(){
        view?.showLoading(true)
        disposables.add(
            repo.loadNewProduk().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showLoading(false)
                }
                .subscribe({
                    view?.onLoadNewProduk(it)
                },{ doOnError(view,it)})
        )
    }

    //load produk by store id
    fun showProdukByStoreId(storeId:Int,offset:Int,limit:Int){
        view?.showLoading(true)
        val map=HashMap<String,String>()
        map["store_id"] = storeId.toString()
        map["offset"] = offset.toString()
        map["limit"] = limit.toString()
        disposables.add(
            repo.loadProdukByStoreId(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showLoading(false)
                }
                .subscribe({
                    view?.onLoadProdukByStoreId(it)
                },{
                    doOnError(view,it)
                })
        )
    }
}