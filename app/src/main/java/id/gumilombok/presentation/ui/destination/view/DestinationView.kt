package id.gumilombok.presentation.ui.destination.view

import id.gumilombok.data.remote.model.DestinationModel
import id.gumilombok.data.remote.model.PhotoModel
import id.gumilombok.presentation.base.view.BaseView

interface DestinationView : BaseView {
    fun showLoadMoreProgress(show: Boolean) {}
    fun onDestinationLoaded(transaksis: List<DestinationModel>?, clearItem: Boolean) {}
    fun onDesPhotoLoaded(photos:List<PhotoModel>?){}
    fun newDesLoad(dest:ArrayList<DestinationModel>?){}

}