package id.gumilombok.presentation.ui.histori_transaksi


import android.os.Bundle
import android.view.MenuItem
import android.view.View

import id.gumilombok.R
import id.gumilombok.common.startActivity
import id.gumilombok.common.toCurrency
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.CustomAlert
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.home.MainActivity
import kotlinx.android.synthetic.main.activity_cetak_tiket.*
import kotlinx.android.synthetic.main.detail_penanan_tiket.*
import kotlinx.android.synthetic.main.toolbar.*
import net.glxn.qrgen.android.QRCode
import javax.inject.Inject

class CetakTiketActivity : BaseActivity() {
    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    @Inject lateinit var sp:PreferencesManager

    private var tiketId:String=""
    private var orderCode:String=""
    private var tiketAgent:String=""
    private var tiketName:String=""
    private var tiketPrice:String=""
    private var tiketRoute:String=""
    private var tiketKberangkatan:String=""
    private var tiketDetail:String=""

    private var durasiTour:String=""
    private var tiketPriceChil:String=""
    private var jumlahBookAnak:String=""
    private var jumlahBookDewasa:String=""
    private var totalBayar:String=""
    private var orderId:String=""
    private var bankName:String=""
    private var norek:String=""
    private var cardHolder:String=""
    private var metodeBayar:String=""
    private var orderStatus:String=""
    private lateinit var customAlert: CustomAlert

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cetak_tiket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Tiket"

        orderId=intent.getStringExtra("orderId")
        orderCode=intent.getStringExtra("orderCode")
        tiketId=intent.getStringExtra("tiketId")
        tiketId=intent.getStringExtra("tiketId")
        tiketAgent=intent.getStringExtra("tiketAgent")
        tiketName=intent.getStringExtra("tiketName")
        tiketPrice=intent.getStringExtra("tiketPrice")
        tiketRoute=intent.getStringExtra("tiketRoute")
        tiketKberangkatan=intent.getStringExtra("tiketKeberangkatan")
        tiketDetail=intent.getStringExtra("tiketDetail")

        durasiTour=intent.getStringExtra("durasi")
        tiketPriceChil=intent.getStringExtra("tiketPriceChil")
        totalBayar=intent.getStringExtra("totalBayar")
        jumlahBookAnak=intent.getStringExtra("jumlahBookAnak")
        jumlahBookDewasa = intent.getStringExtra("jumlahBookDewasa")


        val bitmap = QRCode.from(orderCode).withSize(1000, 1000).bitmap()
        imgBarcode.setImageBitmap(bitmap)
        tvOrderCode.text = "Book Code : ${orderCode}"
        tvNamaPemesan.text = sp.userSession.name
        edtEmail.text=sp.userSession.email


        if (jumlahBookAnak.isNotEmpty() && jumlahBookAnak != "0"){
            tvTotalTiketAnak.text = "${jumlahBookAnak} x ${toCurrency(tiketPriceChil.toInt())}"
            tvTiketAnak.text = "${jumlahBookAnak} Tiket Anak"
        }else{

            textView18.visibility = View.GONE
            tvTotalTiketAnak.visibility = View.GONE
            tvTiketAnak.visibility=View.GONE
        }
        if (jumlahBookDewasa.isNotEmpty() && jumlahBookDewasa != "0"){
            tvTotalTiketDewasa.text = "${jumlahBookDewasa} x ${toCurrency(tiketPrice.toInt())}"
            tvTiketDewasa.text = "${jumlahBookDewasa} Tiket Dewasa"
        }else{
            textView22.visibility = View.GONE
            tvTotalTiketDewasa.visibility = View.GONE
            tvTiketDewasa.visibility=View.GONE
        }
        tvTotal.text = "Rp. ${toCurrency(totalBayar.toInt())}"
        tvTiketAgent.text = tiketAgent
        tvNamaTiket.text = tiketName
        tvTanggalBerangkat.text=tiketKberangkatan
        btnClose.setOnClickListener {
            startActivity<MainActivity>()
        }




    }



}
