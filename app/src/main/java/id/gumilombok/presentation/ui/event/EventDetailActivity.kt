package id.gumilombok.presentation.ui.event

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import id.gumilombok.R
import kotlinx.android.synthetic.main.item_detail_acara_baru.*

class EventDetailActivity : AppCompatActivity() {

    private var eventId:String=""
    private var eventImage:String=""
    private var eventCost:String=""
    private var eventLocation:String=""
    private var eventDate:String=""
    private var createdAt:String=""
    private var eventName:String=""
    private var seoLink:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)

        eventId=intent.getStringExtra("eventId")
        eventImage=intent.getStringExtra("eventImage")
        eventCost=intent.getStringExtra("eventCost")
        eventLocation=intent.getStringExtra("eventLocation")
        eventDate=intent.getStringExtra("eventDate")
        createdAt=intent.getStringExtra("createdAt")
        eventName=intent.getStringExtra("eventName")
        seoLink=intent.getStringExtra("seoLink")

        Glide.with(this).load(eventImage).into(img_blog_view)
        tv_judul_acara.text=eventName.toUpperCase()
//        tv_Event_cost.text=eventCost
//        tv_event_desc.text=eventLocation
//        tv_event_date.text=eventDate
        tv_tanggal.text=createdAt
//        btn_readmore.setOnClickListener{
//            val intent = Intent(Intent.ACTION_VIEW)
//            intent.data = Uri.parse("https://gumilombok.id/gumi-event-detail/$seoLink")
//            startActivity(intent)
//        }
//        shareImageView.setOnClickListener{
//            share()
//        }
//        backImageView.setOnClickListener{
//            finish()
//        }
    }
    fun share(){
        val share = Intent.createChooser(Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "https://developer.android.com/training/sharing/")

            // (Optional) Here we're setting the title of the content
            putExtra(Intent.EXTRA_TITLE, "Introducing content previews")

            // (Optional) Here we're passing a content URI to an image to be displayed
//            setClipData(destinationImage);
            setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }, null)
        startActivity(share)
    }
}
