package id.gumilombok.presentation.ui.blog.adapter

import android.content.Context
import android.os.Build
import android.text.Html
import android.util.Log
import android.widget.Filter
import android.widget.Filterable
import com.bumptech.glide.Glide
import com.google.gson.Gson
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.UrlHelper
import id.gumilombok.data.remote.model.BlogModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_blog.view.*

class BlogAdapter (private val onItemClick: OnItemClick<BlogModel>,
                   private val searchResultCallback:SearchResultCallback):BaseListAdapter<BlogModel>(),Filterable{
    private val blogs=items
    private val url = UrlHelper()

    interface SearchResultCallback {
        fun onDataNotFound(s: String)
    }

    override fun getItemView(context: Context): BaseViewHolder<BlogModel> = ViewHolder(context)

    inner class ViewHolder(context: Context?):BaseViewHolder<BlogModel>(context){
        override fun layoutResId(): Int = R.layout.item_blog

        override fun bind(item: BlogModel, position: Int) {
            tv_blog_title.text=item.blogJudul
            Glide.with(this).load(url.imageUrl+item.blogCreatorImage).into(img_creator_profile)
            Glide.with(this).load(item.blogImage).into(img_blog_view)
            tvViews.text=item.blogView +" views"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvBlogIsi.setText(Html.fromHtml("${item.blogIsi}", Html.FROM_HTML_MODE_COMPACT));
            } else {
                tvBlogIsi.setText(Html.fromHtml("${item.blogIsi}"));
            }
            setOnClickListener{onItemClick.onItemClick(item,position)}
        }

    }

    override fun getFilter(): Filter {
        return object:Filter(){
            override fun performFiltering(text: CharSequence?): FilterResults {
                val search = text?.toString() ?: " "
                val itemList:List<BlogModel> = if (search.isEmpty()){
                    blogs
                }else{
                    blogs.filter {
//                        it.blogJudul?.toLowerCase()?.contains(search) == true || it.blogCreator?.toLowerCase()?.contains(search) == true
                        it.blogCategory?.toLowerCase()?.contains(search)==true
                    }

                }
                val results = FilterResults()
                results.values = itemList
                Log.d("cek filter tiket","${Gson().toJsonTree(itemList)}")
                return results
            }

            override fun publishResults(s: CharSequence?, results: FilterResults?) {
                items = results?.values as ArrayList<BlogModel>
                if (items.isEmpty()){
                    searchResultCallback.onDataNotFound(s?.toString()?:" ")
                }
                notifyDataSetChanged()
                Log.d("cek filter publist","${Gson().toJsonTree(items)}")
            }

        }
    }
}