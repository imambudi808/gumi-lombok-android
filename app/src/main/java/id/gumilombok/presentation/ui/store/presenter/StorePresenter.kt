package id.gumilombok.presentation.ui.store.presenter

import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.response.BaseResponse
import id.gumilombok.data.repository.StoreProdukRepository
import id.gumilombok.data.repository.StoreRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.store.view.StoreView
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StorePresenter @Inject constructor(val repo:StoreRepository,val sp:PreferencesManager):BasePresenter<StoreView>(){
    private val tag=this::class.java.simpleName

    //alstorelist
    fun showStoreList(offset: Int,limit: Int,clearItem:Boolean,text:String?=null){
        view?.showProgress((clearItem && text == null) || (!clearItem && text != null))
        view?.showLoading(!clearItem && text == null)
        val map=HashMap<String,String>()
        map["offset"] = offset.toString()
        map["limit"] = limit.toString()
        map["keyword"] = text.toString()
        disposables.add(
            repo.storeList(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                    view?.showLoading(false)
                }
                .subscribe({
                    view?.onStoresLoad(it,clearItem)
                },{ doOnError(view,it)})
        )
    }

    //showNewStore
    fun showNewStore(){
        view?.showLoading(true)
        disposables.add(
            repo.loadNewStore().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showLoading(false)
                }
                .subscribe({
                    view?.onLoadNewStore(it)
                },{ doOnError(view,it)})
        )
    }


}