package id.gumilombok.presentation.ui.rating.view


import id.gumilombok.data.remote.model.RatingModel
import id.gumilombok.data.remote.response.CekRatingResponse
import id.gumilombok.presentation.base.view.BaseView

interface RatingView : BaseView {
    fun onloadRating(rating:List<RatingModel>?){}
    fun cekRatingV(status:String?,pointValue:String?,ratingKomen:String?)
}