package id.gumilombok.presentation.ui.blog

import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import com.bumptech.glide.Glide
import id.gumilombok.R
import id.gumilombok.common.UrlHelper

import kotlinx.android.synthetic.main.activity_blog_detail.*
import kotlinx.android.synthetic.main.item_detail_blog_baru.*
import kotlinx.android.synthetic.main.toolbar.view.*

class BlogDetailActivity : AppCompatActivity() {


    private var blogName:String=""
    private var blogImage:String=""
    private var blogId:String=""
    private var blogIsi:String=""
    private var blogCreatorName:String=""
    private var blogCreatorDes:String=""
    private var blogCreatorImage:String=""
    private var blogView:String=""
    private var createdAt:String=""
    private var seoLink:String=""
    private val url = UrlHelper()
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blog_detail)
        setSupportActionBar(layToolbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title="Blog"

        blogName=intent.getStringExtra("blogName")
        blogImage=intent.getStringExtra("blogImage")
        blogId=intent.getStringExtra("blogId")
        blogIsi=intent.getStringExtra("blogIsi")
        blogCreatorName=intent.getStringExtra("blogCreatorName")
        blogCreatorDes=intent.getStringExtra("blogCreatorDes")
        blogCreatorImage=intent.getStringExtra("blogCreatorImage")
        blogView=intent.getStringExtra("blogView")
        createdAt=intent.getStringExtra("createdAt")
        seoLink=intent.getStringExtra("seoLink")

        tv_judul_artikel.text=blogName
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv_blog_text.setText(Html.fromHtml("${blogIsi}", Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv_blog_text.setText(Html.fromHtml("${blogIsi}"));
        }
        Glide.with(this).load(blogImage).into(img_blog_view)
        Glide.with(this).load(url.imageUrl+blogCreatorImage).into(iv_profile)
        tv_nama_penulis.text=blogCreatorName
        tv_tanggal.text="Published: "+createdAt
        btn_to_web.setOnClickListener{
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://gumilombok.id/gumi-blog-detail/$seoLink")
            startActivity(intent)
        }
//        shareImageView.setOnClickListener{
//            share()
//        }
//        backImageView.setOnClickListener{
//            finish()
//        }

    }
    fun share(){
        val share = Intent.createChooser(Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "https://developer.android.com/training/sharing/")

            // (Optional) Here we're setting the title of the content
            putExtra(Intent.EXTRA_TITLE, "Introducing content previews")

            // (Optional) Here we're passing a content URI to an image to be displayed
//            setClipData(destinationImage);
            setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }, null)
        startActivity(share)
    }
}
