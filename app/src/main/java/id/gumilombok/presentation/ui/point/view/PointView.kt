package id.gumilombok.presentation.ui.point.view

import id.gumilombok.data.remote.model.PointModel
import id.gumilombok.presentation.base.view.BaseView
import id.gumilombok.presentation.model.Point

interface PointView : BaseView {
    fun displayPointLocationById(points: List<Point>?) {}
}