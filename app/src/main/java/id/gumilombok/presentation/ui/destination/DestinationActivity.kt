package id.gumilombok.presentation.ui.destination


import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.intentFor
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.DestinationModel
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.EndlessRecyclerViewScrollListener

import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.destination.adapter.DestinationAdapter
import id.gumilombok.presentation.ui.destination.presenter.DestinationPresenter
import id.gumilombok.presentation.ui.destination.view.DestinationView


import kotlinx.android.synthetic.main.activity_destination.*

import kotlinx.android.synthetic.main.toolbar.view.*
import javax.inject.Inject

class DestinationActivity : BaseActivity(),DestinationView {
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    @Inject
    lateinit var presenter:DestinationPresenter
    @Inject
    lateinit var sp: PreferencesManager
    private var query:String?=""

    lateinit var adapter:DestinationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destination)
        setSupportActionBar(layToolbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter.bind(this, isOnline)
        title = "Destinasi Wisata"
        setUpView()
    }

    private fun setUpView(){
        adapter = DestinationAdapter(object : OnItemClick<DestinationModel> {
            override fun onItemClick(item: DestinationModel?, position: Int?) {
                startActivity(intentFor<DetailDestinationActivity>().apply {
                    putExtra("destinationId", item?.destinationId)
                    putExtra("destinationName",item?.destinationName)
                    putExtra("destinationAuthor",item?.destinationAuthor)
                    putExtra("destinationContent",item?.destinationContent)
                    putExtra("destinationImage",item?.destinationImage)
                    putExtra("destinationAddress",item?.destinationAddress)
                    putExtra("destinationLat",item?.destinationLat)
                    putExtra("destinationLong",item?.destinationLong)
                    putExtra("destinationLokasi",item?.islands?.IslandName)
                    putExtra("desRataRating",item?.rataRating)
                    putExtra("seoLink",item?.seoLink)
                    Log.d("dat rata rating :",item?.rataRating)

                })
            }
        }, object : DestinationAdapter.SearchResultCallback {
            override fun onDataNotFound(s: String) {
                presenter.showListDestination(0, 5, false, s)
            }
        })

        val layoutManager = GridLayoutManager(this,2)
        rvDestination.layoutManager = layoutManager
        rvDestination.adapter = adapter
        rvDestination.invalidate()
        val scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.showListDestination(totalItemsCount, totalItemsCount.plus(5), false, query)
            }
        }
        rvDestination.addOnScrollListener(scrollListener)
        presenter.showListDestination(0,5,false)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchItem: MenuItem? = menu?.findItem(R.id.act_search)
        val searchView = searchItem?.actionView as SearchView?
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean = false

            override fun onQueryTextChange(newText: String?): Boolean {
                query = newText
                adapter.filter.filter(query)
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun showLoadMoreProgress(show: Boolean) {
        loadMoreProgress.visibility = if (show) View.VISIBLE else View.GONE
    }
    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onDestinationLoaded(transaksis: List<DestinationModel>?, clearItem: Boolean) {
        if (clearItem) adapter.clearItems()
        adapter.addItems(transaksis)
    }

}
