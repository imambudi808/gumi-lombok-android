package id.gumilombok.presentation.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.gumilombok.R
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.Gumi
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.module.SyncServiceModule
import id.gumilombok.presentation.ui.histori_transaksi.Adapter.OrderAdapter
import id.gumilombok.presentation.ui.ticket.presenter.OrderPresenter
import id.gumilombok.presentation.ui.ticket.view.OrderView
import kotlinx.android.synthetic.main.fragment_histori.*
import javax.inject.Inject
import id.gumilombok.common.*
import id.gumilombok.data.remote.model.HistoriModel
import id.gumilombok.presentation._costume.EndlessRecyclerViewScrollListener
import id.gumilombok.presentation.ui.histori_transaksi.DetailHistoriTransaksi
import kotlinx.android.synthetic.main.fragment_histori.progress

class HistoriFragment:Fragment(), OrderView {

    @Inject
    lateinit var presenter: OrderPresenter
    @Inject
    lateinit var sp: PreferencesManager
    private var query:String?=""
    lateinit var adapter: OrderAdapter

    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val serviceInjector: SyncComponent = Gumi.get(context!!.applicationContext)
            .appComponent
            .syncComponent()
            .syncModule(SyncServiceModule(context!!.applicationContext))
            .build()
        serviceInjector.inject(this)


    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_histori, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()

    }

    private fun setupView() = view?.run {
        presenter.bind(this@HistoriFragment,isNetworkAvailable(context))
        adapter = OrderAdapter(object : OnItemClick<HistoriModel> {
            override fun onItemClick(item: HistoriModel?, position: Int?) {
                val intent = Intent(activity, DetailHistoriTransaksi::class.java)
                    intent.putExtra("orderId",item?.orderId)
                intent.putExtra("orderCode",item?.orderCode)
                intent.putExtra("tiketId",item?.tiketId)
                intent.putExtra("tiketId",item?.orderCode)
                intent.putExtra("tiketAgent",item?.tiketAgent)
                intent.putExtra("tiketName",item?.tiketName)
                intent.putExtra("tiketPrice",item?.tiketPriceAdult)
                intent.putExtra("tiketRoute",item?.tiketRute)
                intent.putExtra("tiketKeberangkatan",item?.tiketKeberangkatan)
                intent.putExtra("tiketDetail",item?.tiketDetail)

                intent.putExtra("durasi",item?.durasiTiket)
                intent.putExtra("tiketPriceChil",item?.tiketPriceChil)
                intent.putExtra("totalBayar",item?.totalBayar)
                intent.putExtra("jumlahBookDewasa",item?.jumlahAdult)
                intent.putExtra("jumlahBookAnak",item?.jumlahAnak)
                intent.putExtra("metodeBayar",item?.metodeBayar)
                intent.putExtra("bankName",item?.bankTujuan)
                intent.putExtra("norek",item?.norekening)
                intent.putExtra("cardHolder",item?.cardHolder)
                intent.putExtra("orderStatus",item?.orderStatus)
                startActivity(intent)

            }
        }, object : OrderAdapter.SearchResultCallback {
            override fun onDataNotFound(s: String) {
                presenter.showListOrder(sp.userSession.id,0, 15, false, s)
            }
        })

        val layoutManager = LinearLayoutManager(context)
        rvHistori.layoutManager = layoutManager
        rvHistori.adapter = adapter
        rvHistori.invalidate()
        val scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.showListOrder(sp.userSession.id,totalItemsCount, totalItemsCount.plus(15), false, query)
            }
        }
        rvHistori.addOnScrollListener(scrollListener)
        presenter.showListOrder(sp.userSession.id,0,15,false)

    }

    override fun onOrderLoaded(orders: List<HistoriModel>?, clearItem: Boolean) {

        if (clearItem) adapter.clearItems()
        adapter.addItems(orders)
    }
}