package id.gumilombok.presentation.ui.ticket

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import id.gumilombok.R
import id.gumilombok.common.intentFor
import id.gumilombok.common.startActivity
import id.gumilombok.common.toCurrency
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.home.MainActivity
import kotlinx.android.synthetic.main.activity_metode_pembayaran.*
import kotlinx.android.synthetic.main.detail_penanan_tiket.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.singleTop

class MetodePembayaran : BaseActivity() {
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }
    var orderId:String?=""
    private var tiketId:String=""
    private var tiketAgent:String=""
    private var tiketName:String=""
    private var tiketPrice:String=""
    private var tiketRoute:String=""
    private var tiketKberangkatan:String=""
    private var tiketDetail:String=""
    private var tiketAvailableBook:String=""
    private var durasiTour:String=""
    private var tiketPriceChil:String=""
    private var jumlahBookAnak:String=""
    private var jumlahBookDewasa:String=""
    private var totalBayar:String=""
    private var orderIdBase:String=""
    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_metode_pembayaran)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Pilih Pembayaran"

        orderId=intent.getStringExtra("orderId")
        tiketId=intent.getStringExtra("tiketId")
        tiketAgent=intent.getStringExtra("tiketAgent")
        tiketName=intent.getStringExtra("tiketName")
        tiketPrice=intent.getStringExtra("tiketPrice")
        tiketRoute=intent.getStringExtra("tiketRoute")
        tiketKberangkatan=intent.getStringExtra("tiketKeberangkatan")
        tiketDetail=intent.getStringExtra("tiketDetail")
//        tiketAvailableBook=intent.getStringExtra("tiketAvailableBook")
        durasiTour=intent.getStringExtra("durasi")
        tiketPriceChil=intent.getStringExtra("tiketPriceChil")
        totalBayar=intent.getStringExtra("totalBayar")
        jumlahBookAnak=intent.getStringExtra("jumlahBookAnak")
        jumlahBookDewasa = intent.getStringExtra("jumlahBookDewasa")
        orderIdBase=intent.getStringExtra("orderId")

        tvInvoice.text = "Invoice : ${orderId.toString()}"
        tvTotalBayar.text = "Total : Rp ${toCurrency(totalBayar.toInt())}"

        constDetailPenanan.visibility=View.GONE
        showDetail.setOnClickListener {

            if (constDetailPenanan.visibility == View.GONE){
                constDetailPenanan.visibility = View.VISIBLE
                showDetail.setText("Hide Detail")
            }else if (constDetailPenanan.visibility==View.VISIBLE){
                constDetailPenanan.visibility=View.GONE
                showDetail.setText("Show Detail")
            }
        }


        if (jumlahBookAnak.isNotEmpty() && jumlahBookAnak != "0"){
            tvTotalTiketAnak.text = "${jumlahBookAnak} x ${toCurrency(tiketPriceChil.toInt())}"
        }else{

            textView18.visibility = View.GONE
            tvTotalTiketAnak.visibility = View.GONE
        }
        if (jumlahBookDewasa.isNotEmpty() && jumlahBookDewasa != "0"){
            tvTotalTiketDewasa.text = "${jumlahBookDewasa} x ${toCurrency(tiketPrice.toInt())}"
        }else{
            textView22.visibility = View.GONE
            tvTotalTiketDewasa.visibility = View.GONE
        }
        tvTotal.text = "Rp. ${toCurrency(totalBayar.toInt())}"
        tvTiketAgent.text = tiketAgent
        tvNamaTiket.text = tiketName
        layTransferBank.setOnClickListener{
            startActivity(intentFor<PilihBankActivity>().apply {
                putExtra("orderId",orderIdBase)
                putExtra("tiketId",tiketId)
                putExtra("tiketAgent",tiketAgent)
                putExtra("tiketName",tiketName)
                putExtra("tiketPrice",tiketPrice)
                putExtra("tiketRoute",tiketRoute)
                putExtra("tiketKeberangkatan",tiketKberangkatan)
                putExtra("tiketDetail",tiketDetail)
//                putExtra("tiketAvailableBook",tiketAvailableBook)
                putExtra("durasi",durasiTour)
                putExtra("tiketPriceChil",tiketPriceChil)
                putExtra("totalBayar",totalBayar.toString())
                putExtra("jumlahBookDewasa",jumlahBookDewasa)
                putExtra("jumlahBookAnak",jumlahBookAnak)
            })
        }




//        layDetailPenanTiket.visibility = View.GONE
    }
    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(intentFor<MainActivity>().singleTop().clearTop().clearTask())
        finish()
    }
}
