package id.gumilombok.presentation.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.gumilombok.Gumi
import id.gumilombok.R
import id.gumilombok.common.isNetworkAvailable
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.di.module.SyncServiceModule
import id.gumilombok.presentation.ui.destination.adapter.DestinationAdapter
import id.gumilombok.presentation.ui.destination.presenter.DestinationPresenter
import id.gumilombok.presentation.ui.destination.view.DestinationView
import kotlinx.android.synthetic.main.fragment_des_search.*
import javax.inject.Inject
import id.gumilombok.common.*
import id.gumilombok.data.remote.model.DestinationModel
import id.gumilombok.presentation._costume.EndlessRecyclerViewScrollListener
import id.gumilombok.presentation.ui.destination.DetailDestinationActivity
import kotlinx.android.synthetic.main.fragment_des_search.view.*





class DestinationSearchFragment: Fragment(),DestinationView {
    @Inject
    lateinit var presenter: DestinationPresenter
    @Inject
    lateinit var sp: PreferencesManager
    private var query: String? = ""

    lateinit var adapter: DestinationAdapter

    override fun showMessage(message: String?) {
        message?.let { Toast.makeText(getActivity(),it, Toast.LENGTH_SHORT).show(); }
    }

    override fun showLoadMoreProgress(show: Boolean) {
        progress?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun showProgress(show: Boolean) {
        progress?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        set

        val serviceInjector: SyncComponent = Gumi.get(context!!.applicationContext)
            .appComponent
            .syncComponent()
            .syncModule(SyncServiceModule(context!!.applicationContext))
            .build()
        serviceInjector.inject(this)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        return inflater.inflate(R.layout.fragment_des_search, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()


    }

    private fun setupView() = view?.run {
        searchViewDes.setIconifiedByDefault(true);
        searchViewDes.setFocusable(true);
        searchViewDes.setIconified(false);
        searchViewDes.requestFocusFromTouch();

        searchViewDes.setOnQueryTextListener(object : android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = false
            override fun onQueryTextChange(newText: String?): Boolean {
                query = newText
                adapter.filter.filter(query)
                return true
            }
        })

        btnAlam.setOnClickListener {
            searchViewDes.setQuery("alam",true)
        }
        btnPantai.setOnClickListener {
            searchViewDes.setQuery("pantai",true)
        }
        btnAir.setOnClickListener {
            searchViewDes.setQuery("air terjun",true)
        }
        btnKerajinan.setOnClickListener {
            searchViewDes.setQuery("kerajinan",true)
        }





        presenter.bind(this@DestinationSearchFragment,isNetworkAvailable(context))
        adapter = DestinationAdapter(object : OnItemClick<DestinationModel> {
            override fun onItemClick(item: DestinationModel?, position: Int?) {
                val intent = Intent(activity, DetailDestinationActivity::class.java)
                intent.putExtra("destinationId", item?.destinationId)
                intent.putExtra("destinationName",item?.destinationName)
                intent.putExtra("destinationAuthor",item?.destinationAuthor)
                intent.putExtra("destinationContent",item?.destinationContent)
                intent.putExtra("destinationImage",item?.destinationImage)
                intent.putExtra("destinationAddress",item?.destinationAddress)
                intent.putExtra("destinationLat",item?.destinationLat)
                intent.putExtra("destinationLong",item?.destinationLong)
                intent.putExtra("destinationLokasi",item?.islands?.IslandName)
                intent.putExtra("desRataRating",item?.rataRating)
                intent.putExtra("desJumlahReview",item?.jumlahResponse)
                startActivity(intent)
            }
        }, object : DestinationAdapter.SearchResultCallback {
            override fun onDataNotFound(s: String) {
                presenter.showListDestination(0,15,false)
            }
        })
    val layoutManager = GridLayoutManager(context,2)
    rvDestination.layoutManager = layoutManager
        rvDestination.adapter = adapter
        rvDestination.invalidate()
        val scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.showListDestination(totalItemsCount, totalItemsCount.plus(15), false, query)
            }
        }
        rvDestination.addOnScrollListener(scrollListener)
        presenter.showListDestination(0,15,false)

    }
    override fun onDestinationLoaded(transaksis: List<DestinationModel>?, clearItem: Boolean) {
        if (clearItem) adapter.clearItems()
        adapter.addItems(transaksis)
    }

}