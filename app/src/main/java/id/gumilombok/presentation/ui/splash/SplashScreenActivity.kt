package id.gumilombok.presentation.ui.splash


import android.content.Intent
import android.os.Bundle
import id.gumilombok.R
import id.gumilombok.common.startActivity
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.home.MainActivity
import javax.inject.Inject

class SplashScreenActivity : BaseActivity() {

    @Inject lateinit var sp:PreferencesManager
    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        if (sp.isFirstOpen){
            startActivity<WelcomeActivity>()
            finish()
            return
        }else{
            val c=if (sp.isLoggedIn){
                MainActivity::class.java
            }else{
                WelcomeActivity::class.java
            }
            startActivity(Intent(this@SplashScreenActivity,c).apply {
//                putExtra("",sp.u)
            })
            finish()
        }
    }
}
