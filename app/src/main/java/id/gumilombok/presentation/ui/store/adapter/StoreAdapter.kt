package id.gumilombok.presentation.ui.store.adapter

import android.content.Context
import com.squareup.picasso.Picasso
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.data.remote.model.StoreModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_store.view.*

class StoreAdapter(private val onItemClick:OnItemClick<StoreModel>):BaseListAdapter<StoreModel>() {
    override fun getItemView(context: Context): BaseViewHolder<StoreModel> =Holder(context)

    inner class Holder(context: Context?):BaseViewHolder<StoreModel>(context){
        override fun layoutResId(): Int = R.layout.item_store

        override fun bind(item: StoreModel, position: Int) {
            Picasso.get().load(item.storeBanner).into(store_banner)
            tvDesName.text=item.storeName
            tvLokasiStore.text="Lombok"

            setOnClickListener { onItemClick.onItemClick(item,position) }
        }

    }

}