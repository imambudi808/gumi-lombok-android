package id.gumilombok.presentation.ui.destination.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import id.gumilombok.common.OnItemClick
import id.gumilombok.data.remote.model.DestinationModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import id.gumilombok.R
import id.gumilombok.common.UrlHelper
import kotlinx.android.synthetic.main.item_destination.view.*
import java.text.DecimalFormat


class DestinationAdapter(private val itemClickListener: OnItemClick<DestinationModel>,
                         private val searchResultCallback:SearchResultCallback):BaseListAdapter<DestinationModel>(),Filterable {

    private val transaksisCopy = items
    private var sum:Int?=0
    private val url = UrlHelper()
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(text: CharSequence?): FilterResults {
                val search = text?.toString() ?: " "
                val itemList: List<DestinationModel> = if (search.isEmpty()) {
                    transaksisCopy
                } else {
                    transaksisCopy.filter {
                        it.destinationName?.toLowerCase()?.contains(search) == true || it.destinationCategory?.toLowerCase()?.contains(search)==true
                    }
                }
                val result = Filter.FilterResults()
                result.values = itemList
                Log.d("cek filter", "${Gson().toJsonTree(itemList)}")
                return result
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(s: CharSequence?, results: FilterResults?) {
                items = results?.values as ArrayList<DestinationModel>
                if (items.isEmpty()) {
                    searchResultCallback.onDataNotFound(s?.toString() ?: " ")
                }
                notifyDataSetChanged()
                Log.d("cek filter publish", "${Gson().toJsonTree(items)}")
            }
        }
    }

    override fun getItemView(context: Context): BaseViewHolder<DestinationModel> = ViewHolder(context)


    inner class ViewHolder(context: Context?) : BaseViewHolder<DestinationModel>(context) {
        override fun layoutResId(): Int = R.layout.item_destination

        @SuppressLint("SetTextI18n")
        override fun bind(item: DestinationModel, position: Int) {
            val f = DecimalFormat("#.#")

            tvDesName.text=item.destinationName.toString()
            tvPulau.text="Sumber : Google"
            tvLokasi.visibility = View.GONE
            Log.d("Data rata rating :",item.rataRating.toString())
            Picasso.get().load(item.destinationImage).into(imgDestination)
            ratingBarDes.rating=item.rataRating!!.toFloat()
           /* if(f.format(item.rataRating?.toDouble()).toDouble() >=0.5 && f.format(item.rataRating?.toDouble()).toDouble() < 1.0){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_half_black_24dp)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
            }
            else if(f.format(item.rataRating?.toDouble()).toDouble() >=1.0 && f.format(item.rataRating?.toDouble()).toDouble() < 1.5){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
            }
            else if(f.format(item.rataRating?.toDouble()).toDouble() >=1.5 && f.format(item.rataRating?.toDouble()).toDouble() < 2.0){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_half_black_24dp)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
            }
            else if(f.format(item.rataRating?.toDouble()).toDouble() >=2.0 && f.format(item.rataRating?.toDouble()).toDouble() < 2.5){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
            }
            else if(f.format(item.rataRating?.toDouble()).toDouble() >=2.5 && f.format(item.rataRating?.toDouble()).toDouble() < 3.0){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_half_black_24dp)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
            }
            else if(f.format(item.rataRating?.toDouble()).toDouble() >= 3.0 && f.format(item.rataRating?.toDouble()).toDouble() < 3.5){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
            }
            else if(f.format(item.rataRating?.toDouble()).toDouble() >=3.5 && f.format(item.rataRating?.toDouble()).toDouble() < 4.0){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_half_black_24dp)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
            }
            else if(f.format(item.rataRating?.toDouble()).toDouble() >= 4.0 && f.format(item.rataRating?.toDouble()).toDouble() <  4.5){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
            }
            else if(f.format(item.rataRating?.toDouble()).toDouble() >= 4.5  && f.format(item.rataRating?.toDouble()).toDouble() < 5.0){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_half_black_24dp)
            }
            else if(f.format(item.rataRating?.toDouble()).toDouble()>= 5.0 ){
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_berisi)
            }else{
                bintang1.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang2.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang3.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
                bintang4.background=ContextCompat.getDrawable(context,R.drawable.ic_star_kosong)
            }
            */
            tvReview.text="${item.jumlahResponse?.toDouble()?.toInt()} Reviews"


//            tvStatus.background= ContextCompat.getDrawable(context,R.drawable.bg_blue_radius_4)

            setOnClickListener { itemClickListener.onItemClick(item, position) }
            Log.d("cek isi dalam adapter", "${Gson().toJsonTree(items)}")

        }

    }

    interface SearchResultCallback {
        fun onDataNotFound(s: String)
    }
}