package id.gumilombok.presentation.ui.ticket


import android.app.DatePickerDialog
import android.os.Bundle
import android.view.MenuItem

import android.view.View
import android.widget.DatePicker
import androidx.recyclerview.widget.LinearLayoutManager
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.intentFor


import id.gumilombok.common.toCurrency
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.ServiceModel

import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity

import id.gumilombok.presentation.ui.ticket.adapter.TiketServiceAdapter
import id.gumilombok.presentation.ui.ticket.presenter.TiketServicePresenter
import id.gumilombok.presentation.ui.ticket.view.TiketServiceView
import kotlinx.android.synthetic.main.activity_detail_tiket.*
import kotlinx.android.synthetic.main.activity_ticket.*
import kotlinx.android.synthetic.main.activity_ticket.progress

import kotlinx.android.synthetic.main.detail_tour.*
import kotlinx.android.synthetic.main.toolbar.*
import java.text.DateFormat
import java.util.*
import javax.inject.Inject







class DetailTiketActivity : BaseActivity(), TiketServiceView {

    @Inject lateinit var servicePresenter:TiketServicePresenter

    @Inject lateinit var sp:PreferencesManager
    lateinit var serviceAdapter:TiketServiceAdapter

    private var tiketId:String=""
    private var tiketAgent:String=""
    private var tiketName:String=""
    private var tiketPrice:String=""
    private var tiketRoute:String=""
    private var tiketKberangkatan:String=""
    private var tiketDetail:String=""
    private var tiketAvailableBook:String=""
    private var durasiTour:String=""
    private var tiketPriceChil:String=""
    private var jumlahBook:String=""
    private var totalBayar:Int=0
    private var tglBerangkat:String?=""
    var cal = Calendar.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(id.gumilombok.R.layout.activity_detail_tiket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        servicePresenter.bind(this,isOnline)
        title = "Detail Tiket Tour"

        tiketId=intent.getStringExtra("tiketId")
//        val intent = intent
        tiketAgent=intent.getStringExtra("tiketAgent")
        tiketName=intent.getStringExtra("tiketName")
        tiketPrice=intent.getStringExtra("tiketPrice")
        tiketRoute=intent.getStringExtra("tiketRoute")
        tiketKberangkatan=intent.getStringExtra("tiketKeberangkatan")
        tiketDetail=intent.getStringExtra("tiketDetail")
        tiketAvailableBook=intent.getStringExtra("tiketAvailableBook")
        durasiTour=intent.getStringExtra("durasi")
        tiketPriceChil=intent.getStringExtra("tiketPriceChil")
        tglBerangkat=intent.getStringExtra("tanggalBerangkat")



        tv_agent_name.text = tiketAgent
        tvTourName.text = tiketName
        if (tglBerangkat!=null){
            tv_bernagkat.text = "Tanggal Berangkat : ${tglBerangkat}"
        }else{
            updateDateInView()
        }

        tvNameTiket1.visibility = View.GONE
        tvNameTiket2.visibility = View.GONE
        tvPriceDewasa.visibility = View.GONE
        tvPriceAnak.visibility = View.GONE
        if (tiketPrice.isNotEmpty()){
            tvNameTiket1.visibility = View.VISIBLE
            tvPriceDewasa.visibility = View.VISIBLE
            tvPriceDewasa.text = "Rp ${toCurrency(tiketPrice.toInt())}"

        }
        if (tiketPriceChil.isNotEmpty()){
            tvNameTiket2.visibility = View.VISIBLE
            tvPriceAnak.visibility = View.VISIBLE
            tvPriceAnak.text = "Rp ${toCurrency(tiketPriceChil.toInt())}"

        }


        tvDurasi.text = "${durasiTour} Jam"



        serviceAdapter = TiketServiceAdapter(object : OnItemClick<ServiceModel> {
            override fun onItemClick(item: ServiceModel?, position: Int?) {

            }
        })

        val layoutManager = LinearLayoutManager(this)
        rvService.layoutManager = layoutManager
        rvService.adapter = serviceAdapter
        servicePresenter.showTiketServiceByTiket(tiketId)
        btnExpandService.setText("Show More")

                lay_detail_service.visibility = View.GONE
        btnExpandService.setOnClickListener {
            if (lay_detail_service.visibility == View.GONE){
                lay_detail_service.visibility = View.VISIBLE
                btnExpandService.setText("Hide More")
            }else{
                lay_detail_service.visibility = View.GONE
                btnExpandService.setText("Show More")
            }
        }


        btnPesan.setOnClickListener {
            startActivity(intentFor<KonfirmasiTransaksi>().apply {
                putExtra("tiketId",tiketId)
                putExtra("tiketAgent",tiketAgent)
                putExtra("tiketName",tiketName)
                putExtra("tiketPrice",tiketPrice)
                putExtra("tiketRoute",tiketRoute)
                putExtra("tiketKeberangkatan",tiketKberangkatan)
                putExtra("tiketDetail",tiketDetail)
                putExtra("tiketAvailableBook",tiketAvailableBook)
                putExtra("durasi",durasiTour)
                putExtra("tiketPriceChil",tiketPriceChil)
                putExtra("tanggalBerangkat",tglBerangkat)
            })
        }

//        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
//            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
//                                   dayOfMonth: Int) {
//                cal.set(Calendar.YEAR, year)
//                cal.set(Calendar.MONTH, monthOfYear)
//                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
//                updateDateInView()
//            }
//        }
//
//        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
//        tvGantiTgl!!.setOnClickListener(object : View.OnClickListener {
//            override fun onClick(view: View) {
//                DatePickerDialog(this@DetailTiketActivity,
//                    dateSetListener,
//                    // set DatePickerDialog to point to today's date when it loads up
//                    cal.get(Calendar.YEAR),
//                    cal.get(Calendar.MONTH),
//                    cal.get(Calendar.DAY_OF_MONTH)).show()
//            }
//
//        })

    }

    private fun updateDateInView() {
        val myFormat = "MM-dd-yyyy" // mention the format you need
        val sdf = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault())
        tv_bernagkat!!.text = "Tanggal Berangkat : ${sdf.format(cal.getTime())}"
        tv_bernagkat.text = sdf.format(cal.getTime()).toString()
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    override fun onServiceLoad(services: List<ServiceModel>?) {

        serviceAdapter.addItems(services)
    }


    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }
}
