package id.gumilombok.presentation.ui.blog.presenter


import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.repository.BlogRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.blog.view.BlogView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class BlogPresenter @Inject constructor(val repo:BlogRepository,
                                        val sp:PreferencesManager): BasePresenter<BlogView>() {
    private val tag=this::class.java.simpleName

    fun showListBlog(offset:String,limit:String){
        view?.showLoading(true)
        disposables.add(
            repo.blogList(offset, limit)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showLoading(false)
                }
                .subscribe({
                    view?.onBlogLoad(it)
                },{
                    doOnError(view,it)
                })
        )
    }

    fun showListsBlogs(offset: Int,limit: Int,clearItem:Boolean,text:String?=null){
        view?.showProgress((clearItem && text == null) || (!clearItem && text != null))
        view?.showLoading(!clearItem && text == null)
        val map=HashMap<String,String>()
        map["offset"] = offset.toString()
        map["limit"] = limit.toString()
        map["keyword"] = text.toString()
        disposables.add(
            repo.blogsLists(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                    view?.showLoading(false)
                }
                .subscribe({
                    view?.onBlogLoads(it,clearItem)
                },{ doOnError(view,it)})
        )
    }

    fun loadHotBlog(){
        disposables.add(
            repo.loadHotBlog()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{

                }
                .subscribe({
                    view?.onBlogLoad(it)
                },{
                    doOnError(view,it)
                })
        )
    }

    fun loadListBlogByCategory(offset: Int,limit: Int,category: String?="",clearItem: Boolean){
        view?.showLoading(true)
        val map=HashMap<String,String>()
        map["offset"]=offset.toString()
        map["limit"]=limit.toString()
        map["blog_category"]=category.toString()
        disposables.add(
            repo.loadBlogByCategory(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach {
                    view?.showLoading(false)
                }
                .subscribe({
                    view?.onBlogByCategoryload(it,clearItem)
                },{ doOnError(view,it)})
        )
    }
}