package id.gumilombok.presentation.ui.covid.adapter

import android.content.Context
import android.view.View
import id.gumilombok.R
import id.gumilombok.common.NumberUtils
import id.gumilombok.common.OnItemClick
import id.gumilombok.data.remote.model.CovidModel
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_covid.view.*

class CovidAdapter(private val onItemClick:OnItemClick<CovidModel>):BaseListAdapter<CovidModel>() {
    override fun getItemView(context: Context): BaseViewHolder<CovidModel> =Holder(context)

    inner class Holder(context: Context?):BaseViewHolder<CovidModel>(context){
        override fun layoutResId(): Int = R.layout.item_covid

        override fun bind(item: CovidModel, position: Int) {
//            tv_deats.text="Kematian : ${NumberUtils.numberFormat(item.kematian)}"
//            tv_positive.text="Positif : ${NumberUtils.numberFormat(item.positife)}"
//            tv_sembuh.text="Sembuh : ${NumberUtils.numberFormat(item.sembuh)}"
//            tv_total.text="Total Kasus : ${NumberUtils.numberFormat(item.totalKasus)}"
//            tv_tanggal.text="${NumberUtils.formatTime(item.lastUpdate)}"
            tv_deats.text="Meninggal : ${item.meninggalId}"
            tv_positive.text="Positif : ${item.positifId}"
            tv_sembuh.text="Sembuh : ${item.sembuhId}"
            setOnClickListener { onItemClick.onItemClick(item,position) }

        }

    }

}