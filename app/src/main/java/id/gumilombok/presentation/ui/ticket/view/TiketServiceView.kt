package id.gumilombok.presentation.ui.ticket.view

import id.gumilombok.data.remote.model.ServiceModel
import id.gumilombok.data.remote.model.TiketModel
import id.gumilombok.presentation.base.view.BaseView

interface TiketServiceView: BaseView {

    fun onServiceLoad(tikets: List<ServiceModel>?) {}

}