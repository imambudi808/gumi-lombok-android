package id.gumilombok.presentation.ui.splash


import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.PagerAdapter

import id.gumilombok.R
import id.gumilombok.common.startActivity
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.ViewPagerAdapter
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_welcome.*
import javax.inject.Inject

class WelcomeActivity : BaseActivity() {

    @Inject lateinit var sp:PreferencesManager
    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        setupViewPager()
    }

    private fun setUserAs(){
        sp.setupFirstOpen()
        startActivity<LoginActivity>()
        finish()
    }

    private fun setupViewPager(){
        val pagerAdapter = ViewPagerAdapter(supportFragmentManager)
        (0..1).forEachIndexed{index, i ->
            pagerAdapter.addFragment(WelcomeFragment.newInstance(index))
        }
        viewPager.adapter = pagerAdapter
        pagerIndicator.setViewPager(viewPager)

        if (!sp.isFirstOpen){
            welcomeScreen.visibility = View.GONE
            setUserAs()
        }
        btnSkip.setOnClickListener{
            welcomeScreen.visibility = View.GONE
            setUserAs()
        }
        btnNext.setOnClickListener{
            when(viewPager.currentItem){
                0->{viewPager.currentItem=1}
                1->{setUserAs()}
            }
        }
    }
}
