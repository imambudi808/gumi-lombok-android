package id.gumilombok.presentation.ui.home


import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

import id.gumilombok.R
import id.gumilombok.common.*
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity

import id.gumilombok.presentation.ui.login.LoginActivity
import id.gumilombok.presentation.ui.login.presenter.LoginPresenter
import id.gumilombok.presentation.ui.login.view.LoginView
import kotlinx.android.synthetic.main.activity_main.*

import javax.inject.Inject

class MainActivity : BaseActivity(),LoginView {


    override fun onLoginSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRegistrasionSuccses() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onUpdateProfileSuccses() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onUserNotRegistred() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showMessage(message: String?) {

    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    private lateinit var navButtons: Array<Pair<ImageButton, Int>>
    @Inject lateinit var sp:PreferencesManager
    @Inject lateinit var presenter:LoginPresenter



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
    }

    private fun setupView() {
        val userSession=sp.userSession.email
        if (userSession == null || userSession.isEmpty()){
            startActivity<LoginActivity>()
            finish()
        }
        presenter.bind(this,isOnline)
        presenter.cekPoint(sp.userSession.id.toString())
        setStatusBackgroundColor(getMyColor(R.color.white))
        setLightStatusBar(true)


        navButtons = arrayOf(
            Pair(btnHome, R.id.homeFragment),
            Pair(btnSearchDes,R.id.searchDesFragment),
            Pair(btnHistory, R.id.historiFragment),
            Pair(btnNotif,R.id.notifFragment),
            Pair(btnProfile,R.id.profileFragment)
//            Pair(
//                btnProfile,
//                if (pref.isLoggedIn)
//                    R.id.profileFragment
//                else
//                    R.id.profileUnathorizedFragment
//            )
        )

        navButtons.forEachIndexed { i, pair ->
            pair.first.setOnClickListener {
                navHostFragment.findNavController().navigate(pair.second)
                onPageChanged(i)
            }
        }
        onPageChanged(0)


    }


    override fun onBackPressed() {
        super.onBackPressed()

        moveTaskToBack(true)
        finish()

    }

    override fun onResume() {
        super.onResume()
        presenter.cekPoint(sp.userSession.id.toString())
    }

    override fun onRestart() {
        super.onRestart()
        presenter.cekPoint(sp.userSession.id.toString())
    }

//    override fun onDestroy() {
//        super.onDestroy()
//        finish()
//    }

    private fun onPageChanged(i: Int) {
        navButtons.forEachIndexed { index, nav ->
            if (index == i) {
                nav.first.setColorFilter(
                    ContextCompat.getColor(this, R.color.black),
                    android.graphics.PorterDuff.Mode.SRC_IN
                )
            } else {
                nav.first.setColorFilter(
                    ContextCompat.getColor(this, R.color.grey),
                    android.graphics.PorterDuff.Mode.SRC_IN
                )
            }
        }
    }
}
