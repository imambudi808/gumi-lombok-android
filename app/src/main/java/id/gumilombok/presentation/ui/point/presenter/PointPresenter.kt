package id.gumilombok.presentation.ui.point.presenter


import android.util.Log
import com.google.gson.Gson
import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.PointModel
import id.gumilombok.data.repository.PointRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.model.Point
import id.gumilombok.presentation.ui.point.view.PointView
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class PointPresenter @Inject constructor(val repo: PointRepository,
                                               val sp: PreferencesManager
): BasePresenter<PointView>() {

    val TAG = this::class.java.simpleName


    fun loadPointByIdDestination(destId:String?){
        view?.showProgress(true)
        disposables.add(
            repo.pointListByIdDes(destId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{view?.showProgress(false)}
                .subscribe({
                    if (it.status){
                        it.data?.also { data ->
                            val point:List<Point> = data.map { it.build() }
                            view?.displayPointLocationById(point)
                        }
                    }else{
                        view?.showMessage(it.pesan)
                    }
                },{ doOnError(view,it)})
        )
    }

}