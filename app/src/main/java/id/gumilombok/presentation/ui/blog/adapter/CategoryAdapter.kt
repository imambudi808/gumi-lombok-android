package id.gumilombok.presentation.ui.blog.adapter
import android.widget.LinearLayout
import androidx.core.view.marginBottom
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import id.gumilombok.R
import kotlinx.android.synthetic.main.item_category.view.*

class CategoryAdapter: Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.run {
            if (position == 0) {
                val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                params.setMargins(container.marginStart * 2, container.top, container.marginEnd, container.marginBottom)
                container.layoutParams = params
                container.requestLayout()
            }
        }
    }

    override fun getLayout(): Int = R.layout.item_category
}


