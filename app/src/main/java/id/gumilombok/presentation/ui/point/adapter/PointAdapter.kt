package id.gumilombok.presentation.ui.point.adapter

import android.content.Context
import com.github.nitrico.lastadapter.Holder
import id.gumilombok.common.OnItemClick
import id.gumilombok.presentation.base.adapter.BaseListAdapter
import id.gumilombok.presentation.base.adapter.BaseViewHolder
import id.gumilombok.presentation.model.Point
import id.gumilombok.R
import kotlinx.android.synthetic.main.item_point_location.view.*

class PointAdapter(private val onItemClick:OnItemClick<Point>) : BaseListAdapter<Point>() {
    override fun getItemView(context: Context): BaseViewHolder<Point> = Holder(context)

    inner class Holder(context: Context?) : BaseViewHolder<Point>(context){
        override fun layoutResId(): Int = R.layout.item_point_location

        override fun bind(item: Point, position: Int) {
            tvLokasi.text=item.pointValue
            tvPointPlaceName.text=item.pointName
            setOnClickListener { onItemClick.onItemClick(item, position) }
//            btnTukarPoint.setOnClickListener { onItemClick.onItemClick(item,position) }
        }

    }

}