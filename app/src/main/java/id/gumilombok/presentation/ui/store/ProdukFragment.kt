package id.gumilombok.presentation.ui.store

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager

import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.toast
import id.gumilombok.data.remote.model.StoreProdukModel
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.presentation.base.BaseFragment
import id.gumilombok.presentation.ui.store.adapter.ProdukAdapter
import id.gumilombok.presentation.ui.store.presenter.StoreProdukPresenter
import id.gumilombok.presentation.ui.store.view.StoreView
import kotlinx.android.synthetic.main.fragment_produk.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class ProdukFragment : BaseFragment(), StoreView {

    companion object {
        private const val STOREID = "storeId"
        fun newInstance(storeId: Int) = ProdukFragment().apply {
            arguments = Bundle().apply {
                putInt(STOREID,storeId)
            }
        }
    }

    @Inject lateinit var produkPresenter: StoreProdukPresenter
    lateinit var produkAdapter:ProdukAdapter
    override fun injectModule(activityComponent: SyncComponent) {
        activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        produkPresenter.bind(this,isOnline = true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_produk, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
//        scrollView.setOnScrollListener { v, x, y, oldX, oldY ->
//            if (y > oldY)
//                fabChatToko.hide()
//            else
//                fabChatToko.show()
//        }

        fabChatToko.setOnClickListener {
            toast("kilk")
            sendMessageWithWhatsApp()
        }


    }

    private fun sendMessageWithWhatsApp() {
        try {
            val text = "Salam, Saya ingin menanyakan barang yang anda jual..?"
            val toNumber = "+62" + "081717366252"
            val intent = Intent(Intent.ACTION_VIEW)
            intent.`package` = ("com.whatsapp")
            intent.data = Uri.parse("http://api.whatsapp.com/send?phone=$toNumber&text=$text")
            startActivity(intent)
        } catch (e: Exception) {
            toast("Install WhatsApp untuk mengirim Pesan")
            e.printStackTrace()
        }
    }

    fun setupView(){
        showProdukByStoreId()
    }

    fun showProdukByStoreId(){
        val storeId=arguments?.getInt(STOREID)

        produkAdapter= ProdukAdapter(object : OnItemClick<StoreProdukModel> {

        })
        val layoutManager= GridLayoutManager(context,2)
        rv_produk_frag.layoutManager=layoutManager
        rv_produk_frag.adapter=produkAdapter
        produkPresenter.showProdukByStoreId(storeId!!,0,10)
    }

    override fun showMessage(message: String?) {
        TODO("Not yet implemented")
    }

    override fun showProgress(show: Boolean) {
        TODO("Not yet implemented")
    }

    override fun onLoadProdukByStoreId(produks:List<StoreProdukModel>?){

            produkAdapter.addItems(produks)

    }

}
