package id.gumilombok.presentation.ui.ticket.presenter

import android.util.Log
import com.google.gson.Gson
import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.model.OrderModel
import id.gumilombok.data.repository.OrderRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.ticket.view.OrderView
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class OrderPresenter @Inject constructor(val repo:OrderRepository,
                                         val sp:PreferencesManager):BasePresenter<OrderView>(){
    private val tag = this::class.java.simpleName

    lateinit var itemList:List<OrderModel>

    fun insertTiketToOrder(tiketId:String?="",tiketName:String?="",tiketRute:String?="",tiketAgent:String?="",tiketPriceDiskon:String?="",
                           tiketKeberangkatan:String?="",tiketTiba:String?="",tiketDetail:String?="",durasiTiket:String?="",
                           orderTiketBook:String?="",orderStatus:String?="",createdAt:String?="",updateAt:String="",
                           trash:String?="",metodeBayar:String?="",bankTujuan:String?="",namaPemilikRekening:String?="",
                           tiketPriceAdult:String?="",tiketPriceChil:String?="",jumlahAdult:String?="",jumlahAnak:String?="",
                           totalBayar:String?="",userId:String?=""){
        view?.showProgress(true)

        val map=HashMap<String,String?>()

        map["tiket_id"] = tiketId.toString()
        map["tiket_name"] = tiketName.toString()
        map["tiket_route"] = tiketRute.toString()
        map["tiket_agent"] = tiketAgent.toString()
        map["tiket_price_diskon"] = tiketPriceDiskon.toString()
        map["tiket_keberangkatan"] = tiketKeberangkatan.toString()
        map["tiket_tiba"] = tiketTiba.toString()
        map["tiket_detail"] = tiketDetail.toString()
        map["durasi_tiket"] = durasiTiket.toString()
        map["order_tiket_book"] = orderTiketBook.toString()
        map["order_status"] = orderStatus.toString()
        map["created_at"] = createdAt.toString()
        map["updated_at"] = updateAt.toString()
        map["trash"] = trash.toString()
        map["metode_bayar"] = metodeBayar.toString()
        map["bank_tujuan"] = bankTujuan.toString()
        map["nama_pemilik_rekening"] = namaPemilikRekening.toString()
        map["tiket_price_adult"] = tiketPriceAdult.toString()
        map["tiket_price_chil"] = tiketPriceChil.toString()
        map["jumlah_adult"] = jumlahAdult.toString()
        map["jumlah_anak"] = jumlahAnak.toString()
        map["total_bayar"] = totalBayar.toString()
        map["user_id"] = userId.toString()
        disposables.add(
            repo.destinationInsertedList(map)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)

                }
                .subscribe({
                    Log.i(tag, "Inserted data order: "+Gson().toJsonTree(it))
                    view?.onTransaksiSuccess(it.orderIdBase)

                },{ doOnError(view,it) })
        )

    }

    fun updateMetodeBayar(orderId:String="",metodeBayar: String="",bankTujuan: String="",norekeningTujuan:String="",cardHolderTujuan:String=""){
        view?.showProgress(true)

        val map=HashMap<String,String?>()

        map["order_id"] = orderId
        map["metode_bayar"] = metodeBayar.toString()
        map["bank_tujuan"] = bankTujuan.toString()
        map["norek"] = norekeningTujuan.toString()
        map["card_holder"] = cardHolderTujuan

        disposables.add(
            repo.updateMetodeBayar(map)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)

                }
                .subscribe({
                    Log.i(tag, "Inserted data order: "+Gson().toJsonTree(it))
//                    view?.onTransaksiSuccess(it.orderIdBase)

                },{ doOnError(view,it) })
        )

    }

    fun konfirmasiSudahBayar(orderId:String="",orderStatus: String="",namaPemilikRekening: String?=""){
        view?.showProgress(true)

        val map=HashMap<String,String?>()

        map["order_id"] = orderId
        map["order_status"] = orderStatus.toString()
        map["nama_pemilik_rekening"] = namaPemilikRekening.toString()

        disposables.add(
            repo.konfirmasiSudahBayar(map)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)

                }
                .subscribe({
                    Log.i(tag, "Inserted data order: "+Gson().toJsonTree(it))
//                    view?.onTransaksiSuccess(it.orderIdBase)

                },{ doOnError(view,it) })
        )

    }

    fun showListOrder(userId: String?,offset: Int, limit: Int, clearItem: Boolean, text: String? = null) {
        view?.showProgress((clearItem && text == null) || (!clearItem && text != null))
        view?.showLoadMoreProgress(!clearItem && text == null)

        val map = HashMap<String, String?>()
        map["user_id"] = userId.toString()
        map["offset"] = offset.toString()
        map["limit"] = limit.toString()
        map["keyword"] = text
        disposables.add(
            repo.orderList(map)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach {
                    view?.showProgress(false)
                    view?.showLoadMoreProgress(false)
                }
                .subscribe({
                    Log.i(tag, "load destination: " + Gson().toJsonTree(it))
                    view?.onOrderLoaded(it, clearItem)
                }, { doOnError(view, it) }))
    }

}