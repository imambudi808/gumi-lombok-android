package id.gumilombok.presentation.ui.destination


import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat

import id.gumilombok.R
import id.gumilombok.common.intentFor
import id.gumilombok.common.startActivity
import id.gumilombok.common.toCurrency
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.remote.response.CekRatingResponse
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.destination.presenter.DestinationPresenter
import id.gumilombok.presentation.ui.home.MainActivity
import id.gumilombok.presentation.ui.rating.presenter.RatingPresenter
import id.gumilombok.presentation.ui.rating.view.RatingView
import kotlinx.android.synthetic.main.activity_form_kementar.*
import kotlinx.android.synthetic.main.costume_comment.*
import kotlinx.android.synthetic.main.toolbar.view.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.singleTop
import javax.inject.Inject

class FormKementarActivity : BaseActivity(),RatingView {
    override fun cekRatingV(status: String?, pointValue: String?, ratingKomen: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }


    @Inject lateinit var sp:PreferencesManager
    @Inject lateinit var presenter:RatingPresenter
    private var destinationId:String?=""
    private var jumlahRate:String?=""
    private var rataRating:String?=""

    private var rataRatingBaru:Double?=0.0

    private var tampungJumlahRate:Int=0
    private var review:String=""
    private var bantu:String=""
//
    private var komentarExtra:String?=""
    @Inject lateinit var desPresenter:DestinationPresenter
    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_kementar)
        presenter.bind(this,isOnline)

        destinationId=intent.getStringExtra("destinationId")
        jumlahRate=intent.getStringExtra("ratingJumlah")
        rataRating=intent.getStringExtra("rataRating")
        review = intent.getStringExtra("reviews")
        komentarExtra = intent?.getStringExtra("komentarExtra")
        bantu = intent.getStringExtra("bantuEdit")

        tampungJumlahRate = (jumlahRate!!.toDouble().toInt()+rataRating!!.toDouble().toInt())
        var a=tampungJumlahRate.toDouble()
        var b=review.toInt().toDouble()
        var hasil:Double=0.0
        if (b==0.0){
            hasil = a / (1)
        }else if (b==0.toDouble()){
            hasil = a / (1)
        }else{
            hasil = a/2
        }

        var hasilb:Double=b+1

        Log.d("Hasil pembagian ","${hasil.toString()} ${a.toString()} ${b+1}" )
        Log.d("Nilai a b hasil","$a $b $hasil")



        rataRatingBaru = hasil.toDouble()
        review = hasilb.toString()


        setUpView()
    }

    fun setUpView(){
        setSupportActionBar(layToolbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Destinasi Wisata"
        edtComment.requestFocus()
        edtComment.requestFocusFromTouch()
        tvNamaHome2.text = sp.userSession.name?.substring(0,1)?.toUpperCase()
        warnaBintang(jumlahRate.toString())
        var coment = edtComment
        if (komentarExtra!=null){
            coment.setText("$komentarExtra")
        }
        btnSent.setOnClickListener {
            if (bantu=="baru"){
                presenter.insertDataToRating(jumlahRate.toString(),coment.text.toString(),destinationId,sp.userSession.id)
                desPresenter.updateDestination(destinationId,"${rataRatingBaru}",review)
//            presenter.updateRatingRata(destinationId)
                toast("berhasil memberikan tanggapan")

                startActivity(intentFor<MainActivity>().clearTop().clearTask())
            }else if (bantu=="lama"){
//                presenter.insertDataToRating(jumlahRate.toString(),coment.text.toString(),destinationId,sp.userSession.id)
//                desPresenter.updateDestination(destinationId,"${rataRatingBaru}",review)
//            presenter.updateRatingRata(destinationId)
                presenter.updateRatingUser(sp.userSession.id.toString(),destinationId.toString(),coment.text.toString())
                toast("berhasil mengubah tanggapan")
                startActivity(intentFor<MainActivity>().singleTop().clearTop().clearTask())
            }

        }


    }
    private fun warnaBintang(rating:String?){
        if (rating=="1"){
            bintang1.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
        }else if (rating=="2"){
            bintang1.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
            bintang2.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
        }
        else if (rating=="3"){
            bintang1.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
            bintang2.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
            bintang3.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
        }
        else if (rating=="4"){
            bintang1.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
            bintang2.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
            bintang3.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
            bintang4.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
        }
        else if (rating=="5"){
            bintang1.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
            bintang2.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
            bintang3.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
            bintang4.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
            bintang5.background= ContextCompat.getDrawable(this,R.drawable.ic_star_berisi)
        }else{

        }
    }
}
