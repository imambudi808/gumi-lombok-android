package id.gumilombok.presentation.ui.store.presenter

import id.gumilombok.common.doOnError
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.data.repository.StoreReviewRepository
import id.gumilombok.presentation.base.BasePresenter
import id.gumilombok.presentation.ui.store.view.StoreReviewView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StoreReviewPresenter @Inject constructor(val repository: StoreReviewRepository,val sp:PreferencesManager):
    BasePresenter<StoreReviewView>() {
    private val tag=this::class.java.simpleName

    //showAllListReview
    fun showReviewStore(storeId:Int,userId:Int){
        view?.showProgress(true)
        val map=HashMap<String,String>()
        map["store_id"] = storeId.toString()
        map["user_id"] = userId.toString()
        disposables.add(
            repository.storeReviewList(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEach{
                    view?.showProgress(false)
                }
                .subscribe({
                    view?.onReviewLoads(it)
                },{
                    doOnError(view,it)
                })
        )
    }
}