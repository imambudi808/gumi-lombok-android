package id.gumilombok.presentation.ui.point


import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View

import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import id.gumilombok.R
import id.gumilombok.common.startActivity
import id.gumilombok.common.toast
import id.gumilombok.data.local.PreferencesManager

import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.CustomAlert
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.destination.presenter.DestinationPresenter
import id.gumilombok.presentation.ui.dompet.presenter.DompetPresenter

import id.gumilombok.presentation.ui.dompet.view.DompetView
import id.gumilombok.presentation.ui.home.MainActivity
import id.gumilombok.presentation.ui.point.presenter.PointPresenter
import id.gumilombok.presentation.ui.point.view.PointView

import kotlinx.android.synthetic.main.activity_detail_point.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject


class DetailPointActivity : BaseActivity(),OnMapReadyCallback,DompetView {



    @Inject lateinit var sp:PreferencesManager
    @Inject lateinit var dompetPresenter: DompetPresenter
    private var pointAkhir = 0
    private var pointAwal = 0


    override fun showMessage(message: String?) {
        runOnUiThread { message?.let { toast(it) } }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    override fun showProgress(show: Boolean) {
        runOnUiThread { progress.visibility = if (show) View.VISIBLE else View.GONE }
    }

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var googleMap: GoogleMap? = null
    private lateinit var customAlert: CustomAlert
    private var pointCode:String?=""
    private var pointName:String?=""
    private var pointValue:String?=""
    private var pointId:String?=""
    private var latitude:String?=""
    private var longitude:String?=""

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
//        val latLngOrigin = LatLng(10.3181466, 123.9029382) // Ayala
        val latLngDestination = LatLng(latitude!!.toDouble(),longitude!!.toDouble()) // SM City
//        this.googleMap!!.addMarker(MarkerOptions().position(latLngOrigin).title("Ayala"))
        this.googleMap!!.addMarker(MarkerOptions().position(latLngDestination).title(pointName))
        this.googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngDestination, 14.5f))
    }


    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_point)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title="Detail Point"
        dompetPresenter.bind(this,isOnline)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        pointId = intent.getStringExtra("pointId")
        pointName = intent.getStringExtra("pointName")
        pointCode = intent.getStringExtra("pointCode")
        pointValue = intent.getStringExtra("pointValue")
        latitude = intent.getStringExtra("latitude")
        longitude = intent.getStringExtra("longitude")

        tvPointName.text=" Lokasi cek point :${pointName.toString().toLowerCase()}"
        tvJumlahPoint.text="Tukarkan kode point anda untuk :${pointValue} Pts"
        pointAwal=sp.userSession.total_point!!.toInt()
//        tvPointSisa.text = " Total Point : $pointAwal Pt"
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        layAmbilPoint.setOnClickListener{
            onPointTukar()
        }

    }

    private fun onPointTukar(){
        val v = LayoutInflater.from(this).inflate(R.layout.alert_tukar_point,ConstraintLayout(this))
        val code = v.findViewById<TextView>(R.id.edt_Kode)
        customAlert = CustomAlert(this)
        customAlert.setTitle("Penukaran Kode Point")
        customAlert.setContent(v)
        customAlert.setPositiveButton("Tukar",object :CustomAlert.AlertClickListener{
            override fun onClick() {
                val insertCode = code.text.toString()
                if (insertCode.isEmpty()){
                    showMessage("Anda belum memasukkan kode point")
                }else{
                    if (insertCode != pointCode){
                        showMessage("Code point tidak valid")
                    }else if(insertCode == pointCode){
                        dompetPresenter.savePointToDompet(pointValue,sp.userSession.id,pointName,pointId)
                        sp.cekLastPoint="${pointAwal + pointValue!!.toInt()}"
                        customAlert.dismiss()
                        showMessage("Point berhasil ditambahkan")
                        finish()
                        startActivity<MainActivity>()

//                        sp.userSession.total_point = "12"
                    }
                }
            }
        })

        customAlert.setNegativeButton("Batal",object :CustomAlert.AlertClickListener{
            override fun onClick() {
                customAlert.dismiss()
            }
        })
        customAlert.show()
    }
}
