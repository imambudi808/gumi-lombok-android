package id.gumilombok.presentation.ui.event

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.gumilombok.R
import id.gumilombok.common.OnItemClick
import id.gumilombok.common.toast
import id.gumilombok.data.remote.model.EventModel
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation._costume.EndlessRecyclerViewScrollListener
import id.gumilombok.presentation.base.BaseActivity
import id.gumilombok.presentation.ui.event.adapter.EventAdapterActivity
import id.gumilombok.presentation.ui.event.presenter.EventPresenter
import id.gumilombok.presentation.ui.event.view.EventView
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.toolbar.view.*
import org.jetbrains.anko.intentFor
import javax.inject.Inject



class EventActivity : BaseActivity(),EventView {
    @Inject lateinit var presenter:EventPresenter
    private var query:String?=""
    lateinit var adapter:EventAdapterActivity
     override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)
        setSupportActionBar(layToolbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title="Event"
        presenter.bind(this,isOnline)
        setUpView()
    }

    private fun setUpView(){
        adapter= EventAdapterActivity(object :OnItemClick<EventModel>{
            override fun onItemClick(item: EventModel?, position: Int?) {
                startActivity(intentFor<EventDetailActivity>().apply {
                    putExtra("eventId",item?.eventId)
                    putExtra("eventName",item?.eventName)
                    putExtra("eventImage",item?.eventImage)
                    putExtra("eventCost",item?.eventCost)
                    putExtra("eventLocation",item?.eventLocation)
                    putExtra("eventDate",item?.eventDate)
                    putExtra("createdAt",item?.createdAt)
                    putExtra("seoLink",item?.seoLink)

                })
            }
        },object :EventAdapterActivity.SearchResultCallback{
            override fun onDataNotFound(s: String) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })
        val lm=LinearLayoutManager(this)
        rv_event.layoutManager=lm
        rv_event.adapter=adapter
        rv_event.invalidate()
        val scrollListener = object : EndlessRecyclerViewScrollListener(lm) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.showEvents(totalItemsCount, totalItemsCount.plus(3), false, query)
            }
        }
        rv_event.addOnScrollListener(scrollListener)
        presenter.showEvents(0,3,true)


    }

    override fun injectModule(activityComponent: ActivityComponent) {
        activityComponent.inject(this)

    }

    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun showLoadMore(showLoading: Boolean) {
        loadMoreProgress.visibility=if (showLoading) View.VISIBLE else View.GONE
    }

    override fun onEventLoads(events: List<EventModel>?, clearItem: Boolean) {
        if (clearItem) adapter.clearItems()
        adapter.addItems(events)
         }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchItem: MenuItem? = menu?.findItem(R.id.act_search)
        val searchView = searchItem?.actionView as SearchView?
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean = false

            override fun onQueryTextChange(newText: String?): Boolean {

                query = newText
                adapter.filter.filter(query)
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }



}
