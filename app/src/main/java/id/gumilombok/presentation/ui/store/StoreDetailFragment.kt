package id.gumilombok.presentation.ui.store

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.gumilombok.R
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.activity_ticket.*

/**
 * A simple [Fragment] subclass.
 */
class StoreDetailFragment : BaseFragment() {
    var des:String?=""
    companion object {
        private const val STOREID = "storeId"
        fun newInstance(storeId: Int) = StoreReviewFragment().apply {
            arguments = Bundle().apply {
                putInt(STOREID,storeId)
            }
        }
    }
    override fun injectModule(activityComponent: SyncComponent) {
        activityComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_store_detail, container, false)
//        textView.text=i
    }

}
