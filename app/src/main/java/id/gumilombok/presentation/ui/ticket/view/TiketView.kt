package id.gumilombok.presentation.ui.ticket.view


import id.gumilombok.data.remote.model.TiketModel
import id.gumilombok.presentation.base.view.BaseView

interface TiketView:BaseView{
    fun showLoadMoreProgress(show: Boolean) {}
    fun onTiketLoad(tikets: List<TiketModel>?, clearItem: Boolean) {}

}