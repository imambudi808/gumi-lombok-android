package id.gumilombok.presentation.ui.ticket.view



import id.gumilombok.data.remote.model.HistoriModel
import id.gumilombok.data.remote.model.OrderModel
import id.gumilombok.presentation.base.view.BaseView
import id.gumilombok.presentation.model.Order

interface OrderView: BaseView {
    fun onTransaksiSuccess(order:String?){}
    fun showLoadMoreProgress(show: Boolean) {}
    fun onOrderLoaded(orders: List<HistoriModel>?, clearItem: Boolean) {}

}