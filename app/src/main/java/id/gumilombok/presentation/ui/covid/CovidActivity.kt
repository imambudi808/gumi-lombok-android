package id.gumilombok.presentation.ui.covid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.gumilombok.R
import id.gumilombok.di.component.ActivityComponent
import id.gumilombok.presentation.base.BaseActivity

class CovidActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_covid)
    }

    override fun injectModule(activityComponent: ActivityComponent) {
        TODO("Not yet implemented")
    }
}
