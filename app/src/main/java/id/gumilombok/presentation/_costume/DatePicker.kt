//package id.gumilombok.presentation._costume
//
//import androidx.appcompat.app.AppCompatActivity
//import android.os.Bundle
//import android.os.PersistableBundle
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.appcompat.app.AppCompatDialogFragment
//import androidx.recyclerview.widget.GridLayoutManager
//import com.rackspira.kristiawan.rackmonthpicker.MonthAdapter
//import com.xwray.groupie.GroupAdapter
//import com.xwray.groupie.ViewHolder
//import id.gumilombok.R
//import kotlinx.android.synthetic.main.activity_date_picker.*
//import java.lang.RuntimeException
//import java.text.SimpleDateFormat
//import java.util.*
//import id.gumilombok.presentation._costume.DateAdapter
//
//class DatePicker : AppCompatActivity() {
//    private lateinit var onSelect: (Int, Int, String) -> Unit
//
//    companion object {
//        fun newInstance() = DatePicker()
//    }
//
//    private val monthAdapter = GroupAdapter<com.xwray.groupie.kotlinandroidextensions.ViewHolder>()
//
//    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
//        super.onCreate(savedInstanceState, persistentState)
//        setContentView(R.layout.activity_date_picker)
//        setupView()
//        showMonth()
//    }
//
////    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
////        return inflater.inflate(R.layout.activity_date_picker, container, false)
////    }
////
////    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
////        super.onViewCreated(view, savedInstanceState)
////
////        setupView()
////        showMonth()
////    }
//
//    private fun setupView() = view?.run {
//        rvMonth.layoutManager = GridLayoutManager(context, 4)
//        rvMonth.adapter = monthAdapter
//
//        val cal = Calendar.getInstance()
//        val year = cal.get(Calendar.YEAR)
//        tvYear.text = year.toString()
//    }
//
//    private fun showMonth() {
//        monthAdapter.clear()
//        MonthHelper.getMonthList(Locale.getDefault()).forEach {
//            monthAdapter.add(DateAdapter(it, this::onMonthSelected))
//        }
//
//        btnPreviousYear.setOnClickListener {
//            setPreviousYear()
//        }
//
//        btnNextYear.setOnClickListener {
//            setNextYear()
//        }
//    }
//
//    private fun setPreviousYear() {
//        var year = tvYear.text.toString().toInt()
//        if (year > 2019)
//            tvYear.text = (--year).toString()
//    }
//
//    private fun setNextYear() {
//        var year = tvYear.text.toString().toInt()
//        tvYear.text = (++year).toString()
//    }
//
//    private fun onMonthSelected(pair: Pair<Int, String>) {
//        val year = tvYear.text.toString().toInt()
//        onSelect(year, pair.first, pair.second)
//    }
//
//    fun setOnSelectedListener(onSelect: (Int, Int, String) -> Unit) {
//        this.onSelect = onSelect
//    }
//    ///////////////////////////////////////////////////////////////////////////////////////////////
//
//    object MonthHelper {
//
//        fun getMonthList(locale: Locale): List<Pair<Int, String>> {
//            val monthParse = SimpleDateFormat("MM", locale)
//            val monthDisplay = SimpleDateFormat("MMMM", locale)
//            return (1..12).map {
//                val month = monthDisplay.format(monthParse.parse(it.toString())!!)
//                Pair(it, month)
//            }
//        }
//
//        /**
//         * Used for returning display name of given index of month
//         * @link index : month index like 1 for January, 2 for February, etc
//         * @link locale : get name by given locale
//         * */
//
//        fun getMonthByIndex(locale: Locale, index: Int): String {
//            val monthParse = SimpleDateFormat("MM", locale)
//            val monthDisplay = SimpleDateFormat("MMMM", locale)
//            val date = monthParse.parse(index.toString())
//            if (date != null)
//                return monthDisplay.format(date)
//            else
//                throw RuntimeException("Invalid given index")
//        }
//    }
//}
