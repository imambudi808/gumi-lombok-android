package id.gumilombok.presentation._costume

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import id.gumilombok.R

class CustomAlert(private val context: Context) {


    private val dialog: Dialog = Dialog(context)
    private val view: View by lazy {
        LayoutInflater.from(context).inflate(R.layout.custom_alert, RelativeLayout(context), false)
    }
    private val alertHeader: ConstraintLayout = view.findViewById(R.id.alertHeader)
    private val tvTitle: TextView = view.findViewById(R.id.alertTitle)
    private val txtMessage: TextView = view.findViewById(R.id.alertMessage)
    private val alertContent: LinearLayout = view.findViewById(R.id.alertContent)
    private val alertButton: LinearLayout = view.findViewById(R.id.alertButton)
    private val btnPositive: Button = view.findViewById(R.id.alertBtnPositive)
    private val btnNegative: Button = view.findViewById(R.id.alertBtnNegative)
    private val tvWa: TextView =view.findViewById(R.id.tvWa)

//    private val imgClose: ImageView =view.findViewById(R.id.iv_close)


    init {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(view)
    }


    fun setCancelable(isCancelable: Boolean): CustomAlert {
        dialog.setCancelable(isCancelable)
        return this
    }

    fun setTitle(title: String): CustomAlert {
        alertHeader.visibility = View.VISIBLE
        tvTitle.text = title
        return this
    }

    fun setMessage(message: String): CustomAlert {
        txtMessage.visibility = View.VISIBLE
        txtMessage.text = message
        return this
    }

    fun setExpiringMessage(message: String): CustomAlert {
        txtMessage.visibility = View.VISIBLE
        txtMessage.text = message
        txtMessage.textSize = 14f
        txtMessage.textAlignment = View.TEXT_ALIGNMENT_CENTER

        return this
    }

    fun isi(message: String): CustomAlert {
        txtMessage.visibility = View.VISIBLE
        txtMessage.text = message
        txtMessage.textSize = 14f
        txtMessage.textAlignment = View.TEXT_ALIGNMENT_CENTER

        return this
    }

    fun setContent(view: View?): CustomAlert {
        alertContent.visibility = View.VISIBLE
        if (alertContent.childCount > 0) {
            alertContent.removeAllViews()
            alertContent.removeViews(0, alertContent.childCount)
        } else {
            alertContent.addView(view)
        }
        return this
    }
    fun formFeed(text: String,alertClickListener: AlertClickListener):CustomAlert{
        tvWa.visibility= View.VISIBLE
        tvWa.text=text
        tvWa.textAlignment = View.TEXT_ALIGNMENT_CENTER
        tvWa.setOnClickListener{alertClickListener.onClick()}
        return this
    }

    fun setSuccessMessage(message: String): CustomAlert {
        val view = LayoutInflater.from(context).inflate(R.layout.alert_success, RelativeLayout(context), false)
        val txtMessage = view.findViewById<TextView>(R.id.txt_message)
        txtMessage.text = message
        setContent(view)
        return this
    }

    fun setPositiveButton(text: String, alertClickListener: AlertClickListener): CustomAlert {
        alertButton.visibility = View.VISIBLE
        btnPositive.visibility = View.VISIBLE
        btnPositive.text = text
        btnPositive.setOnClickListener { alertClickListener.onClick() }
        return this
    }


    fun setNegativeButton(text: String, alertClickListener: AlertClickListener): CustomAlert {
        alertButton.visibility = View.VISIBLE
        btnNegative.visibility = View.VISIBLE
        btnNegative.text = text
        btnNegative.setOnClickListener { alertClickListener.onClick() }
        return this
    }

//    fun closeAlert(alertClick:AlertClickListener):CustomAlert{
//        imgClose.visibility = View.VISIBLE
//        imgClose.setOnClickListener{alertClick.onClick()}
//        return this
//    }

    fun show() {
        dialog.show()
    }

    fun dismiss() {
        if (alertContent.childCount > 0) {
            alertContent.removeAllViews()
            alertContent.removeViews(0, alertContent.childCount)
        }
        dialog.cancel()
    }

    interface AlertClickListener {
        fun onClick()
    }
}
