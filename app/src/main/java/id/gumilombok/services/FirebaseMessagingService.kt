package id.gumilombok.services

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.singleTop
import java.util.*


class FirebaseMessagingService : FirebaseMessagingService() {

    private val tag = this::class.java.simpleName

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage?.data?.size?:0 > 0 || !remoteMessage?.notification?.body.isNullOrEmpty()) {
            Log.d(tag, "message data : ${remoteMessage?.data?.get("data")}")
            Log.d(tag, "message body : ${remoteMessage?.notification?.body}")
//            sendNotification(remoteMessage?.notification?.body)
        }
    }

//    private fun sendNotification(messageBody: String?) {
//        val title = "Londree"
//        val pendingIntent = PendingIntent.getActivity(this,
//            0,
//            intentFor<InboxActivity>().singleTop(),
//            PendingIntent.FLAG_ONE_SHOT)
//        val sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
//
//        val builder = NotificationCompat.Builder(this,
//            getString(R.string.default_notification_channel_id))
//            .setSmallIcon(R.mipmap.icon)
//            .setContentTitle(title)
//            .setContentText(fromHtml(messageBody?: ""))
//            .setAutoCancel(true)
//            .setSound(sound)
//            .setContentIntent(pendingIntent)
//
//        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//        notificationManager.notify(getRequestCode(), builder.build())
//    }

    private fun getRequestCode() : Int {
        val rnd = Random()
        return 100 + rnd.nextInt(900000)
    }
}
