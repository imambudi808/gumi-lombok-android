package id.gumilombok.services

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import id.gumilombok.Gumi
import id.gumilombok.data.local.PreferencesManager
import id.gumilombok.di.component.SyncComponent
import id.gumilombok.di.module.SyncServiceModule
import javax.inject.Inject

class FirebaseInstanceIDService: FirebaseMessagingService() {

    private val TAG = this::class.java.simpleName

    @Inject
    lateinit var pref: PreferencesManager

    override fun onCreate() {
        super.onCreate()
        val serviceInjector: SyncComponent = Gumi.get(applicationContext)
            .appComponent
            .syncComponent()
            .syncModule(SyncServiceModule(applicationContext))
            .build()
        serviceInjector.inject(this)
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s);
        Log.d("NEW_TOKEN", s);

    }
}